//
//  OSSP asgui - Accounting system graphical user interface
//  Copyright (c) 2002-2004 The OSSP Project (http://www.ossp.org/)
//  Copyright (c) 2002-2004 Ralf S. Engelschall <rse@engelschall.com>
//  Copyright (c) 2002-2004 Michael Schloh von Bennewitz <michael@schloh.com>
//  Copyright (c) 2002-2004 Cable & Wireless Telecommunications Services GmbH
//
//  This file is part of OSSP asgui, an accounting system graphical user
//  interface which can be found at http://www.ossp.org/pkg/tool/asgui/.
//
//  Permission to use, copy, modify, and distribute this software for
//  any purpose with or without fee is hereby granted, provided that
//  the above copyright notice and this permission notice appear in all
//  copies.
//
//  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
//  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
//  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
//  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
//  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
//  SUCH DAMAGE.
//
//  as_gui.cpp: ISO C++ implementation
//

// User interface
#include "as_gui.h"     // Main classes
#include "as_except.h"  // Exception classes
#include "as_table.h"   // Class TiTable
#include "as_const.h"   // Application constants
#include "as_pref.h"    // Class Preferences


//
// Construct a Titraqform which is a child of 'pParent', with the
// name 'kszName' and widget flags set to 'Flags'
//
Titraqform::Titraqform(QWidget *pParent, const char *kszName, WFlags Flags) :
    QMainWindow(pParent, kszName, Flags)
{
    // Early initializations
    m_pFiletools = NULL;
    m_pEdittools = NULL;
    m_pViewtools = NULL;
    m_pWhatstools = NULL;
    m_szFilename = NULL;

    // Atenzione! Order is very important in the following sequence.
    // FIXME: Reorganize this procedural mess into self contained objects
    try {
        setupPrefs();           // Load general preferences
        setupActions();         // Create and initialize actions
        setupMenubar();         // Create and initialize menu bar
        setupToolbars();        // Create and initialize tool bars
        setupCentralwidget();   // Format the layout of related widgets
        setupStatusbar();       // Create and initialize status bar
        setupTable();           // Create and initialize table and cells
        setupEditlay();         // Create and initialize edit controls
        setupColumns();         // Prepare columns for viewing, sorting
        enableIface(false);     // Start things off in a empty state
    }
    catch (Genexcept& Genex) {
        Genex.reportErr();
        exit(1);
    }

    // Postsetup manipulations
    m_pMaintable->setDirty(false);
    if (!kszName)
        this->setName(trUtf8("ASGuiapp"));
    this->resize(m_pPrefs->getNumber(TITRAQ_PREFFRAMEWIDTH, TITRAQ_DEFFRAMEWIDTH),
        m_pPrefs->getNumber(TITRAQ_PREFFRAMEHEIGHT, TITRAQ_DEFFRAMEHEIGHT));
    connect(qApp, SIGNAL(aboutToQuit()), this, SLOT(savePrefs()));
    this->setCaption(QString(TITRAQ_APPTITLE) + ' ' + asgui_version.v_short);

    // Rehydrate main window layout and doc positions
    QString Laystring = m_pPrefs->getString(TITRAQ_PREFFRAMELAY, NULL); // FIXME: Handle first case better
    QTextStream Laystream(&Laystring, IO_ReadOnly);
    Laystream >> *this;

//    // Lock down window size
//    setSizePolicy(QSizePolicy((QSizePolicy::SizeType)0,
//        (QSizePolicy::SizeType)0, 0, 0, sizePolicy().hasHeightForWidth()));
}

//
// Destroy the object and free any allocated resources
//
Titraqform::~Titraqform(void)
{
    // Qt deletes child widgets for us
    m_pPrefs->flush();
    delete m_pPrefs;
}
