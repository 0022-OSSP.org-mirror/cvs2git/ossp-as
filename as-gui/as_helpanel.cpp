//
//  OSSP asgui - Accounting system graphical user interface
//  Copyright (c) 2002-2004 The OSSP Project (http://www.ossp.org/)
//  Copyright (c) 2002-2004 Ralf S. Engelschall <rse@engelschall.com>
//  Copyright (c) 2002-2004 Michael Schloh von Bennewitz <michael@schloh.com>
//  Copyright (c) 2002-2004 Cable & Wireless Telecommunications Services GmbH
//
//  This file is part of OSSP asgui, an accounting system graphical user
//  interface which can be found at http://www.ossp.org/pkg/tool/asgui/.
//
//  Permission to use, copy, modify, and distribute this software for
//  any purpose with or without fee is hereby granted, provided that
//  the above copyright notice and this permission notice appear in all
//  copies.
//
//  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
//  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
//  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
//  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
//  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
//  SUCH DAMAGE.
//
//  as_helpanel.cpp: ISO C++ implementation
//

#include <qvariant.h>
#include <qpushbutton.h>
#include <qtextbrowser.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

#include "as_helpanel.h"


namespace AS {

//
// Constructs a Helpanel as a child of 'pParent', with the 
// name 'kszName' and widget flags set to 'Flags'.
//
// The dialog will by default be modal, unless you set 'bModal' to
// false to construct a modeless dialog.
//
Helpanel::Helpanel(const QString &kSource, QWidget *pParent, const char *kszName, bool bModal, WFlags Flags)
    : QDialog(pParent, kszName, bModal, Flags)
{
    // Boilerplate code to initialize the panel
    if (!kszName)
        this->setName("Helpanel");

    // Make panel resizeable
    this->setSizeGripEnabled(true);
    this->setSizePolicy(QSizePolicy((QSizePolicy::SizeType)5,
        (QSizePolicy::SizeType)5, 0, 0, this->sizePolicy().hasHeightForWidth()));

    // Build panel using already constructed widgets and layouts
    m_pFormlay = new QVBoxLayout(this, 11, 6, "Formlayout");

    // Groupbox and its text display
    m_pBrowser = new QTextBrowser(this, "Helpbrowser");
    m_pBrowser->setSource(QString(TITRAQ_DOCDIR) + QChar('/') + kSource);
    m_pBrowser->setReadOnly(true);
    m_pBrowser->setFocus();

    // Add a spacer to sideline the otherwise massive dismiss button
    m_pButtlay = new QHBoxLayout(0, 0, 6, "Buttonlayout"); 
    QSpacerItem *pSpacey = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);
    m_pButtlay->addItem(pSpacey);

    // Add dismiss push button
    m_pDismissbutt = new QPushButton(this, "Dismissbutton");
    m_pDismissbutt->setPaletteBackgroundColor(QColor(198, 196, 186));
    m_pDismissbutt->setCursor(QCursor(13));
    m_pButtlay->addWidget(m_pDismissbutt);

    // Add the stuff to our form layout
    m_pFormlay->addWidget(m_pBrowser);
    m_pFormlay->addSpacing(6);
    m_pFormlay->addLayout(m_pButtlay);

    // Connect signals to slots, accept() and reject() are Qt implicit
    connect(m_pDismissbutt, SIGNAL(clicked(void)), SLOT(accept(void)));
    this->resize(QSize(464, 332).expandedTo(minimumSizeHint()));
    this->textChange();
}

//
// Sets the strings of the subwidgets using the current language
//
void Helpanel::textChange(void)
{
    this->setCaption(trUtf8("AS Accounting System help contents", "Help contents for the AS GUI application."));

    // Top level push buttons associated with accept and save slots
    m_pDismissbutt->setText(trUtf8("Dismiss", "Comment for Dismissbutton"));
    QToolTip::add(m_pDismissbutt, trUtf8("Closes the help panel", "Comment for tooltip Dismissbutton"));
    QWhatsThis::add(m_pDismissbutt, trUtf8("The dismiss button dismisses the help panel", "Comment for whatsThis Dismissbutton"));

    // The main text browser window which presents the HTML help contents
    QWhatsThis::add(m_pBrowser, trUtf8("The text browser window displays the help contents", "Comment for whatsThis Browser"));
}
} // namespace AS
