//
//  OSSP asgui - Accounting system graphical user interface
//  Copyright (c) 2002-2004 The OSSP Project (http://www.ossp.org/)
//  Copyright (c) 2002-2004 Ralf S. Engelschall <rse@engelschall.com>
//  Copyright (c) 2002-2004 Michael Schloh von Bennewitz <michael@schloh.com>
//  Copyright (c) 2002-2004 Cable & Wireless Telecommunications Services GmbH
//
//  This file is part of OSSP asgui, an accounting system graphical user
//  interface which can be found at http://www.ossp.org/pkg/tool/asgui/.
//
//  Permission to use, copy, modify, and distribute this software for
//  any purpose with or without fee is hereby granted, provided that
//  the above copyright notice and this permission notice appear in all
//  copies.
//
//  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
//  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
//  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
//  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
//  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
//  SUCH DAMAGE.
//
//  as_user.cpp: ISO C++ implementation
//

// For username research
#if defined(Q_OS_WIN32)
#include <lmcons.h>
#else
#include <pwd.h>
#include <unistd.h>
#endif // #if defined(Q_OS_WIN32)

// Local class definitions
#include "as_user.h"
#include "as_const.h"


// Constructor
User::User(void)
{
// Start the show by getting the username
#if defined(Q_OS_WIN32)
    {
        DWORD dwWinusernamesize = sizeof(dwWinusernamesize);
#if defined(UNICODE)
        TCHAR szWinusername[UNLEN + 1];   // UNLEN is defined in lmcons.h
        GetUserName(szWinusername, &dwWinusernamesize);
        m_Name = qt_winQString(szWinusername);
#else   // Not unicode
        char szWinusername[UNLEN + 1];    // UNLEN is defined in lmcons.h
        GetUserNameA(szWinusername, &dwWinusernamesize);
        this->setName(szWinusername);
    }
#endif  // #if defined(UNICODE)
#else   // Not windows
    {
//#include <stdio.h>
//        char *szUser = NULL;
//        szUser = cuserid();
//        m_Name = QString::fromLocal8Bit(szUser);

        // Get the user name from the environment
        char *szLogin = getenv(TITRAQ_ENVUSERNAME);
        if (szLogin == NULL)        // Is the user name in the environment?
            szLogin = getlogin();   // No, so fetch it from the system

        // Get the home directory from the environment
        char *szHomedir = getenv(TITRAQ_ENVHOMEDIR);
        if (szHomedir == NULL) {    // Is the home directory in the environment?
            passwd *pUserpwd = getpwnam(szLogin); // No, so fetch it from the system
            szHomedir = pUserpwd->pw_dir;         // Drill into the password struct
        }

        this->setName(szLogin);
        this->setHomedir(szHomedir);
    }
#endif  // #if defined(Q_OS_WIN32)
}
