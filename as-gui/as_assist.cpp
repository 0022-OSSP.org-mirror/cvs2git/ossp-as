//
//  OSSP asgui - Accounting system graphical user interface
//  Copyright (c) 2002-2004 The OSSP Project (http://www.ossp.org/)
//  Copyright (c) 2002-2004 Ralf S. Engelschall <rse@engelschall.com>
//  Copyright (c) 2002-2004 Michael Schloh von Bennewitz <michael@schloh.com>
//  Copyright (c) 2002-2004 Cable & Wireless Telecommunications Services GmbH
//
//  This file is part of OSSP asgui, an accounting system graphical user
//  interface which can be found at http://www.ossp.org/pkg/tool/asgui/.
//
//  Permission to use, copy, modify, and distribute this software for
//  any purpose with or without fee is hereby granted, provided that
//  the above copyright notice and this permission notice appear in all
//  copies.
//
//  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
//  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
//  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
//  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
//  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
//  SUCH DAMAGE.
//
//  as_assist.cpp: ISO C++ implementation
//

// Qt style headers
#include <qcdestyle.h>
#include <qsgistyle.h>
#include <qmotifstyle.h>
#include <qmotifplusstyle.h>
#include <qplatinumstyle.h>
#include <qwindowsstyle.h>

// Qt general headers
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qstringlist.h>
#include <qpopupmenu.h>
#include <qcombobox.h>
#include <qlistbox.h>
#include <qstatusbar.h>
#include <qmenubar.h>
#include <qcursor.h>
#include <qaction.h>
#include <qrect.h>
#include <qdir.h>

// User interface
#include "as_amount.h"          // AmountBox class
#include "as_user.h"            // User class
#include "as_const.h"           // Application constants
#include "as_table.h"           // Class TiTable
#include "as_pref.h"            // Class Preferences
#include "as_panel.h"           // Class Prefpanel
#include "as_datedit.h"         // Class Daydateedit

// Icon pixel maps
#include "as_gfx/filenew.xpm"   // static const char *s_kpcFilenew_xpm[]
#include "as_gfx/fileopen.xpm"  // static const char *s_kpcFileopen_xpm[]
#include "as_gfx/filesave.xpm"  // static const char *s_kpcFilesave_xpm[]
#include "as_gfx/filesaved.xpm" // static const char *s_kpcDfilesave_xpm[]
#include "as_gfx/cut.xpm"       // static const char *s_kpcCut_xpm[]
#include "as_gfx/cutd.xpm"      // static const char *s_kpcDcut_xpm[]
#include "as_gfx/copy.xpm"      // static const char *s_kpcCopy_xpm[]
#include "as_gfx/copyd.xpm"     // static const char *s_kpcDcopy_xpm[]
#include "as_gfx/paste.xpm"     // static const char *s_kpcPaste_xpm[]
#include "as_gfx/pasted.xpm"    // static const char *s_kpcDcaste_xpm[]
#include "as_gfx/rowadd.xpm"    // static const char *s_kpcRowadd_xpm[]
#include "as_gfx/rowdel.xpm"    // static const char *s_kpcRowdel_xpm[]
#include "as_gfx/rowaddd.xpm"   // static const char *s_kpcDrowadd_xpm[]
#include "as_gfx/rowdeld.xpm"   // static const char *s_kpcDrowdel_xpm[]
#include "as_gfx/osspicon.xpm"  // static const char *s_kpcOsspicon_xpm[]
#include "as_gfx/whatsthis.xpm" // static const char *s_kpcWhatsthis_xpm[]
#include "as_gfx/statok.xpm"    // static const char *s_kpcStatokay_xpm[]
#include "as_gfx/staterr.xpm"   // static const char *s_kpcStaterror_xpm[]
#include "as_gfx/statwrn.xpm"   // static const char *s_kpcStatwarn_xpm[]
#include "as_gfx/statvoid.xpm"  // static const char *s_kpcStatvoid_xpm[]
#include "as_gfx/refresh.xpm"   // static const char *s_kpcRefresh_xpm[]
#include "as_gfx/refreshd.xpm"  // static const char *s_kpcDrefresh_xpm[]
#include "as_gfx/sync.xpm"      // static const char *s_kpcSync_xpm[]
#include "as_gfx/syncd.xpm"     // static const char *s_kpcDsync_xpm[]
#include "as_gfx/report.xpm"    // static const char *s_kpcReport_xpm[]
#include "as_gfx/reportd.xpm"   // static const char *s_kpcDreport_xpm[]
#include "as_gfx/prefs.xpm"     // static const char *s_kpcPrefs_xpm[]


//
// Construct the preferences
//
void Titraqform::setupPrefs(void)
{
    // Can't bootstrap until we know who is using our tool, so find out
    User Localuser   = User();
    QString Username = Localuser.getName();
    QString Homedir  = Localuser.getHomedir();

    // Bootstrap a user preferences object from operating system info
    m_pPrefs = new Preferences(Homedir + QChar('/') +
                               QString::fromLocal8Bit(TITRAQ_PREFNAME),
                               QString::fromLocal8Bit(TITRAQ_APPTITLE),
                               QString::fromLocal8Bit(TITRAQ_PREFVER));
    if (!m_pPrefs->fileState()) {   // No file was found, so assume a null state
        if (!Homedir.isEmpty())
            m_pPrefs->setString(TITRAQ_PREFHOME, Homedir);
        else
            m_pPrefs->setString(TITRAQ_PREFHOME, TITRAQ_DEFHOME);
        if (!Username.isEmpty())
            m_pPrefs->setString(TITRAQ_PREFUSER, Username);
        else
            m_pPrefs->setString(TITRAQ_PREFUSER, TITRAQ_DEFUSER);

        m_pPrefs->setNumber(TITRAQ_PREFSTYLE, TITRAQ_STYLECDE);
        m_pPrefs->setString(TITRAQ_PREFACCOUNTS, TITRAQ_DEFACCOUNTS);
        m_pPrefs->setString(TITRAQ_PREFASDIR, TITRAQ_DEFASDIR);
        m_pPrefs->setString(TITRAQ_PREFVIEW, TITRAQ_DEFVIEW);
        m_pPrefs->setNumber(TITRAQ_PREFSORTCOL, TITRAQ_DEFSORTCOL);
        m_pPrefs->setBool(TITRAQ_PREFSORTDIR, TITRAQ_DEFSORTDIR);
        m_pPrefs->setString(TITRAQ_PREFREMOTELOG, TITRAQ_DEFREMOTELOG);
        m_pPrefs->setString(TITRAQ_PREFLOCALLOG, TITRAQ_DEFLOCALLOG);
        m_pPrefs->setString(TITRAQ_PREFCORBHOST, TITRAQ_DEFCORBHOST);
        m_pPrefs->setString(TITRAQ_PREFSOAPHOST, TITRAQ_DEFSOAPHOST);
        m_pPrefs->setBool(TITRAQ_PREFCORBON, TITRAQ_DEFCORBON);
        m_pPrefs->setBool(TITRAQ_PREFSOAPON, TITRAQ_DEFSOAPON);
        m_pPrefs->setBool(TITRAQ_PREFBAKON, TITRAQ_DEFBAKON);
        m_pPrefs->setBool(TITRAQ_PREFEXTENDON, TITRAQ_DEFEXTENDON);
        m_pPrefs->setBool(TITRAQ_PREFDETAILON, TITRAQ_DEFDETAILON);
        m_pPrefs->setBool(TITRAQ_PREFSIGNATON, TITRAQ_DEFSIGNATON);
        m_pPrefs->setNumber(TITRAQ_PREFREPORTYPE, TITRAQ_DEFREPORTYPE);
        m_pPrefs->setNumber(TITRAQ_PREFREPORTWEEKS, TITRAQ_DEFREPORTWEEKS);
        m_pPrefs->setNumber(TITRAQ_PREFREPORTMONTHS, TITRAQ_DEFREPORTMONTHS);
        m_pPrefs->setNumber(TITRAQ_PREFLIGHTRED, TITRAQ_DEFLIGHTRED);
        m_pPrefs->setNumber(TITRAQ_PREFLIGHTGREEN, TITRAQ_DEFLIGHTGREEN);
        m_pPrefs->setNumber(TITRAQ_PREFLIGHTBLUE, TITRAQ_DEFLIGHTBLUE);
        m_pPrefs->setNumber(TITRAQ_PREFDARKRED, TITRAQ_DEFDARKRED);
        m_pPrefs->setNumber(TITRAQ_PREFDARKGREEN, TITRAQ_DEFDARKGREEN);
        m_pPrefs->setNumber(TITRAQ_PREFDARKBLUE, TITRAQ_DEFDARKBLUE);
        m_pPrefs->flush();  // Write the new conf file
        QTextStream cerr(stderr, IO_WriteOnly);
        cerr << trUtf8("Created new preferences file ") << Homedir << QChar('/')
             << QString::fromLocal8Bit(TITRAQ_PREFNAME) << endl;
    }

    // Use the preferred configuration values to initialize titraq
    switch (m_pPrefs->getNumber(TITRAQ_PREFSTYLE, TITRAQ_STYLECDE)) {
    case TITRAQ_STYLECDE:
        qApp->setStyle(new QCDEStyle);
        break;
    case TITRAQ_STYLESGI:
        qApp->setStyle(new QSGIStyle);
        break;
    case TITRAQ_STYLEMOTIF:
        qApp->setStyle(new QMotifStyle);
        break;
    case TITRAQ_STYLEMPLUS:
        qApp->setStyle(new QMotifPlusStyle);
        break;
    case TITRAQ_STYLEPLAT:
        qApp->setStyle(new QPlatinumStyle);
        break;
    case TITRAQ_STYLEMSOFT:
        qApp->setStyle(new QWindowsStyle);
        break;
    default:
        qApp->setStyle(new QCDEStyle);
        break;
    }
}

//
// Construct various actions
//
void Titraqform::setupActions(void)
{
    QIconSet Saveiset, Cutiset, Copyiset, Pasteiset;
    QIconSet Rowaddiset, Rowdeliset, Refreshiset;
    QIconSet Synciset, Reportiset;

    // Construct iconsets to use later for multistate action images
    Saveiset.setPixmap(QPixmap(s_kpcFilesave_xpm), QIconSet::Automatic, QIconSet::Normal);
    Saveiset.setPixmap(QPixmap(s_kpcDfilesave_xpm), QIconSet::Automatic, QIconSet::Disabled);
    Cutiset.setPixmap(QPixmap(s_kpcCut_xpm), QIconSet::Automatic, QIconSet::Normal);
    Cutiset.setPixmap(QPixmap(s_kpcDcut_xpm), QIconSet::Automatic, QIconSet::Disabled);
    Copyiset.setPixmap(QPixmap(s_kpcCopy_xpm), QIconSet::Automatic, QIconSet::Normal);
    Copyiset.setPixmap(QPixmap(s_kpcDcopy_xpm), QIconSet::Automatic, QIconSet::Disabled);
    Pasteiset.setPixmap(QPixmap(s_kpcPaste_xpm), QIconSet::Automatic, QIconSet::Normal);
    Pasteiset.setPixmap(QPixmap(s_kpcDpaste_xpm), QIconSet::Automatic, QIconSet::Disabled);
    Rowaddiset.setPixmap(QPixmap(s_kpcRowadd_xpm), QIconSet::Automatic, QIconSet::Normal);
    Rowaddiset.setPixmap(QPixmap(s_kpcDrowadd_xpm), QIconSet::Automatic, QIconSet::Disabled);
    Rowdeliset.setPixmap(QPixmap(s_kpcRowdel_xpm), QIconSet::Automatic, QIconSet::Normal);
    Rowdeliset.setPixmap(QPixmap(s_kpcDrowdel_xpm), QIconSet::Automatic, QIconSet::Disabled);
    Refreshiset.setPixmap(QPixmap(s_kpcRefresh_xpm), QIconSet::Automatic, QIconSet::Normal);
    Refreshiset.setPixmap(QPixmap(s_kpcDrefresh_xpm), QIconSet::Automatic, QIconSet::Disabled);
    Synciset.setPixmap(QPixmap(s_kpcSync_xpm), QIconSet::Automatic, QIconSet::Normal);
    Synciset.setPixmap(QPixmap(s_kpcDsync_xpm), QIconSet::Automatic, QIconSet::Disabled);
    Reportiset.setPixmap(QPixmap(s_kpcReport_xpm), QIconSet::Automatic, QIconSet::Normal);
    Reportiset.setPixmap(QPixmap(s_kpcDreport_xpm), QIconSet::Automatic, QIconSet::Disabled);

    // First associate the graphics with MIME types
    QMimeSourceFactory::defaultFactory()->setPixmap("osspicon", QPixmap(s_kpcOsspicon_xpm));
    QMimeSourceFactory::defaultFactory()->setPixmap("filenew", QPixmap(s_kpcFilenew_xpm));
    QMimeSourceFactory::defaultFactory()->setPixmap("fileopen", QPixmap(s_kpcFileopen_xpm));
    QMimeSourceFactory::defaultFactory()->setPixmap("filesave", QPixmap(s_kpcFilesave_xpm));
    QMimeSourceFactory::defaultFactory()->setPixmap("cut", QPixmap(s_kpcCut_xpm));
    QMimeSourceFactory::defaultFactory()->setPixmap("copy", QPixmap(s_kpcCopy_xpm));
    QMimeSourceFactory::defaultFactory()->setPixmap("paste", QPixmap(s_kpcPaste_xpm));
    QMimeSourceFactory::defaultFactory()->setPixmap("rowadd", QPixmap(s_kpcRowadd_xpm));
    QMimeSourceFactory::defaultFactory()->setPixmap("rowdel", QPixmap(s_kpcRowdel_xpm));
    QMimeSourceFactory::defaultFactory()->setPixmap("refresh", QPixmap(s_kpcRefresh_xpm));
    QMimeSourceFactory::defaultFactory()->setPixmap("syncrpc", QPixmap(s_kpcSync_xpm));
    QMimeSourceFactory::defaultFactory()->setPixmap("locreport", QPixmap(s_kpcReport_xpm));
    QMimeSourceFactory::defaultFactory()->setPixmap("prefs", QPixmap(s_kpcPrefs_xpm));

    // File new action
    m_pFilenewact = new QAction(trUtf8("New File"), QPixmap(s_kpcFilenew_xpm), trUtf8("&New"), CTRL+Key_N, this, "New");
    if (m_pFilenewact == NULL)  // Sanity check
        throw Genexcept("Main window file new action creation failed.");
    connect(m_pFilenewact, SIGNAL(activated()), SLOT(newDoc()));
    const char *kszFilenewtext = "<p><img source=\"filenew\"> "
                                        "Click this button to make a "
                                        "<em>blank file</em>. You can also "
                                        "select the <b>New</b> command from "
                                        "the <b>File</b> menu.</p>";
    m_pFilenewact->setWhatsThis(kszFilenewtext);

    // File open action
    m_pFileopenact = new QAction(trUtf8("Open File"), QPixmap(s_kpcFileopen_xpm), trUtf8("&Open..."), CTRL+Key_O, this, "Open");
    if (m_pFileopenact == NULL) // Sanity check
        throw Genexcept("Main window file open action creation failed.");
    connect(m_pFileopenact, SIGNAL(activated()), SLOT(openDoc()));
    const char *kszFileopentext = "<p><img source=\"fileopen\"> "
                                  "Click this button to open a "
                                  "<em>new file</em>. You can also select "
                                  "the <b>Open</b> command from the "
                                  "<b>File</b> menu.</p>";
    m_pFileopenact->setWhatsThis(kszFileopentext);

    // File save current action
    m_pFilesaveact = new QAction(trUtf8("Save File"), Saveiset, trUtf8("&Save"), CTRL+Key_S, this, "Save");
    if (m_pFilesaveact == NULL) // Sanity check
        throw Genexcept("Main window file save action creation failed.");
    connect(m_pFilesaveact, SIGNAL(activated()), SLOT(saveFile()));
    const char *kszFilesavetext = "<p><img source=\"filesave\"> "
                                  "Click this button to <em>save</em> "
                                  "the file you are editing. You will be "
                                  "prompted for a file name. You can also "
                                  "select the<b> Save</b> command from "
                                  "the <b>File</b> menu.</p>";
    m_pFilesaveact->setWhatsThis(kszFilesavetext);

    // File save selected action
    m_pFilesaveasact = new QAction(trUtf8("Save File As"), trUtf8("Save &As..."), 0, this, "SaveAs");
    if (m_pFilesaveasact == NULL)   // Sanity check
        throw Genexcept("Main window file save as action creation failed.");
    connect(m_pFilesaveasact, SIGNAL(activated()), SLOT(saveAs()));
    m_pFilesaveasact->setWhatsThis(kszFilesavetext);

    // File close current action
    m_pFilecloseact = new QAction(trUtf8("Close"), trUtf8("&Close"), CTRL+Key_W, this, "Close");
    if (m_pFilecloseact == NULL)    // Sanity check
        throw Genexcept("Main window file close action creation failed.");
    connect(m_pFilecloseact, SIGNAL(activated()), SLOT(close()));

    // File quit action
    m_pFilequitact = new QAction(trUtf8("Exit"), trUtf8("E&xit"), CTRL+Key_Q, this, "Exit");
    if (m_pFilequitact == NULL)     // Sanity check
        throw Genexcept("Main window file quit action creation failed.");
    connect(m_pFilequitact, SIGNAL(activated()), SLOT(quitApp()));

    // Cut action
    m_pCutact = new QAction(trUtf8("Cut"), Cutiset, trUtf8("&Cut"), CTRL+Key_X, this, "Cut");
    if (m_pCutact == NULL)      // Sanity check
        throw Genexcept("Main window cut edit action creation failed.");
    connect(m_pCutact, SIGNAL(activated()), SLOT(cutEntry()));
    const char *kszCuttext = "<p><img source=\"cut\"> "
                                   "Click this button to cut an <em>entry</em>. "
                                   "You can also select the <b>Cut</b> command "
                                   "from the <b>Edit</b> menu.</p>";
    m_pCutact->setWhatsThis(kszCuttext);

    // Copy action
    m_pCopyact = new QAction(trUtf8("Copy"), Copyiset, trUtf8("&Copy"), CTRL+Key_C, this, "Copy");
    if (m_pCopyact == NULL)     // Sanity check
        throw Genexcept("Main window copy edit action creation failed.");
    connect(m_pCopyact, SIGNAL(activated()), SLOT(copyEntry()));
    const char *kszCopytext = "<p><img source=\"copy\"> "
                                   "Click this button to copy an <em>entry</em>. "
                                   "You can also select the <b>Copy</b> command "
                                   "from the <b>Edit</b> menu.</p>";
    m_pCopyact->setWhatsThis(kszCopytext);

    // Paste action
    m_pPasteact = new QAction(trUtf8("Paste"), Pasteiset, trUtf8("&Paste"), CTRL+Key_V, this, "Paste");
    if (m_pPasteact == NULL)    // Sanity check
        throw Genexcept("Main window paste edit action creation failed.");
    connect(m_pPasteact, SIGNAL(activated()), SLOT(pasteEntry()));
    const char *kszPastetext = "<p><img source=\"paste\"> "
                                   "Click this button to paste an <em>entry</em>. "
                                   "You can also select the <b>Paste</b> command "
                                   "from the <b>Edit</b> menu.</p>";
    m_pPasteact->setWhatsThis(kszPastetext);

    // Add data row action
    m_pAddrowact = new QAction(trUtf8("Add Entry"), Rowaddiset, trUtf8("&Add entry"), Key_Insert, this, "Addentry");
    if (m_pAddrowact == NULL)   // Sanity check
        throw Genexcept("Main window add entry action creation failed.");
    connect(m_pAddrowact, SIGNAL(activated()), SLOT(addEntry()));
    const char *kszAddrowtext = "<p><img source=\"rowadd\"> "
                                "Click this button to add a <em>new entry</em>. "
                                "You can also select the <b>Add</b> command "
                                "from the <b>Edit</b> menu.</p>";
    m_pAddrowact->setWhatsThis(kszAddrowtext);

    // Delete data row action
    m_pDelrowact = new QAction(trUtf8("Delete Entry"), Rowdeliset, trUtf8("&Delete entry"), Key_Delete, this, "Delentry");
    if (m_pDelrowact == NULL)   // Sanity check
        throw Genexcept("Main window delete entry action creation failed.");
    connect(m_pDelrowact, SIGNAL(activated()), SLOT(delEntry()));
    const char *kszDelrowtext = "<p><img source=\"rowdel\"> "
                                   "Click this button to delete a <em>entry</em>. "
                                   "You can also select the <b>Delete</b> command "
                                   "from the <b>Edit</b> menu.</p>";
    m_pDelrowact->setWhatsThis(kszDelrowtext);

    // Refresh data display action
    m_pRefreshact = new QAction(trUtf8("Refresh Display"), Refreshiset, trUtf8("&Refresh display"), CTRL+Key_R, this, "Refresh");
    if (m_pRefreshact == NULL)  // Sanity check
        throw Genexcept("Main window refresh action creation failed.");
    connect(m_pRefreshact, SIGNAL(activated()), SLOT(refreshDisplay()));
    const char *kszRefreshtext = "<p><img source=\"refresh\"> "
                                 "Click this button to <em>refresh the display</em>. "
                                 "You can also select the <b>Refresh</b> command "
                                 "from the <b>View</b> menu.</p>";
    m_pRefreshact->setWhatsThis(kszRefreshtext);

    // Syncronize data with remote server over IIOP or SOAP action
    m_pSyncact = new QAction(trUtf8("Synchronize with server"), Synciset, trUtf8("&Synchronize"), CTRL+Key_E, this, "Synchronize");
    if (m_pSyncact == NULL) // Sanity check
        throw Genexcept("Main window synchronize RPC action creation failed.");
    connect(m_pSyncact, SIGNAL(activated()), SLOT(syncIiop()));
    connect(m_pSyncact, SIGNAL(activated()), SLOT(syncSoap()));
    const char *kszSynctext = "<p><img source=\"syncrpc\"> "
                              "Click this button to <em>synchronize the data</em>. "
                              "Your changed entries will be sent to the server. "
                              "You can also select the <b>Synchronize</b> command "
                              "from the <b>Report</b> menu.</p>";
    m_pSyncact->setWhatsThis(kszSynctext);

    // Local report generation action
    m_pReportact = new QAction(trUtf8("Generate a local report"), Reportiset, trUtf8("&Report"), CTRL+Key_T, this, "LocalReport");
    if (m_pReportact == NULL) // Sanity check
        throw Genexcept("Main window local report action creation failed.");
    connect(m_pReportact, SIGNAL(activated()), SLOT(genReport()));
    const char *kszReporttext = "<p><img source=\"locreport\"> "
                                "Click this button to <em>report your data</em>. "
                                "A local report will be generated for you to view. "
                                "You can also select the <b>Local Report</b> command "
                                "from the <b>Report</b> menu.</p>";
    m_pReportact->setWhatsThis(kszReporttext);

    // Offer a formatted preferences panel for intuitive prefs object editing
    m_pPrefsact = new QAction(trUtf8("Edit the preferences"), QPixmap(s_kpcPrefs_xpm), trUtf8("P&references..."), CTRL+Key_F, this, "Prefspanel");
    if (m_pPrefsact == NULL) // Sanity check
        throw Genexcept("Main window preferences panel action creation failed.");
    connect(m_pPrefsact, SIGNAL(activated()), SLOT(configPrefs()));
    const char *kszPrefstext = "<p><img source=\"prefs\"> "
                               "Click this button to <em>edit the preferences</em>. "
                               "You can then apply, accept, or cancel your changes. "
                               "You can also select the <b>Preferences</b> command "
                               "from the <b>Edit</b> menu.</p>";
    m_pPrefsact->setWhatsThis(kszPrefstext);
}

//
// Construct the menu bar
//
void Titraqform::setupMenubar(void)
{
    int nMenuid = 0;        // Used to store id during menu item creation

    m_pMenubar = menuBar(); // Grab menu bar owned by QMainWindow
    if (m_pMenubar == NULL) // Sanity check
        throw Genexcept("Main window menu bar nonexistant.");

//    // Make an easter egg ;-)
//    QToolTip::add(m_pMenubar, QRect(0, 0, 2, 2), trUtf8("Easter Egg"));

    // Construct and populate the file menu with actions
    QPopupMenu *pFilepopup = new QPopupMenu(this);
    if (pFilepopup == NULL) // Sanity check
        throw Genexcept("Main window file popup creation failed.");
    m_pMenubar->insertItem(trUtf8("&File"), pFilepopup);
    m_pFilenewact->addTo(pFilepopup);
    m_pFileopenact->addTo(pFilepopup);
    pFilepopup->insertSeparator();
    m_pFilecloseact->addTo(pFilepopup);
    pFilepopup->insertSeparator();
    m_pFilesaveact->addTo(pFilepopup);
    m_pFilesaveasact->addTo(pFilepopup);
    pFilepopup->insertSeparator();
    m_pFilequitact->addTo(pFilepopup);

    // Construct and populate the edit menu with subitems
    QPopupMenu *pEditpopup = new QPopupMenu(this);
    if (pEditpopup == NULL) // Sanity check
        throw Genexcept("Main window edit popup creation failed.");
    m_pMenubar->insertItem(trUtf8("&Edit"), pEditpopup);
    m_pCutact->addTo(pEditpopup);
    m_pCopyact->addTo(pEditpopup);
    m_pPasteact->addTo(pEditpopup);
    pEditpopup->insertSeparator();
    m_pAddrowact->addTo(pEditpopup);
    m_pDelrowact->addTo(pEditpopup);
    pEditpopup->insertSeparator();
    m_pPrefsact->addTo(pEditpopup);

    // Construct the view menu and submenus
    m_pViewpopup = new QPopupMenu(this);
    m_pTbarspopup = new QPopupMenu(this);
    m_pColspopup = new QPopupMenu(this);

    // Give the new menus krass attributes
    m_pTbarspopup->insertTearOffHandle();
    m_pTbarspopup->setCheckable(true);
    m_pColspopup->insertTearOffHandle();
    m_pColspopup->setCheckable(true);

    // Major sanity check wrapped in one call
    if (!(m_pViewpopup && m_pTbarspopup && m_pColspopup))
        throw Genexcept("Main window view popups creation failed.");

    // Populate the view menu with subitems
    m_pMenubar->insertItem(trUtf8("&View"), m_pViewpopup);
    m_pViewpopup->insertItem(trUtf8("&Normal"), this, SLOT(normalView()));
    nMenuid = m_pViewpopup->insertItem(trUtf8("&Editing"), this, SLOT(editingView()));
    m_pViewpopup->setItemEnabled(nMenuid, false);
    nMenuid = m_pViewpopup->insertItem(trUtf8("&Timing"), this, SLOT(timingView()));
    m_pViewpopup->setItemEnabled(nMenuid, false);
    m_pViewpopup->insertSeparator();
    m_pRefreshact->addTo(m_pViewpopup); // Refresh action
    m_pViewpopup->insertSeparator();
    m_pViewpopup->insertItem(trUtf8("&Toolbars"), m_pTbarspopup);
    nMenuid = m_pTbarspopup->insertItem(trUtf8("&File"), this, SLOT(showFilebar()));
    m_pTbarspopup->setItemChecked(nMenuid, true);
    nMenuid = m_pTbarspopup->insertItem(trUtf8("&Edit"), this, SLOT(showEditbar()));
    m_pTbarspopup->setItemChecked(nMenuid, true);
    nMenuid = m_pTbarspopup->insertItem(trUtf8("&View"), this, SLOT(showViewbar()));
    m_pTbarspopup->setItemChecked(nMenuid, true);
    nMenuid = m_pTbarspopup->insertItem(trUtf8("&Prefs"), this, SLOT(showPrefsbar()));
    m_pTbarspopup->setItemChecked(nMenuid, true);
    nMenuid = m_pTbarspopup->insertItem(trUtf8("&Whats"), this, SLOT(showWhatsbar()));
    m_pTbarspopup->setItemChecked(nMenuid, true);
    m_pViewpopup->insertItem(trUtf8("&Columns"), m_pColspopup);
    nMenuid = m_pColspopup->insertItem(trUtf8("&Status"), this, SLOT(showStatcol()));
    m_pColspopup->setItemChecked(nMenuid, true);
    nMenuid = m_pColspopup->insertItem(trUtf8("&Line"), this, SLOT(showLinecol()));
    m_pColspopup->setItemChecked(nMenuid, true);
    nMenuid = m_pColspopup->insertItem(trUtf8("&User"), this, SLOT(showUsercol()));
    m_pColspopup->setItemChecked(nMenuid, true);
    nMenuid = m_pColspopup->insertItem(trUtf8("&GUID"), this, SLOT(showGuidcol()));
    m_pColspopup->setItemChecked(nMenuid, true);
    nMenuid = m_pColspopup->insertItem(trUtf8("&CRC"), this, SLOT(showCrccol()));
    m_pColspopup->setItemChecked(nMenuid, true);
    nMenuid = m_pColspopup->insertItem(trUtf8("&Rev"), this, SLOT(showRevcol()));
    m_pColspopup->setItemChecked(nMenuid, true);
    nMenuid = m_pColspopup->insertItem(trUtf8("&Date"), this, SLOT(showDatecol()));
    m_pColspopup->setItemChecked(nMenuid, true);
    nMenuid = m_pColspopup->insertItem(trUtf8("&Start"), this, SLOT(showStartcol()));
    m_pColspopup->setItemChecked(nMenuid, true);
    nMenuid = m_pColspopup->insertItem(trUtf8("&Finish"), this, SLOT(showFinishcol()));
    m_pColspopup->setItemChecked(nMenuid, true);
    nMenuid = m_pColspopup->insertItem(trUtf8("&Amount"), this, SLOT(showAmountcol()));
    m_pColspopup->setItemChecked(nMenuid, true);
    nMenuid = m_pColspopup->insertItem(trUtf8("&Task"), this, SLOT(showTaskcol()));
    m_pColspopup->setItemChecked(nMenuid, true);
    nMenuid = m_pColspopup->insertItem(trUtf8("&Remark"), this, SLOT(showRemarkcol()));
    m_pColspopup->setItemChecked(nMenuid, true);

    // Construct and populate the report menu with subitems
    QPopupMenu *pReportpopup = new QPopupMenu(this);
    if (pReportpopup == NULL) // Sanity check
        throw Genexcept("Main window report popup creation failed.");
    m_pMenubar->insertItem(trUtf8("&Report"), pReportpopup);
#if defined HAVE_MICO || defined HAVE_ESOAP
    m_pSyncact->addTo(pReportpopup);
    m_pSyncact->setEnabled(m_pPrefs->getBool(TITRAQ_PREFCORBON, TITRAQ_DEFCORBON)
        | m_pPrefs->getBool(TITRAQ_PREFSOAPON, TITRAQ_DEFSOAPON));
#endif // defined HAVE_MICO || defined HAVE_ESOAP
    m_pReportact->addTo(pReportpopup);

    // Pad spacing to force help menu to appear far right
    m_pMenubar->insertSeparator();

    // Construct and populate the help menu with subitems
    QPopupMenu *pHelppopup = new QPopupMenu(this);
    if (pHelppopup == NULL) // Sanity check
        throw Genexcept("Main window help popup creation failed.");
    m_pMenubar->insertItem(trUtf8("&Help"), pHelppopup);
    pHelppopup->insertItem(trUtf8("&Contents"), this, SLOT(helpContents()), Key_F1);
    pHelppopup->insertSeparator();
    pHelppopup->insertItem(trUtf8("About &as-gui"), this, SLOT(aboutTitraq()));
    pHelppopup->insertItem(trUtf8("About &OSSP"), this, SLOT(aboutOSSP()));
    pHelppopup->insertItem(trUtf8("About &Qt"), this, SLOT(aboutQt()));
    pHelppopup->insertSeparator();
    pHelppopup->insertItem(QPixmap(s_kpcWhatsthis_xpm), trUtf8("What's &This"), this, SLOT(whatsThis()), SHIFT+Key_F1);
}

//
// Construct the tool bars
//
void Titraqform::setupToolbars(void)
{
    // Construct and populate the file tool bar
    m_pFiletools = new QToolBar("Toolfile", this, DockTop);
    if (m_pFiletools == NULL)                   // Sanity check
        throw Genexcept("File tool bar creation failed.");
    m_pFiletools->setLabel(trUtf8("File Ops"));
    m_pFiletools->setOpaqueMoving(false);
    m_pFiletools->setCloseMode(QDockWindow::Never);
    m_pFilenewact->addTo(m_pFiletools);
    m_pFileopenact->addTo(m_pFiletools);
    m_pFilesaveact->addTo(m_pFiletools);

    // Construct and populate the edit tool bar
    m_pEdittools = new QToolBar("Tooledit", this, DockTop);
    if (m_pEdittools == NULL)                   // Sanity check
        throw Genexcept("Edit tool bar creation failed.");
    m_pEdittools->setLabel(trUtf8("Edit Ops"));
    m_pEdittools->setOpaqueMoving(false);
    m_pEdittools->setCloseMode(QDockWindow::Never);
    m_pCutact->addTo(m_pEdittools);
    m_pCopyact->addTo(m_pEdittools);
    m_pPasteact->addTo(m_pEdittools);
    m_pEdittools->addSeparator();
    m_pAddrowact->addTo(m_pEdittools);
    m_pDelrowact->addTo(m_pEdittools);

    // Construct and populate the view tool bar
    m_pViewtools = new QToolBar("Toolview", this, DockTop);
    if (m_pViewtools == NULL)                   // Sanity check
        throw Genexcept("View tool bar creation failed.");
    m_pViewtools->setLabel(trUtf8("View Ops"));
    m_pViewtools->setOpaqueMoving(false);
    m_pViewtools->setCloseMode(QDockWindow::Never);
    m_pRefreshact->addTo(m_pViewtools);
    m_pReportact->addTo(m_pViewtools);
    m_pSyncact->addTo(m_pViewtools);

    // Construct and populate the lonely preferences tool bar
    m_pPrefstools = new QToolBar("Preferences", this, DockTop);
    if (m_pPrefstools == NULL)                   // Sanity check
        throw Genexcept("Prefs tool bar creation failed.");
    m_pPrefstools->setLabel(trUtf8("Preferences editor"));
    m_pPrefstools->setOpaqueMoving(false);
    m_pPrefstools->setCloseMode(QDockWindow::Never);
    m_pPrefsact->addTo(m_pPrefstools);

    // Construct and populate the lonely whatsthis tool bar
    m_pWhatstools = new QToolBar("Toolwhats", this, DockTop);
    if (m_pWhatstools == NULL)                  // Sanity check
        throw Genexcept("Whats this tool bar creation failed.");
    m_pWhatstools->setLabel(trUtf8("Whats this"));
    m_pWhatstools->setOpaqueMoving(false);
    m_pWhatstools->setCloseMode(QDockWindow::Never);
    QWhatsThis::whatsThisButton(m_pWhatstools); // Preconfigured whats this button
}

//
// Construct the central frame
//
void Titraqform::setupCentralwidget(void)
{
    // Create a central frame and associated layout for QMainWindow
    m_pCenframe = new QFrame(this, "Centralframe");
    if (m_pCenframe == NULL)    // Sanity check
        throw Genexcept("Main window central frame creation failed.");
    m_pCenframe->setFrameShape(QFrame::StyledPanel);
    m_pCenframe->setFrameShadow(QFrame::Sunken);
    this->setCentralWidget(m_pCenframe);

    // Layout controls for table, editing widgets
    m_pMainlayout = new QVBoxLayout(m_pCenframe, TITRAQ_MARGIN, TITRAQ_SPACING, "Mainlayout");  // For layouts
    m_pPackagelayout = new QVBoxLayout(0, 0, TITRAQ_SPACING, "Packagelayout");  // For table
    m_pEditlayout = new QHBoxLayout(0, 0, TITRAQ_SPACING, "Editlayout");        // For edits

    // Major sanity check wrapped in one call
    if (!(m_pMainlayout && m_pPackagelayout && m_pEditlayout))
        throw Genexcept("Main window layout creation failed.");

    // Specify ordering of the layouts
    m_pMainlayout->addLayout(m_pPackagelayout);
    m_pMainlayout->addLayout(m_pEditlayout);
}

//
// Construct the status bar
//
void Titraqform::setupStatusbar(void)
{
    m_pStatbar = statusBar();   // Grab status bar owned by QMainWindow
    if (m_pStatbar == NULL)     // Sanity check
        throw Genexcept("Main window status bar nonexistant.");
}

//
// Construct the table
//
void Titraqform::setupTable(void)
{
    // Create, configure, and setup the table itself
    m_pMaintable = new TiTable(m_pPrefs, m_pCenframe, "Maintable");
    if (m_pMaintable == NULL)   // Sanity check
        throw Genexcept("Main window table creation failed.");
    m_pMaintable->setNumCols(TITRAQ_IDXTAIL);
    m_pMaintable->setDirty(false);              // Reset data to clean state
    m_pMaintable->setReadOnly(true);                    // Table is read only
    m_pMaintable->setColumnMovingEnabled(false);        // Ctrl-drag disabled
    m_pMaintable->setSelectionMode(QTable::MultiRow);   // Multi row selection
    m_pMaintable->setFocusStyle(QTable::FollowStyle);   // How cells are drawn
    m_pMaintable->setLeftMargin(0);             // Get rid of the vertical header
    m_pMaintable->verticalHeader()->hide();     // by hiding it with a margin of 0
    m_pMaintable->horizontalHeader()->setResizeEnabled(true);
    m_pMaintable->horizontalHeader()->setClickEnabled(true); // Allow click signals
    m_pMaintable->horizontalHeader()->setTracking(false); // No continuous tracking
    m_pMaintable->setColumnStretchable(TITRAQ_IDXTAIL - 1, true);

    // Allow for user determined data sorting and use saved values
    m_pMaintable->setSortcol((int)m_pPrefs->getNumber(TITRAQ_PREFSORTCOL, TITRAQ_DEFSORTCOL));
    m_pMaintable->setSortdir(m_pPrefs->getBool(TITRAQ_PREFSORTDIR, TITRAQ_DEFSORTDIR));
    m_pMaintable->setSorting(true);

    // Table header row
    m_pTablehead = m_pMaintable->horizontalHeader();
    m_pMaintable->setHScrollBarMode(QScrollView::AlwaysOff);
    m_pTablehead->setLabel(TITRAQ_IDXSTATUS, QObject::trUtf8("Stat"),
        m_pPrefs->getNumber(TITRAQ_PREFSTATCOLWIDTH, TITRAQ_DEFSTATCOLWIDTH));
    m_pMaintable->setColumnReadOnly (TITRAQ_IDXSTATUS, true);
    m_pTablehead->setLabel(TITRAQ_IDXLINE, QObject::trUtf8("Line"),
        m_pPrefs->getNumber(TITRAQ_PREFLCOLWIDTH, TITRAQ_DEFLCOLWIDTH));
    m_pMaintable->setColumnReadOnly (TITRAQ_IDXLINE, true);
    m_pTablehead->setLabel(TITRAQ_IDXUSER, QObject::trUtf8("User"),
        m_pPrefs->getNumber(TITRAQ_PREFUCOLWIDTH, TITRAQ_DEFUCOLWIDTH));
    m_pTablehead->setLabel(TITRAQ_IDXGUID, QObject::trUtf8("GUID"),
        m_pPrefs->getNumber(TITRAQ_PREFGCOLWIDTH, TITRAQ_DEFGCOLWIDTH));
    m_pMaintable->setColumnReadOnly (TITRAQ_IDXGUID, true);
    m_pTablehead->setLabel(TITRAQ_IDXCRC, QObject::trUtf8("CRC"),
        m_pPrefs->getNumber(TITRAQ_PREFCCOLWIDTH, TITRAQ_DEFCCOLWIDTH));
    m_pMaintable->setColumnReadOnly (TITRAQ_IDXCRC, true);
    m_pTablehead->setLabel(TITRAQ_IDXREV, QObject::trUtf8("Rev"),
        m_pPrefs->getNumber(TITRAQ_PREFREVCOLWIDTH, TITRAQ_DEFREVCOLWIDTH));
    m_pMaintable->setColumnReadOnly (TITRAQ_IDXREV, true);
    m_pTablehead->setLabel(TITRAQ_IDXDATE, QObject::trUtf8("Date"),
        m_pPrefs->getNumber(TITRAQ_PREFDCOLWIDTH, TITRAQ_DEFDCOLWIDTH));
    m_pTablehead->setLabel(TITRAQ_IDXSTART, QObject::trUtf8("Begin"),
        m_pPrefs->getNumber(TITRAQ_PREFSTARTCOLWIDTH, TITRAQ_DEFSTARTCOLWIDTH));
    m_pTablehead->setLabel(TITRAQ_IDXFINISH, QObject::trUtf8("End"),
        m_pPrefs->getNumber(TITRAQ_PREFFCOLWIDTH, TITRAQ_DEFFCOLWIDTH));
    m_pTablehead->setLabel(TITRAQ_IDXAMOUNT, QObject::trUtf8("Amount"),
        m_pPrefs->getNumber(TITRAQ_PREFACOLWIDTH, TITRAQ_DEFACOLWIDTH));
    m_pTablehead->setLabel(TITRAQ_IDXTASK, QObject::trUtf8("Task"),
        m_pPrefs->getNumber(TITRAQ_PREFTCOLWIDTH, TITRAQ_DEFTCOLWIDTH));
    m_pTablehead->setLabel(TITRAQ_IDXREMARK, QObject::trUtf8("Remark"));

    m_pPackagelayout->addWidget(m_pMaintable);  // Finally add the damn table

    // Table update signals
    connect(m_pMaintable, SIGNAL(currentChanged(int, int)), SLOT(updEdit(int, int)));
    connect(m_pMaintable, SIGNAL(clicked(int, int, int, const QPoint&)), SLOT(onClick(int, int, int, const QPoint&)));
    connect(m_pMaintable, SIGNAL(doubleClicked(int, int, int, const QPoint&)), SLOT(inplaceEdit(int, int, int, const QPoint&)));
    connect(m_pTablehead, SIGNAL(sizeChange(int, int, int)), SLOT(updSizes(int, int, int)));
    connect(m_pMaintable, SIGNAL(textEdited(int, int)), SLOT(dataChanged(int, int)));
    connect(m_pMaintable, SIGNAL(valueChanged(int, int)), SLOT(dataChanged(int, int)));
}

//
// Construct the bottom edit widgets
//
void Titraqform::setupEditlay(void)
{
//    QSpacerItem Pad(TITRAQ_SPACING / 2, 0, QSizePolicy::Fixed);
//    m_pEditlayout->addWidget(&Pad); // Padd left side of control layout

    // Make the status edit
    m_pStatusedit = new QLabel(m_pCenframe, "Status");
    if (m_pStatusedit == NULL) // Sanity check
        throw Genexcept("Main window status edit creation failed.");
    m_pStatusedit->setPixmap(QPixmap(s_kpcStatvoid_xpm));   // Placeholder


    // Whatsthis info for the item status edit
    const char *kszStatustext = "The <em>item status</em> shows a green "
                                "symbol for valid entries, a yellow symbol "
                                "for warning, and a red symbol for flawed entries.";
    QWhatsThis::add(m_pStatusedit, kszStatustext);
    QToolTip::add(m_pStatusedit, trUtf8("Status Indicator"));
    m_pEditlayout->addWidget(m_pStatusedit);  // Finally add the item status edit

    // Make the line number edit
    m_pLineedit = new QLineEdit(m_pCenframe, "Lineno");
    if (m_pLineedit == NULL) // Sanity check
        throw Genexcept("Main window line number edit creation failed.");
    m_pLineedit->setFrameShape(QLineEdit::LineEditPanel);
    m_pLineedit->setFrameShadow(QLineEdit::Sunken);
    m_pLineedit->setFrame(true);
    m_pLineedit->setReadOnly(true);
    m_pLineedit->setEnabled(false);
    m_pLineedit->setAlignment(Qt::AlignRight);

    // Whatsthis info for the line number edit
    const char *kszLinenotext = "The <em>line number</em> helps identify "
                                "an entry when a warning or error message "
                                "appears.";
    QWhatsThis::add(m_pLineedit, kszLinenotext);
    QToolTip::add(m_pLineedit, trUtf8("Line Number"));
    m_pEditlayout->addWidget(m_pLineedit);  // Finally add the line number edit

    // Make the user edit
    m_pUseredit = new QLineEdit(m_pCenframe, "User");
    if (m_pUseredit == NULL) // Sanity check
        throw Genexcept("Main window user edit creation failed.");
    m_pUseredit->setFrameShape(QLineEdit::LineEditPanel);
    m_pUseredit->setFrameShadow(QLineEdit::Sunken);
    m_pUseredit->setFrame(true);
    m_pUseredit->setReadOnly(false);
    m_pUseredit->setEnabled(false);

    // Whatsthis info for the user edit
    const char *kszUsertext = "Edit the <em>user name</em> by clicking "
                              "on this control and typing the user name "
                              "you want to account.";
    QWhatsThis::add(m_pUseredit, kszUsertext);
    QToolTip::add(m_pUseredit, trUtf8("User Name"));
    m_pEditlayout->addWidget(m_pUseredit);  // Finally add the user edit

    // Make the GUID edit
    m_pGuidedit = new QLineEdit(m_pCenframe, "Guid");
    if (m_pGuidedit == NULL) // Sanity check
        throw Genexcept("Main window GUID edit creation failed.");
    m_pGuidedit->setFrameShape(QLineEdit::LineEditPanel);
    m_pGuidedit->setFrameShadow(QLineEdit::Sunken);
    m_pGuidedit->setFrame(true);
    m_pGuidedit->setReadOnly(true);
    m_pGuidedit->setEnabled(false);

    // Whatsthis info for the GUID edit
    const char *kszGuidtext = "Edit the <em>GUID</em> by clicking "
                              "on this control and typing the GUID "
                              "corresponding to this account item.";
    QWhatsThis::add(m_pGuidedit, kszGuidtext);
    QToolTip::add(m_pGuidedit, trUtf8("GUID"));
    m_pEditlayout->addWidget(m_pGuidedit);  // Finally add the GUID edit

    // Make the CRC edit
    m_pCrcedit = new QLineEdit(m_pCenframe, "Crc");
    if (m_pCrcedit == NULL) // Sanity check
        throw Genexcept("Main window CRC edit creation failed.");
    m_pCrcedit->setFrameShape(QLineEdit::LineEditPanel);
    m_pCrcedit->setFrameShadow(QLineEdit::Sunken);
    m_pCrcedit->setFrame(true);
    m_pCrcedit->setReadOnly(true);
    m_pCrcedit->setEnabled(false);

    // Whatsthis info for the CRC edit
    const char *kszCrctext = "Edit the <em>CRC</em> by clicking "
                             "on this control and typing the new "
                             "value for this account item.";
    QWhatsThis::add(m_pCrcedit, kszCrctext);
    QToolTip::add(m_pCrcedit, trUtf8("CRC"));
    m_pEditlayout->addWidget(m_pCrcedit);  // Finally add the CRC edit

    // Make the revision edit
    m_pRevedit = new QLineEdit(m_pCenframe, "Revision");
    if (m_pRevedit == NULL) // Sanity check
        throw Genexcept("Main window revision edit creation failed.");
    m_pRevedit->setFrameShape(QLineEdit::LineEditPanel);
    m_pRevedit->setFrameShadow(QLineEdit::Sunken);
    m_pRevedit->setFrame(true);
    m_pRevedit->setReadOnly(true);
    m_pRevedit->setEnabled(false);

    // Whatsthis info for the revision edit
    const char *kszRevtext = "Edit the <em>revision</em> by clicking "
                             "on this control and typing the new value "
                             "for this account type.";
    QWhatsThis::add(m_pRevedit, kszRevtext);
    QToolTip::add(m_pRevedit, trUtf8("Revision"));
    m_pEditlayout->addWidget(m_pRevedit);  // Finally add the revision edit

    // Make the date selector
    m_pDateedit = new Daydatedit(QDate::currentDate(), m_pCenframe, "Date");
    if (m_pDateedit == NULL)                                        // Sanity check
        throw Genexcept("Main window date edit creation failed.");  // Spew errors

    // Configure attributes
    m_pDateedit->setOrder(QDateEdit::YMD);
    m_pDateedit->setAutoAdvance(true);
    m_pDateedit->setSeparator(trUtf8("-"));
    m_pDateedit->setEnabled(false);

    // Whatsthis info for the date editor
    const char *kszDateedit = "Edit the <em>task date</em> by clicking on "
                              "the year, month, or day, and then changing their "
                              "values with the arrow buttons.";
    QWhatsThis::add(m_pDateedit, kszDateedit);
    QToolTip::add(m_pDateedit, trUtf8("Task Date"));
    m_pEditlayout->addWidget(m_pDateedit);                          // Finally add the date editor

    // Make the start time selector
    m_pStarttime = new QTimeEdit(m_pCenframe, "StartTime");
    if (m_pStarttime == NULL)                                       // Sanity check
        throw Genexcept("Main window start time creation failed."); // Spew errors

    // Configure attributes
    m_pStarttime->setDisplay(QTimeEdit::Hours | QTimeEdit::Minutes);
    m_pStarttime->setAutoAdvance(true);
    m_pStarttime->setEnabled(false);

    // Whatsthis info for the time editor
    const char *kszStarttime = "Edit the <em>task starting time</em> by clicking on "
                               "the hour and minute, and then changing their "
                               "values with the arrow buttons.";
    QWhatsThis::add(m_pStarttime, kszStarttime);
    QToolTip::add(m_pStarttime, trUtf8("Task Starting Time"));
    m_pEditlayout->addWidget(m_pStarttime);                         // Finally add the start editor

    // Make the end time selector
    m_pEndtime = new QTimeEdit(m_pCenframe, "EndTime");
    if (m_pEndtime == NULL)                                         // Sanity check
        throw Genexcept("Main window end time creation failed.");   // Spew errors

    // Configure attributes
    m_pEndtime->setDisplay(QTimeEdit::Hours | QTimeEdit::Minutes);
    m_pEndtime->setAutoAdvance(true);
    m_pEndtime->setEnabled(false);

    // Whatsthis info for the time editor
    const char *kszEndtime = "Edit the <em>task ending time</em> by clicking on "
                             "the hour and minute, and then changing their "
                             "values with the arrow buttons.";
    QWhatsThis::add(m_pEndtime, kszEndtime);
    QToolTip::add(m_pEndtime, trUtf8("Task Ending Time"));
    m_pEditlayout->addWidget(m_pEndtime);                           // Finally add the end editor

//    // Make the total time amount selector
//    m_pAmount = new QTimeEdit(m_pCenframe, "TotalAmount");
//    if (m_pAmount == NULL)                                          // Sanity check
//        throw Genexcept("Main window amount creation failed.");     // Spew errors
//
//    // Configure attributes
//    m_pAmount->setDisplay(QTimeEdit::Hours | QTimeEdit::Minutes);
//    m_pAmount->setAutoAdvance(true);
//    m_pAmount->setEnabled(false);

    // Make the amount selector
    m_pAmount = new ASTimeEdit(m_pCenframe, "TotalAmount");
    if (m_pAmount == NULL)                                      // Sanity check
        throw Genexcept("Main window amount creation failed."); // Spew errors

    // Whatsthis info for the amount editor
    const char *kszAmount = "Edit the <em>task amount</em> by clicking on "
                            "the amount, and then changing its "
                            "value with the arrow buttons.";
    QWhatsThis::add(m_pAmount, kszAmount);
    QToolTip::add(m_pAmount, trUtf8("Task Amount"));
    m_pAmount->setEnabled(false);           // Don't enable until it's ready
    m_pEditlayout->addWidget(m_pAmount);    // Finally add the amount editor

    // Construct a stringlist just to hold task values
    m_pTaskentries = new QStringList;
    if (m_pTaskentries == NULL) // Sanity check
        throw Genexcept("Main window task entries creation failed.");

    // Populate the stringlist with fresh accounts, taken from filename in prefs
    QString Asdir = m_pPrefs->getString(TITRAQ_PREFACCOUNTS, TITRAQ_DEFACCOUNTS);
    if (Asdir.startsWith(TITRAQ_HOMEDIRTOK))
        Asdir = QDir::homeDirPath() + Asdir.remove(0, QString(TITRAQ_HOMEDIRTOK).length() - 1);
    QFile Filetemp(Asdir);
    this->loadAccounts(Filetemp);   // Load account helper method

    // Make the combobox task edit
    m_pTasks = new QComboBox(true, m_pCenframe, "Tasks");
    if (m_pTasks == NULL)   // Sanity check
        throw Genexcept("Main window task edit creation failed.");
    m_pTasks->insertStringList(*m_pTaskentries);
    m_pTasks->setCurrentText(NULL);
    m_pTasks->setSizeLimit(TITRAQ_ACCTBOXHEIGHT);
    m_pTasks->setAutoCompletion(true);
    m_pTasks->setDuplicatesEnabled(false);
    m_pTasks->setEnabled(false);
//    m_pTasks->setEditable(false);   // Only idiots define noneditable and autocompletion

    // Add an auto scroll bar if possible under current GUI style
    if (m_pTasks->listBox()) {  // Motif style has no list box
        int nPopupwidth = m_pTasks->listBox()->maxItemWidth();
        nPopupwidth += m_pTasks->listBox()->horizontalScrollBar()->height();
        m_pTasks->listBox()->setFixedWidth(nPopupwidth);
        m_pTasks->listBox()->setHScrollBarMode(QScrollView::Auto);
        m_pTasks->listBox()->sort();
    }

    // Whatsthis info for the task editor
    const char *kszTasktext = "Choose a <em>task account</em> by clicking on "
                              "this box, and choosing whichever item most "
                              "closely resembles your task.";
    QWhatsThis::add(m_pTasks, kszTasktext);
    QToolTip::add(m_pTasks, trUtf8("Task Selector"));
    m_pEditlayout->addWidget(m_pTasks); // Finally add the task edit

    // Make the Remark line
    m_pRemark = new QLineEdit(m_pCenframe, "Remark");
    if (m_pRemark == NULL)  // Sanity check
        throw Genexcept("Main window remark line creation failed.");
    m_pRemark->setFrameShape(QLineEdit::LineEditPanel);
    m_pRemark->setFrameShadow(QLineEdit::Sunken);
    m_pRemark->setFrame(true);
    m_pRemark->setReadOnly(false);
    m_pRemark->setEnabled(false);

    // Whatsthis info for the remark line
    const char *kszRemarktext = "Edit the <em>task remarks</em> by clicking on "
                                "this line control and typing the remarks you "
                                "have about the task.";
    QWhatsThis::add(m_pRemark, kszRemarktext);
    QToolTip::add(m_pRemark, trUtf8("Remark Line"));
    m_pEditlayout->addWidget(m_pRemark);    // Finally add the remark line

    // Start edit controls off at right size
    this->updSizes(TITRAQ_IDXALLCTRLS, -1, -1);

    // Validate data just as it is being entered
    connect(m_pDateedit, SIGNAL(valueChanged(const QDate &)), SLOT(validateRow(void)));
    connect(m_pStarttime, SIGNAL(valueChanged(const QTime &)), SLOT(validateRow(void)));
    connect(m_pEndtime, SIGNAL(valueChanged(const QTime &)), SLOT(validateRow(void)));
    connect(m_pAmount, SIGNAL(valueChanged(const QTime &)), SLOT(validateRow(void)));
    connect(m_pTasks, SIGNAL(textChanged(const QString &)), SLOT(validateRow(void)));
    connect(m_pRemark, SIGNAL(textChanged(const QString &)), SLOT(validateRow(void)));

    // Signals sent between table cells and corresponding edit control widgets
    connect(m_pLineedit, SIGNAL(textChanged(const QString &)), SLOT(updateLine(const QString &)));
    connect(m_pUseredit, SIGNAL(textChanged(const QString &)), SLOT(updateUser(const QString &)));
    connect(m_pGuidedit, SIGNAL(textChanged(const QString &)), SLOT(updateGuid(const QString &)));
    connect(m_pCrcedit, SIGNAL(textChanged(const QString &)), SLOT(updateCrc(const QString &)));
    connect(m_pRevedit, SIGNAL(textChanged(const QString &)), SLOT(updateRev(const QString &)));
    connect(m_pDateedit, SIGNAL(valueChanged(const QDate &)), SLOT(updateDate(const QDate &)));
    connect(m_pStarttime, SIGNAL(valueChanged(const QTime &)), SLOT(updateStart(const QTime &)));
    connect(m_pEndtime, SIGNAL(valueChanged(const QTime &)), SLOT(updateFinish(const QTime &)));
    connect(m_pAmount, SIGNAL(valueChanged(const QTime &)), SLOT(updateAmount(const QTime &)));
    connect(m_pTasks, SIGNAL(textChanged(const QString &)), SLOT(updateTask(const QString &)));
    connect(m_pRemark, SIGNAL(textChanged(const QString &)), SLOT(updateRemark(const QString &)));

    // Correctly update checksum and revision fields also
    connect(m_pUseredit, SIGNAL(textChanged(const QString &)), SLOT(calcCrc(void)));
    connect(m_pDateedit, SIGNAL(valueChanged(const QDate &)), SLOT(calcCrc(void)));
    connect(m_pStarttime, SIGNAL(valueChanged(const QTime &)), SLOT(calcCrc(void)));
    connect(m_pEndtime, SIGNAL(valueChanged(const QTime &)), SLOT(calcCrc(void)));
    connect(m_pAmount, SIGNAL(valueChanged(const QTime &)), SLOT(calcCrc(void)));
    connect(m_pTasks, SIGNAL(textChanged(const QString &)), SLOT(calcCrc(void)));
    connect(m_pRemark, SIGNAL(textChanged(const QString &)), SLOT(calcCrc(void)));
}

//
// Arrange and configure columns
//
void Titraqform::setupColumns(void)
{
    // Handle column view handling presets and defaults
    if (!m_pPrefs->getBool(TITRAQ_PREFSTATCOLON, TITRAQ_DEFSTATCOLON))
        showStatcol();
    if (!m_pPrefs->getBool(TITRAQ_PREFLCOLON, TITRAQ_DEFLCOLON))
        showLinecol();
    if (!m_pPrefs->getBool(TITRAQ_PREFUCOLON, TITRAQ_DEFUCOLON))
        showUsercol();
    if (!m_pPrefs->getBool(TITRAQ_PREFGCOLON, TITRAQ_DEFGCOLON))
        showGuidcol();
    if (!m_pPrefs->getBool(TITRAQ_PREFCCOLON, TITRAQ_DEFCCOLON))
        showCrccol();
    if (!m_pPrefs->getBool(TITRAQ_PREFREVCOLON, TITRAQ_DEFREVCOLON))
        showRevcol();
    if (!m_pPrefs->getBool(TITRAQ_PREFDCOLON, TITRAQ_DEFDCOLON))
        showDatecol();
    if (!m_pPrefs->getBool(TITRAQ_PREFSTARTCOLON, TITRAQ_DEFSTARTCOLON))
        showStartcol();
    if (!m_pPrefs->getBool(TITRAQ_PREFFCOLON, TITRAQ_DEFFCOLON))
        showFinishcol();
    if (!m_pPrefs->getBool(TITRAQ_PREFACOLON, TITRAQ_DEFACOLON))
        showAmountcol();
    if (!m_pPrefs->getBool(TITRAQ_PREFTCOLON, TITRAQ_DEFTCOLON))
        showTaskcol();
    if (!m_pPrefs->getBool(TITRAQ_PREFREMCOLON, TITRAQ_DEFREMCOLON))
        showRemarkcol();

    // Handle tool bar view handling presets and defaults
    if (!m_pPrefs->getBool(TITRAQ_PREFFILEBAR, TITRAQ_DEFFILEBAR)) {
        m_pFiletools->hide();
        m_pTbarspopup->setItemChecked(m_pTbarspopup->idAt(TITRAQ_IDXFILEBAR), false);
    }
    if (!m_pPrefs->getBool(TITRAQ_PREFEDITBAR, TITRAQ_DEFEDITBAR)) {
        m_pEdittools->hide();
        m_pTbarspopup->setItemChecked(m_pTbarspopup->idAt(TITRAQ_IDXEDITBAR), false);
    }
    if (!m_pPrefs->getBool(TITRAQ_PREFVIEWBAR, TITRAQ_DEFVIEWBAR)) {
        m_pViewtools->hide();
        m_pTbarspopup->setItemChecked(m_pTbarspopup->idAt(TITRAQ_IDXVIEWBAR), false);
    }
    if (!m_pPrefs->getBool(TITRAQ_PREFPREFBAR, TITRAQ_DEFPREFBAR)) {
        m_pPrefstools->hide();
        m_pTbarspopup->setItemChecked(m_pTbarspopup->idAt(TITRAQ_IDXPREFBAR), false);
    }
    if (!m_pPrefs->getBool(TITRAQ_PREFWHATBAR, TITRAQ_DEFWHATBAR)) {
        m_pWhatstools->hide();
        m_pTbarspopup->setItemChecked(m_pTbarspopup->idAt(TITRAQ_IDXWHATBAR), false);
    }
}

//
// [En|dis]able most ui pieces to suit or restrict user
//
void Titraqform::enableIface(bool bTurned)
{
    // FIXME: Does not belong here, so relocate
    if (bTurned)                       // Turn off the interface
        m_pDateedit->setDayfocus();     // Default focus is on day
    else {                              // Turn on the interface
        m_pMaintable->setNumRows(0);    // Remove table data
        m_pStatusedit->setPixmap(QPixmap(s_kpcStatvoid_xpm));
    }

    // Enable or disable the widgets
    m_pMaintable->setEnabled(bTurned);
    m_pLineedit->setEnabled(bTurned);
    m_pUseredit->setEnabled(bTurned);
    m_pGuidedit->setEnabled(bTurned);
    m_pCrcedit->setEnabled(bTurned);
    m_pRevedit->setEnabled(bTurned);
    m_pDateedit->setEnabled(bTurned);
    m_pStarttime->setEnabled(bTurned);
    m_pEndtime->setEnabled(bTurned);
    m_pAmount->setEnabled(bTurned);
    m_pTasks->setEnabled(bTurned);
    m_pRemark->setEnabled(bTurned);

    // Enable or disable the actions
    m_pFilesaveact->setEnabled(bTurned);
    m_pFilesaveasact->setEnabled(bTurned);
    m_pFilecloseact->setEnabled(bTurned);
    m_pAddrowact->setEnabled(bTurned);
    m_pDelrowact->setEnabled(bTurned);
    m_pRefreshact->setEnabled(bTurned);
    m_pCutact->setEnabled(bTurned);
    m_pCopyact->setEnabled(bTurned);
    m_pPasteact->setEnabled(bTurned);
    m_pReportact->setEnabled(bTurned);
#if defined HAVE_MICO || defined HAVE_ESOAP
    if (bTurned)
        m_pSyncact->setEnabled(m_pPrefs->getBool(TITRAQ_PREFCORBON, TITRAQ_DEFCORBON)
            | m_pPrefs->getBool(TITRAQ_PREFSOAPON, TITRAQ_DEFSOAPON));
    else
        m_pSyncact->setEnabled(false);
#endif // defined HAVE_MICO || defined HAVE_ESOAP

    // Unconditional settings
    m_pMaintable->setDirty(false);                  // Reset to clean data
}

//
// Parse command line arguments for an existing filename, and
// open it to edit the intended (but optional) initial data
//
void Titraqform::loadInit(void)
{
    // If the user gives a filename argument to the shell, then open that file
    int nNumargs = qApp->argc();    // Holds the number of cmd arguments
    int nValid = 0;                 // Holds the return value
    QFile Initial;                  // Initial event data file

    if (nNumargs > 1) { // Warm up a nice cascade, to set my mind on four weeks of vacation
        for (int nIter = 1; nIter < nNumargs; nIter++) {    // Salad in New Zealand
            if (QChar(*qApp->argv()[nIter]) != '-') {       // Bunuelos in Colombia
                try {
                    this->setFilename(qApp->argv()[nIter]); // Store inital filename
                    Initial.setName(*this->getFilename());  // Initial file to load
                    if (Initial.exists(*getFilename()) && validateData(Initial)) {
                        this->loadData(Initial);            // Pass to helper method
                        enableIface(true);                  // Turn on the lights
                        m_pStatbar->message(trUtf8("Loaded document ") + *this->getFilename());
                    }
                    else // The inital file name does not correspond to a file
                        m_pStatbar->message(trUtf8(QString("The file ") + *this->getFilename() + QString(" does not exist.")));
                }
                catch (Genexcept& Genex) {
                    Genex.reportErr();
                }
            }
        }
    }
    else
        m_pStatbar->message(trUtf8("Ready"));   // Signal a ready condition
}
