##
##  OSSP asgui - Accounting system graphical user interface
##  Copyright (c) 2002-2004 The OSSP Project (http://www.ossp.org/)
##  Copyright (c) 2002-2004 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2002-2004 Michael Schloh von Bennewitz <michael@schloh.com>
##  Copyright (c) 2002-2004 Cable & Wireless Telecommunications Services GmbH
##
##  This file is part of OSSP GUI, a graphical user interface
##  for OSSP which can be found at http://www.ossp.org/
##
##  Permission to use, copy, modify, and distribute this software for
##  any purpose with or without fee is hereby granted, provided that
##  the above copyright notice and this permission notice appear in all
##  copies.
##
##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
##
##  aclocal.m4: Autoconf M4 macros
##


dnl ##
dnl ##  Look for SIOCGIFHWADDR
dnl ##

AC_DEFUN(AC_CHECK_SIOCGIFHWADDR,[
AC_MSG_CHECKING(whether <sys/ioctl.h> has SIOCGIFHWADDR)
AC_TRY_COMPILE([#include <sys/sockio.h>
#include <sys/ioctl.h>
], [
  int i = SIOCGIFHWADDR;
  return 0;],
    AC_DEFINE(HAVE_SIOCGIFHWADDR, 1, [Define to 1 if <sys/ioctl.h> defines or includes SIOCGIFHWADDR.]),
    AC_MSG_RESULT([no]),
    AC_MSG_RESULT([yes]))
])dnl


dnl ##
dnl ##  Look for SIOCGIFCONF
dnl ##

AC_DEFUN(AC_CHECK_SIOCGIFCONF,[
AC_MSG_CHECKING(whether <sys/ioctl.h> has SIOCGIFCONF)
AC_TRY_COMPILE([#include <sys/sockio.h>
#include <sys/ioctl.h>
], [
  int i = SIOCGIFCONF;
  return 0;],
    AC_DEFINE(HAVE_SIOCGIFCONF, 1, [Define to 1 if <sys/ioctl.h> defines or includes SIOCGIFCONF.]),
    AC_MSG_RESULT([no]),
    AC_MSG_RESULT([yes]))
])dnl


dnl ##
dnl ##  Look for SIOGHWADDR
dnl ##

AC_DEFUN(AC_CHECK_SIOGHWADDR,[
AC_MSG_CHECKING(whether <sys/ioctl.h> has SIOGHWADDR)
AC_TRY_COMPILE([#include <sys/sockio.h>
#include <sys/ioctl.h>
], [
  int i = SIOGHWADDR;
  return 0;],
    AC_DEFINE(HAVE_SIOGHWADDR, 1, [Define to 1 if <sys/ioctl.h> defines or includes SIOGHWADDR.]),
    AC_MSG_RESULT([no]),
    AC_MSG_RESULT([yes]))
])dnl


dnl ##
dnl ##  Look for SIOCGENADDR
dnl ##

AC_DEFUN(AC_CHECK_SIOCGENADDR,[
AC_MSG_CHECKING(whether <sys/ioctl.h> has SIOCGENADDR)
AC_TRY_COMPILE([#include <sys/sockio.h>
#include <sys/ioctl.h>
], [
  int i = SIOCGENADDR;
  return 0;],
    AC_DEFINE(HAVE_SIOCGENADDR, 1, [Define to 1 if <sys/ioctl.h> defines or includes SIOCGENADDR.]),
    AC_MSG_RESULT([no]),
    AC_MSG_RESULT([yes]))
])dnl


dnl ##
dnl ##  Check for sa_len member of the sockaddr
dnl ##  structure found in BSD systems
dnl ##
dnl ##  configure.in:
dnl ##    AC_STRUCT_SA_LEN
dnl ##

AC_DEFUN(AC_STRUCT_SA_LEN,
[AC_CACHE_CHECK([for sa_len in struct sockaddr], ac_cv_struct_sa_len,
[AC_TRY_COMPILE([
    #include <sys/types.h>
    #include <sys/socket.h>
],
[
    struct sockaddr s;
    s.sa_len;
],
ac_cv_struct_sa_len=yes, ac_cv_struct_sa_len=no)])
if test $ac_cv_struct_sa_len = yes; then
AC_DEFINE(HAVE_SA_LEN, 1, [Define to 1 if your sockaddr has a sa_len member.])
fi
])


dnl ##
dnl ##  Support for Platform IDs
dnl ##
dnl ##  configure.in:
dnl ##    AC_PLATFORM(<variable>)
dnl ##

AC_DEFUN(AC_PLATFORM,[
if test ".$host" != .NONE && test ".$host" != .; then
    $1="$host"
else
    $1=`${CONFIG_SHELL-/bin/sh} $srcdir/config.guess`
fi
$1=`${CONFIG_SHELL-/bin/sh} $srcdir/config.sub $$1` || exit 1
AC_SUBST($1)
if test ".$enable_subdir" != .yes; then
    echo "Platform: ${TB}${$1}${TN}"
fi
])dnl


dnl ##
dnl ##  Support for Configuration Headers
dnl ##
dnl ##  configure.in:
dnl ##    AC_HEADLINE(<short-name>, <long-name>,
dnl ##                <vers-var>, <vers-file>,
dnl ##                <copyright>)
dnl ##

AC_DEFUN(AC_HEADLINE,[dnl
#   configuration header
if test ".`echo dummy [$]@ | grep help`" = .; then
    #   bootstrapping shtool
    ac_prog=[$]0
changequote(, )dnl
    ac_srcdir=`echo $ac_prog | sed -e 's%/[^/][^/]*$%%' -e 's%\([^/]\)/*$%\1%'`
changequote([, ])dnl
    test ".$ac_srcdir" = ".$ac_prog" && ac_srcdir=.
    ac_shtool="$ac_srcdir/shtool"

    #   find out terminal sequences
    TB=`$ac_shtool echo -n -e %B 2>/dev/null`
    TN=`$ac_shtool echo -n -e %b 2>/dev/null`

    #   find out package version
    $3_STR="`$ac_shtool version -l c -d long $ac_srcdir/$4`"
    AC_SUBST($3_STR)

    #   friendly header ;)
    if test ".$enable_headline" != .no; then
        echo "Configuring ${TB}$1${TN} ($2), Version ${TB}${$3_STR}${TN}"
        echo "$5"
    fi

    #   additionally find out hex version
    $3_HEX="`$ac_shtool version -l c -d hex $ac_srcdir/$4`"
    AC_SUBST($3_HEX)
fi
])dnl


dnl ##
dnl ##  Check whether compiler option works
dnl ##
dnl ##  configure.in:
dnl ##    AC_COMPILER_OPTION(<name>, <display>, <option>,
dnl ##                       <action-success>, <action-failure>)
dnl ##

AC_DEFUN(AC_COMPILER_OPTION,[dnl
AC_MSG_CHECKING(whether compiler option(s) $2 work)
AC_CACHE_VAL(ac_cv_compiler_option_$1,[
SAVE_CXXFLAGS="$CXXFLAGS"
CXXFLAGS="$CXXFLAGS $3"
AC_LANG(C++)
AC_TRY_COMPILE([],[], ac_cv_compiler_option_$1=yes, ac_cv_compiler_option_$1=no)
CXXFLAGS="$SAVE_CXXFLAGS"
])dnl
if test ".$ac_cv_compiler_option_$1" = .yes; then
    ifelse([$4], , :, [$4])
else
    ifelse([$5], , :, [$5])
fi
AC_MSG_RESULT([$ac_cv_compiler_option_$1])
])dnl


dnl ##
dnl ##  Test for the presence of the MICO ORB
dnl ##
dnl ##  configure.in:
dnl ##    AC_TEST_MICO
dnl ##

AC_DEFUN(AC_TEST_MICO,[dnl
AC_ARG_WITH(mico,dnl
[  --with-mico=[DIR]    prefix where MICO is installed], micodir=$withval,)
if test $micodir; then
AC_MSG_CHECKING(the MICO installation path)

dnl ## Ensure that we have a basic path to start searching
if test -x "$micodir";
then
    CORBABASE=$micodir
    AC_MSG_RESULT(${CORBABASE})
elif test -x "${MICODIR}";
then
    CORBABASE="${MICODIR}"
    AC_MSG_RESULT(${CORBABASE})
else
    AC_MSG_RESULT(not found)
    AC_MSG_ERROR([Neither \$MICODIR nor --with-mico=[DIR] paths exist])
fi

dnl ## Append paths of libs and headers
LIBS="$LIBS -lmico -lmicocoss -lssl -lcrypto"
LDFLAGS="$LDFLAGS -L${CORBABASE}/lib"
CPPFLAGS="$CPPFLAGS -I${CORBABASE}/include"
CXXCPP="$CXXCPP -I${CORBABASE}/include"
CXXFLAGS="$CXXFLAGS"
AC_DEFINE(HAVE_MICO, 1, [Define to 1 if building with the MICO ORB.])
fi
])


dnl ##
dnl ##  Test for the presence of EasySOAP
dnl ##
dnl ##  configure.in:
dnl ##    AC_TEST_ESOAP
dnl ##

AC_DEFUN(AC_TEST_ESOAP,[dnl
AC_ARG_WITH(esoap,dnl
[  --with-easysoap=[DIR]    prefix where EasySOAP is installed], esoapdir=$withval,)
if test $esoapdir; then
AC_MSG_CHECKING(the EasySOAP installation path)

dnl ## Ensure that we have a basic path to start searching
if test -x "$esoapdir";
then
    SOAPBASE=$esoapdir
    AC_MSG_RESULT(${SOAPBASE})
elif test -x "${ESOAPDIR}";
then
    SOAPBASE="${ESOAPDIR}"
    AC_MSG_RESULT(${SOAPBASE})
else
    AC_MSG_RESULT(not found)
    AC_MSG_ERROR([Neither \$ESOAPDIR nor --with-easysoap=[DIR] paths exist])
fi

dnl ## Append paths of libs and headers
LIBS="$LIBS -leasysoap -lexpat -lssl -lcrypto"
LDFLAGS="$LDFLAGS -L${SOAPBASE}/lib"
CPPFLAGS="$CPPFLAGS -I${SOAPBASE}/include"
CXXCPP="$CXXCPP -I${SOAPBASE}/include"
CXXFLAGS="$CXXFLAGS"
AC_DEFINE(HAVE_ESOAP, 1, [Define to 1 if building with EasySOAP.])
fi
])


dnl ##
dnl ##  Check the Qt implementation path
dnl ##
dnl ##  configure.in:
dnl ##    AC_CHECK_QT
dnl ##

AC_DEFUN(AC_CHECK_QT,[dnl
AC_ARG_WITH(qt,dnl
[  --with-qt=[DIR]    prefix where Qt is installed], qtdir=$withval,)
AC_MSG_CHECKING(the Qt installation path)

dnl ## Ensure that we have a basic path to start searching
if test -x "$qtdir";
then
    QTBASE=$qtdir
    AC_MSG_RESULT(${QTBASE})
elif test -x "${QTDIR}";
then
    QTBASE="${QTDIR}"
    AC_MSG_RESULT(${QTBASE})
else
    AC_MSG_RESULT(not found)
    AC_MSG_ERROR([Neither \$QTDIR nor --with-qt=[DIR] paths exist])
fi

dnl ## Now search for libs and headers
changequote(, )dnl
for file in x `find $QTBASE/include -name "qapplication.h" -print`; do
    test .$file = .x && continue
    c_dir=`echo $file | sed -e 's;[^/]*$;;' -e 's;\(.\)/$;\1;'`
done

dnl ## Scan to find out which Qt library to link against
dnl ## FIXME Merge static and dynamic library search blocks
dnl ## FIXME Remove this qt-mt check and do --enable-threads instead
for file in x `find $QTBASE/lib -name "libqt.so" -print`; do
    test .$file = .x && continue
    l_dir=`echo $file | sed -e 's;[^/]*$;;' -e 's;\(.\)/$;\1;'`
    lib=`echo $file | sed -e 's;^.*/\(.*\)$;\1;' -e 's;lib\(.*\)\.so;\1;'`
done

dnl ## If we have a multithreaded shared library, then it's better
for file in x `find $QTBASE/lib -name "libqt-mt.so" -print`; do
    test .$file = .x && continue
    l_dir=`echo $file | sed -e 's;[^/]*$;;' -e 's;\(.\)/$;\1;'`
    lib=`echo $file | sed -e 's;^.*/\(.*\)$;\1;' -e 's;lib\(.*\)\.so;\1;'`
done

dnl ## If we have static libraries, then prefer to use those
for file in x `find $QTBASE/lib -name "libqt.a" -print`; do
    test .$file = .x && continue
    l_dir=`echo $file | sed -e 's;[^/]*$;;' -e 's;\(.\)/$;\1;'`
    lib=`echo $file | sed -e 's;^.*/\(.*\)$;\1;' -e 's;lib\(.*\)\.a;\1;'`
done

dnl ## But the static multithreaded library is preferred above all
for file in x `find $QTBASE/lib -name "libqt-mt.a" -print`; do
    test .$file = .x && continue
    l_dir=`echo $file | sed -e 's;[^/]*$;;' -e 's;\(.\)/$;\1;'`
    lib=`echo $file | sed -e 's;^.*/\(.*\)$;\1;' -e 's;lib\(.*\)\.a;\1;'`
done
changequote([, ])dnl

dnl ## Append paths of libs and headers
if test ".$lib" != .; then
    LIBS="$LIBS -l$lib"
fi
LDFLAGS="$LDFLAGS -L$l_dir"
CPPFLAGS="$CPPFLAGS -DQT_THREAD_SUPPORT"
CXXCPP="$CXXCPP -DQT_THREAD_SUPPORT"
CXXFLAGS="$CXXFLAGS -I$c_dir"
])


dnl ##
dnl ##  UIC check, should come after the Qt check
dnl ##
dnl ##  configure.in:
dnl ##    AC_CHECK_UIC
dnl ##

AC_DEFUN(AC_CHECK_UIC,[dnl
AC_MSG_CHECKING(UIC location)
if test -x ${QTBASE}/bin/uic
then
    UIC=${QTBASE}/bin/uic
    AC_MSG_RESULT(${QTBASE}/bin/uic)
else
    UIC=uic
    AC_MSG_RESULT(assumed present)
fi
])


dnl ##
dnl ##  MOC check, should come after the Qt check
dnl ##
dnl ##  configure.in:
dnl ##    AC_CHECK_MOC
dnl ##

AC_DEFUN(AC_CHECK_MOC,[dnl
AC_MSG_CHECKING(MOC location)
if test -x ${QTBASE}/bin/moc
then
    MOC=${QTBASE}/bin/moc
    AC_MSG_RESULT(${QTBASE}/bin/moc)
else
    MOC=moc
    AC_MSG_RESULT(assumed present)
fi
])


dnl ##  PKG_CHECK_MODULES(GSTUFF, gtk+-2.0 >= 1.3 glib = 1.3.4, action-if, action-not)
dnl ##  defines GSTUFF_LIBS, GSTUFF_CFLAGS, see pkg-config man page
dnl ##  also defines GSTUFF_PKG_ERRORS on error
AC_DEFUN(PKG_CHECK_MODULES, [
  succeeded=no

  if test -z "$PKG_CONFIG"; then
    AC_PATH_PROG(PKG_CONFIG, pkg-config, no)
  fi

  if test "$PKG_CONFIG" = "no" ; then
     echo "*** The pkg-config script could not be found. Make sure it is"
     echo "*** in your path, or set the PKG_CONFIG environment variable"
     echo "*** to the full path to pkg-config."
     echo "*** Or see http://www.freedesktop.org/software/pkgconfig to get pkg-config."
  else
     PKG_CONFIG_MIN_VERSION=0.9.0
     if $PKG_CONFIG --atleast-pkgconfig-version $PKG_CONFIG_MIN_VERSION; then
        AC_MSG_CHECKING(for $2)

        if $PKG_CONFIG --exists "$2" ; then
            AC_MSG_RESULT(yes)
            succeeded=yes

            AC_MSG_CHECKING($1_CFLAGS)
            $1_CFLAGS=`$PKG_CONFIG --cflags "$2"`
            AC_MSG_RESULT($$1_CFLAGS)

            AC_MSG_CHECKING($1_LIBS)
            $1_LIBS=`$PKG_CONFIG --libs "$2"`
            AC_MSG_RESULT($$1_LIBS)
        else
            $1_CFLAGS=""
            $1_LIBS=""
            ## If we have a custom action on failure, don't print errors, but 
            ## do set a variable so people can do so.
            $1_PKG_ERRORS=`$PKG_CONFIG --errors-to-stdout --print-errors "$2"`
            ifelse([$4], ,echo $$1_PKG_ERRORS,)
        fi

        AC_SUBST($1_CFLAGS)
        AC_SUBST($1_LIBS)
     else
        echo "*** Your version of pkg-config is too old. You need version $PKG_CONFIG_MIN_VERSION or newer."
        echo "*** See http://www.freedesktop.org/software/pkgconfig"
     fi
  fi

  if test $succeeded = yes; then
     ifelse([$3], , :, [$3])
  else
     ifelse([$4], , AC_MSG_ERROR([Library requirements ($2) not met; consider adjusting the PKG_CONFIG_PATH environment variable if your libraries are in a nonstandard prefix so pkg-config can find them.]), [$4])
  fi
])


dnl ##
dnl ##  Debugging Support
dnl ##
dnl ##  configure.in:
dnl ##    AC_CHECK_DEBUGGING
dnl ##

AC_DEFUN(AC_CHECK_DEBUGGING,[dnl
AC_ARG_ENABLE(debug,dnl
[  --enable-debug          build for debugging (default=no)],
[dnl
if test ".$ac_cv_prog_gcc" = ".yes"; then
    case "$CXXFLAGS" in
        *-O* ) ;;
           * ) CXXFLAGS="$CXXFLAGS -O2" ;;
    esac
    case "$CXXFLAGS" in
        *-g* ) ;;
           * ) CXXFLAGS="$CXXFLAGS -g" ;;
    esac
    case "$CXXFLAGS" in
        *-pipe* ) ;;
              * ) AC_COMPILER_OPTION(pipe, -pipe, -pipe, CXXFLAGS="$CXXFLAGS -pipe") ;;
    esac
    AC_COMPILER_OPTION(defdbg, -DDEBUG, -DDEBUG, CXXFLAGS="$CXXFLAGS -DDEBUG")
dnl    AC_COMPILER_OPTION(ggdb3, -ggdb3, -ggdb3, CXXFLAGS="$CXXFLAGS -ggdb3")
    CXXFLAGS="$CXXFLAGS -pedantic"
    CXXFLAGS="$CXXFLAGS -Wall"
    WMORE="-Wshadow -Wpointer-arith -Wcast-align -Winline"
    WMORE="$WMORE -Wmissing-prototypes -Wmissing-declarations -Wnested-externs"
    AC_COMPILER_OPTION(wmore, -W<xxx>, $WMORE, CXXFLAGS="$CXXFLAGS $WMORE")
    AC_COMPILER_OPTION(wnolonglong, -Wno-long-long, -Wno-long-long, CXXFLAGS="$CXXFLAGS -Wno-long-long")
else
    case "$CXXFLAGS" in
        *-g* ) ;;
           * ) CXXFLAGS="$CXXFLAGS -g" ;;
    esac
fi
msg="enabled"
dnl AC_DEFINE(DEBUG, 1, [Define to 1 if you want to enable debugging])
],[
if test ".$ac_cv_prog_gcc" = ".yes"; then
case "$CXXFLAGS" in
    *-pipe* ) ;;
          * ) AC_COMPILER_OPTION(pipe, -pipe, -pipe, CXXFLAGS="$CXXFLAGS -pipe") ;;
esac
fi
case "$CXXFLAGS" in
    *-g* ) CXXFLAGS=`echo "$CXXFLAGS" |\
                   sed -e 's/ -g / /g' -e 's/ -g$//' -e 's/^-g //g' -e 's/^-g$//'` ;;
esac
case "$CXXFLAGS" in
    *-g* ) CXXFLAGS=`echo "$CXXFLAGS" |\
                     sed -e 's/ -g / /g' -e 's/ -g$//' -e 's/^-g //g' -e 's/^-g$//'` ;;
esac

CXXFLAGS="$CXXFLAGS -DQT_NO_DEBUG"
msg=disabled
])dnl
AC_MSG_CHECKING(for compilation debug mode)
AC_MSG_RESULT([$msg])
if test ".$msg" = .enabled; then
    enable_shared=no
fi
])

define(AC_IFALLYES,[dnl
ac_rc=yes
for ac_spec in $1; do
    ac_type=`echo "$ac_spec" | sed -e 's/:.*$//'`
    ac_item=`echo "$ac_spec" | sed -e 's/^.*://'`
    case $ac_type in
        header [)]
            ac_item=`echo "$ac_item" | sed 'y%./+-%__p_%'`
            ac_var="ac_cv_header_$ac_item"
            ;;
        file [)]
            ac_item=`echo "$ac_item" | sed 'y%./+-%__p_%'`
            ac_var="ac_cv_file_$ac_item"
            ;;
        func    [)] ac_var="ac_cv_func_$ac_item"   ;;
        lib     [)] ac_var="ac_cv_lib_$ac_item"    ;;
        define  [)] ac_var="ac_cv_define_$ac_item" ;;
        typedef [)] ac_var="ac_cv_typedef_$ac_item" ;;
        custom  [)] ac_var="$ac_item" ;;
    esac
    eval "ac_val=\$$ac_var"
    if test ".$ac_val" != .yes; then
        ac_rc=no
        break
    fi
done
if test ".$ac_rc" = .yes; then
    :
    $2
else
    :
    $3
fi
])


dnl ##
dnl ##  Check for an external/extension library.
dnl ##  - is aware of <libname>-config style scripts
dnl ##  - searches under standard paths include, lib, etc.
dnl ##  - searches under subareas like .libs, etc.
dnl ##
dnl ##  configure.in:
dnl ##      AC_CHECK_EXTLIB(<realname>, <libname>, <func>, <header>,
dnl ##                      [<success-action> [, <fail-action>]])
dnl ##  Makefile.in:
dnl ##      CFLAGS  = @CFLAGS@
dnl ##      LDFLAGS = @LDFLAGS@
dnl ##      LIBS    = @LIBS@
dnl ##  shell:
dnl ##      $ ./configure --with-<libname>[=DIR]
dnl ##

AC_DEFUN(AC_CHECK_EXTLIB,[dnl
AC_ARG_WITH($2, [dnl
[  --with-]m4_substr([$2[[=DIR]]                     ], 0, 19)[build with external $1 library (default=no)]], [dnl
    if test ".$with_$2" = .yes; then
        #   via config script
        $2_version=`($2-config --version) 2>/dev/null`
        if test ".$$2_version" != .; then
            CFLAGS="$CFLAGS `$2-config --cflags`"
            LDFLAGS="$LDFLAGS `$2-config --ldflags`"
        fi
    else
        if test -d "$with_$2"; then
            found=0
            #   via config script
            for dir in $with_$2/bin $with_$2; do
                if test -f "$dir/$2-config"; then
                    $2_version=`($dir/$2-config --version) 2>/dev/null`
                    if test ".$$2_version" != .; then
                        CFLAGS="$CFLAGS `$dir/$2-config --cflags`"
                        LDFLAGS="$LDFLAGS `$dir/$2-config --ldflags`"
                        found=1
                        break
                    fi
                fi
            done
            #   via standard paths
            if test ".$found" = .0; then
                for dir in $with_$2/include/$2 $with_$2/include $with_$2; do
                    if test -f "$dir/$4"; then
                        CFLAGS="$CFLAGS -I$dir"
                        found=1
                        break
                    fi
                done
                for dir in $with_$2/lib/$2 $with_$2/lib $with_$2; do
                    if test -f "$dir/lib$2.a" -o -f "$dir/lib$2.so"; then
                        LDFLAGS="$LDFLAGS -L$dir"
                        found=1
                        break
                    fi
                done
            fi
            #   in any subarea
            if test ".$found" = .0; then
changequote(, )dnl
                for file in x `find $with_$2 -name "$4" -type f -print`; do
                    test .$file = .x && continue
                    dir=`echo $file | sed -e 's;[[^/]]*$;;' -e 's;\(.\)/$;\1;'`
                    CFLAGS="$CFLAGS -I$dir"
                done
                for file in x `find $with_$2 -name "lib$2.[[aso]]" -type f -print`; do
                    test .$file = .x && continue
                    dir=`echo $file | sed -e 's;[[^/]]*$;;' -e 's;\(.\)/$;\1;'`
                    LDFLAGS="$LDFLAGS -L$dir"
                done
changequote([, ])dnl
            fi
        fi
    fi
    AC_HAVE_HEADERS($4)
    AC_CHECK_LIB($2, $3)
    AC_IFALLYES(header:$4 lib:$2_$3, with_$2=yes, with_$2=no)
    if test ".$with_$2" = .no; then
        AC_ERROR([Unable to find $1 library])
    fi
    ], [dnl
if test ".$with_$2" = .; then
    with_$2=no
fi
    ])dnl
AC_MSG_CHECKING(whether to build against external $1 library)
if test ".$with_$2" = .yes; then
    ifelse([$5], , :, [$5])
else
    ifelse([$6], , :, [$6])
fi
AC_MSG_RESULT([$with_$2])
])dnl
