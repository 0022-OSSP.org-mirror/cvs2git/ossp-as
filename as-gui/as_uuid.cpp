//
//  OSSP asgui - Accounting system graphical user interface
//  Copyright (c) 2002-2004 The OSSP Project (http://www.ossp.org/)
//  Copyright (c) 2002-2004 Ralf S. Engelschall <rse@engelschall.com>
//  Copyright (c) 2002-2004 Michael Schloh von Bennewitz <michael@schloh.com>
//  Copyright (c) 2002-2004 Cable & Wireless Telecommunications Services GmbH
//
//  This file is part of OSSP asgui, an accounting system graphical user
//  interface which can be found at http://www.ossp.org/pkg/tool/asgui/.
//
//  Permission to use, copy, modify, and distribute this software for
//  any purpose with or without fee is hereby granted, provided that
//  the above copyright notice and this permission notice appear in all
//  copies.
//
//  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
//  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
//  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
//  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
//  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
//  SUCH DAMAGE.
//
//  as_uuid.cpp: ISO C++ implementation
//

// System headers
#include <string>
#include <sys/socket.h>

// Local headers
#include "as_uuid.h"
#include "as_rand.h"

// All of these are for detecting
// the MAC address in setMac()
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif // HAVE_UNISTD_H
#ifdef HAVE_SYS_SOCKIO_H
#include <sys/sockio.h>
#endif // HAVE_SYS_SOCKIO_H
#ifdef HAVE_NET_IF_H
#include <net/if.h>
#endif // HAVE_NET_IF_H
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif // HAVE_NETINET_IN_H


namespace AS {

//
// Generate a DCE standard UUID
//
void Uuid::genId(void)
{
    Rand Temprand;                  // For random numbers
    unsigned char szChardata[16];   // Intermediate data

    // Generate random data and fill in our UUID member fields with it
    Temprand.genData(szChardata, sizeof(szChardata));
    setId(szChardata);

    // Since we don't just want random data, lets take some clock sequences also
    this->clock_seq = (this->clock_seq & 0x3FFF) | 0x8000;
    this->time_hi_and_version = (this->time_hi_and_version & 0x0FFF) | 0x4000;

    // In every case that we can, set the node ID to the real MAC address
    setMac(this->node);

    setString(); // Set the human readable string
}

//
// Helper method to set the UUID private member data
//
void Uuid::setId(const unsigned char *pkucData)
{
    const U8 *pOctet = pkucData;
    U32       Tmpdat;

    Tmpdat = *pOctet++; // Tmpdat is our iterator

    // Copy data chunks one octet at a time
    Tmpdat = (Tmpdat << 8) | *pOctet++;
    Tmpdat = (Tmpdat << 8) | *pOctet++;
    Tmpdat = (Tmpdat << 8) | *pOctet++;
    this->time_low = Tmpdat;

    Tmpdat = *pOctet++;
    Tmpdat = (Tmpdat << 8) | *pOctet++;
    this->time_mid = Tmpdat;

    Tmpdat = *pOctet++;
    Tmpdat = (Tmpdat << 8) | *pOctet++;
    this->time_hi_and_version = Tmpdat;

    Tmpdat = *pOctet++;
    Tmpdat = (Tmpdat << 8) | *pOctet++;
    this->clock_seq = Tmpdat;

    // Put in the MAC address
    memcpy(this->node, pOctet, 6);
}

//
// Helper method to set up the MAC address node ID member data
//
int Uuid::setMac(unsigned char *pucNode)
{
    return 0;
}

//
// Returns a formatted representation of a DCE standard UUID
//
std::string Uuid::getString(void)
{
    return m_Fmtstr;
}

//
// Helper method to set the private formatted
// representation of a DCE standard UUID
//
void Uuid::setString(void)
{
    char szTemp[48];    // To perform intermediate manipulation

    sprintf(szTemp, // The DCE standard UUID format
        "%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x",
        this->time_low, this->time_mid, this->time_hi_and_version,
        this->clock_seq >> 8, this->clock_seq & 0xFF,
        node[0], node[1], node[2],
        node[3], node[4], node[5]);

    m_Fmtstr = szTemp;  // Finally copy the temporary to our object
}
} // namespace AS
