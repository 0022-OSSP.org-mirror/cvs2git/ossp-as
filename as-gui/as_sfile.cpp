//
//  OSSP asgui - Accounting system graphical user interface
//  Copyright (c) 2002-2004 The OSSP Project (http://www.ossp.org/)
//  Copyright (c) 2002-2004 Ralf S. Engelschall <rse@engelschall.com>
//  Copyright (c) 2002-2004 Michael Schloh von Bennewitz <michael@schloh.com>
//  Copyright (c) 2002-2004 Cable & Wireless Telecommunications Services GmbH
//
//  This file is part of OSSP asgui, an accounting system graphical user
//  interface which can be found at http://www.ossp.org/pkg/tool/asgui/.
//
//  Permission to use, copy, modify, and distribute this software for
//  any purpose with or without fee is hereby granted, provided that
//  the above copyright notice and this permission notice appear in all
//  copies.
//
//  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
//  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
//  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
//  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
//  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
//  SUCH DAMAGE.
//
//  as_sfile.cpp: ISO C++ implementation
//

#include "as_sfile.h"
#include "as_except.h"


//
// Serialize a backup of an incoming file object
//
void Simplefile::makeBackup(void)
{
    QFile Filein;           // Input readonly file
    QFile Filebak;          // Backup writeonly file
    QString Fname;          // Filename of input file
    QTextStream Streamin;   // Stream to read from (Filein)
    QTextStream Streambak;  // Stream to write to (Filebak)

    try {
        if(!this->exists())                 // Conditionally short circuit if
            return;                         // file to backup does not exist
        Fname = this->name();               // Copy filename from original
        Filein.setName(Fname);              // Set filename of original
        Filein.open(IO_ReadOnly);           // Open original read-only
        Filebak.setName(Fname + ".bak");    // Set filename of backup
        Filebak.open(IO_WriteOnly);         // Open backup write-only
        Streamin.setDevice(&Filein);        // Set incoming stream
        Streambak.setDevice(&Filebak);      // Set outgoing stream
        Streambak << Streamin.read();       // Do actual writing
        Filein.close();                     // Close original
        Filebak.close();                    // Close backup
    }
    catch (Genexcept& Genex) {
        Genex.reportErr();
    }
}
