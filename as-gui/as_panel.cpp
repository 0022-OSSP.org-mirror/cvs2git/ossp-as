//
//  OSSP asgui - Accounting system graphical user interface
//  Copyright (c) 2002-2004 The OSSP Project (http://www.ossp.org/)
//  Copyright (c) 2002-2004 Ralf S. Engelschall <rse@engelschall.com>
//  Copyright (c) 2002-2004 Michael Schloh von Bennewitz <michael@schloh.com>
//  Copyright (c) 2002-2004 Cable & Wireless Telecommunications Services GmbH
//
//  This file is part of OSSP asgui, an accounting system graphical user
//  interface which can be found at http://www.ossp.org/pkg/tool/asgui/.
//
//  Permission to use, copy, modify, and distribute this software for
//  any purpose with or without fee is hereby granted, provided that
//  the above copyright notice and this permission notice appear in all
//  copies.
//
//  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
//  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
//  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
//  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
//  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
//  SUCH DAMAGE.
//
//  as_panel.cpp: ISO C++ implementation
//

#include <vector>

#include <qimage.h>

#include "as_panel.h"
#include "as_generic.h"
#include "as_const.h"

#include "as_gfx/revcolour.xpm"

//
// Constructs a Prefpanel as a child of 'pParent', with the 
// name 'kszName' and widget flags set to 'Flags'.
//
// The dialog will by default be modal, unless you set 'bModal' to
// false to construct a modeless dialog.
//
Prefpanel::Prefpanel(QWidget *pParent, const char *kszName, bool bModal, WFlags Flags)
    : QDialog(pParent, kszName, bModal, Flags)
{
    if (!kszName)
        this->setName("Prefpanel");

    this->setSizeGripEnabled(false);
    this->setSizePolicy(QSizePolicy((QSizePolicy::SizeType)5,
        (QSizePolicy::SizeType)5, 0, 0, this->sizePolicy().hasHeightForWidth()));

    // Initialize our button colours
    m_pLight = new QColor();
    m_pDark = new QColor();

    m_pVlayout = new QVBoxLayout(this, 11, 6, "Formlayout");
    m_pTabselect = new QTabWidget(this, "Tabselector");

    // Start of style chooser UI pieces
    m_pBoolpage = new QWidget(m_pTabselect, "Tabpage");
    m_pBoolayout = new QHBoxLayout(m_pBoolpage, 11, 6, "Genlayout"); 
    m_pBinlayout = new QVBoxLayout(0, 0, 6, "Binarylayout"); 
    m_pStylegroup = new QButtonGroup(m_pBoolpage, "Stylebuttongroup");
    m_pStylegroup->setColumnLayout(0, Qt::Vertical);
    m_pStylegroup->layout()->setSpacing(6);
    m_pStylegroup->layout()->setMargin(11);
    m_pStylegroup->setExclusive(true);

    m_pStylelay = new QVBoxLayout(m_pStylegroup->layout());
    m_pStylelay->setAlignment(Qt::AlignTop);

    // Block to handle a vector of radio buttons
    int nIter = 0;
    QStringList Styles = QStyleFactory::keys();
    std::vector<QRadioButton *> Stylevector(Styles.size());

    // Discover which interface styles are available, and
    // dynamically construct new radio buttons to represent them
    for (QStringList::Iterator Stylename = Styles.begin();
        Stylename != Styles.end(); Stylename++) {
        Stylevector[nIter] = new QRadioButton(m_pStylegroup, *Stylename + "_button");
        Stylevector[nIter]->setText(trUtf8(*Stylename, "Comment for " + *Stylename));
        Stylevector[nIter]->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding,
            Stylevector[nIter]->sizePolicy().hasHeightForWidth());
        QToolTip::add(Stylevector[nIter], trUtf8("The " + *Stylename + " style",
            "Comment for toolTip " + *Stylename));
        QWhatsThis::add(Stylevector[nIter], trUtf8("Click this button to enjoy the style of the "
            + *Stylename + " user interface", "Comment whatsThis for " + *Stylename));
        m_pStylelay->addWidget(Stylevector[nIter]);
        nIter++;
    }

    // Start of binary option UI pieces (like make backups, yes or no)
    m_pSwitchgroup = new QButtonGroup(m_pBoolpage, "Switchbuttongroup");
    m_pSwitchgroup->setColumnLayout(0, Qt::Vertical);
    m_pSwitchgroup->layout()->setSpacing(6);
    m_pSwitchgroup->layout()->setMargin(11);
    m_pSwitchgroup->setExclusive(false);
    m_pSwitchlay = new QVBoxLayout(m_pSwitchgroup->layout());
    m_pSwitchlay->setAlignment(Qt::AlignTop);

    // Optional file backups with hard coded extension .bak
    m_pBackupcheck = new QCheckBox(m_pSwitchgroup, "Backupbutton");
    m_pBackupcheck->setText(trUtf8("Make .bak file on save", "Comment for Backupcheck"));
    QToolTip::add(m_pBackupcheck, trUtf8("Make a .bak file when saving", "Comment for toolTip Backupcheck"));
    QWhatsThis::add(m_pBackupcheck, trUtf8("Check this box to enable automatic file backups when overwriting an existing file", "Comment whatsThis for Backupcheck"));
    m_pSwitchlay->addWidget(m_pBackupcheck);

    // Optional easy filename appending with hard coded extension .as
    m_pExtendcheck = new QCheckBox(m_pSwitchgroup, "Extensionbutton");
    m_pExtendcheck->setText(trUtf8("Append .as extension", "Comment for Extensioncheck"));
    QToolTip::add(m_pExtendcheck, trUtf8("Use the .as file extension", "Comment for toolTip Extensioncheck"));
    QWhatsThis::add(m_pExtendcheck, trUtf8("Check this box to automatically append '.as' to new filenames when saving", "Comment whatsThis for Extensioncheck"));
    m_pSwitchlay->addWidget(m_pExtendcheck);

    // Start of report option UI pieces (like detailed report listings)
    m_pReportgroup = new QButtonGroup(m_pBoolpage, "Reportbuttongroup");
    m_pReportgroup->setColumnLayout(0, Qt::Vertical);
    m_pReportgroup->layout()->setSpacing(6);
    m_pReportgroup->layout()->setMargin(11);
    m_pReportgroup->setExclusive(false);
    m_pReportlay = new QVBoxLayout(m_pReportgroup->layout());
    m_pReportlay->setAlignment(Qt::AlignTop);

    // Optional detailed report listings write all events in range
    m_pDetailcheck = new QCheckBox(m_pReportgroup, "Detailistbutton");
    m_pDetailcheck->setText(trUtf8("Detailed listing", "Comment for Detailcheck"));
    QToolTip::add(m_pDetailcheck, trUtf8("Write detailed event listings", "Comment for toolTip Detailcheck"));
    QWhatsThis::add(m_pDetailcheck, trUtf8("Check this box to enable writing of detailed event listings to the local report", "Comment whatsThis for Detailcheck"));
    m_pReportlay->addWidget(m_pDetailcheck);

    // Optional signature line in report footer
    m_pSigncheck = new QCheckBox(m_pReportgroup, "Signaturebutton");
    m_pSigncheck->setText(trUtf8("Signature line", "Comment for Signaturecheck"));
    QToolTip::add(m_pSigncheck, trUtf8("Append a signature line", "Comment for toolTip Signaturecheck"));
    QWhatsThis::add(m_pSigncheck, trUtf8("Check this box to write a signature line to the report footer", "Comment whatsThis for Signaturecheck"));
    m_pReportlay->addWidget(m_pSigncheck);
    m_pBoolayout->addWidget(m_pStylegroup);
    m_pBinlayout->addWidget(m_pReportgroup);
    m_pBinlayout->addWidget(m_pSwitchgroup);
    m_pBoolayout->addLayout(m_pBinlayout);
    m_pTabselect->insertTab(m_pBoolpage, "");

    // Start of paths page UI pieces
    m_pGeneralpage = new QWidget(m_pTabselect, "Generalpage");
    m_pGenlayout = new QHBoxLayout(m_pGeneralpage, 11, 6, "Genlayout");
    m_pGenbox = new QGroupBox(m_pGeneralpage, "Groupboxlayout");
    m_pGenbox->setColumnLayout(0, Qt::Vertical);
    m_pGenbox->layout()->setSpacing(6);
    m_pGenbox->layout()->setMargin(11);
    m_pGenboxlay = new QHBoxLayout(m_pGenbox->layout());
    m_pGenboxlay->setAlignment(Qt::AlignTop);

    // Start of path text entry preferences UI pieces
    m_pGeninner = new QGridLayout(0, 4, 2, 0, 16, "Innergrid");
    m_pAcctlabel = new QLabel(m_pGenbox, "Accfilelabel");
    m_pGeninner->addWidget(m_pAcctlabel, 0, 0);
    m_pAcctline = new QLineEdit(m_pGenbox, "Accountline");
    m_pGeninner->addWidget(m_pAcctline, 0, 1);
    m_pEventlabel = new QLabel(m_pGenbox, "Evntdirlabel");
    m_pGeninner->addWidget(m_pEventlabel, 1, 0);
    m_pEventline = new QLineEdit(m_pGenbox, "Eventline");
    m_pGeninner->addWidget(m_pEventline, 1, 1);
    m_pHomelabel = new QLabel(m_pGenbox, "Homelabel");
    m_pGeninner->addWidget(m_pHomelabel, 2, 0);
    m_pHomeline = new QLineEdit(m_pGenbox, "Homeline");
    m_pGeninner->addWidget(m_pHomeline, 2, 1);
    m_pUserlabel = new QLabel(m_pGenbox, "Userlabel");
    m_pGeninner->addWidget(m_pUserlabel, 3, 0);
    m_pUserline = new QLineEdit(m_pGenbox, "Userline");
    m_pGeninner->addWidget(m_pUserline, 3, 1);
    m_pGenboxlay->addLayout(m_pGeninner);

    m_pGenlayout->addWidget(m_pGenbox);
    m_pTabselect->insertTab(m_pGeneralpage, "");

    // Start of remote page UI pieces
    m_pRemotepage = new QWidget(m_pTabselect, "Remotepage");
    m_pRemlayout = new QHBoxLayout(m_pRemotepage, 11, 6, "Remlayout");
    m_pRembox = new QGroupBox(m_pRemotepage, "Remoteboxlayout");

    m_pRembox->setColumnLayout(0, Qt::Vertical);
    m_pRembox->layout()->setSpacing(6);
    m_pRembox->layout()->setMargin(11);
    m_pRemboxlay = new QHBoxLayout(m_pRembox->layout());
    m_pRemboxlay->setAlignment(Qt::AlignTop);

    // Start of RPC preferences UI pieces
    m_pReminner = new QGridLayout(0, 5, 2, 0, 12, "Innergrid");
    m_pCorbalabel = new QLabel(m_pRembox, "Corbalabel");
    m_pReminner->addWidget(m_pCorbalabel, 0, 0);
    m_pCorbaline = new QLineEdit(m_pRembox, "Corbaline");
    m_pReminner->addWidget(m_pCorbaline, 0, 1);
    m_pCorbacheck = new QCheckBox(m_pRembox, "Corbacheckbox");
    m_pReminner->addWidget(m_pCorbacheck, 1, 1);
    QSpacerItem *Spacey = new QSpacerItem(6, 12, QSizePolicy::Minimum, QSizePolicy::Minimum);
    m_pReminner->addItem(Spacey, 2, 0);
    m_pSoaplabel = new QLabel(m_pRembox, "Soaplabel");
    m_pReminner->addWidget(m_pSoaplabel, 3, 0);
    m_pSoapline = new QLineEdit(m_pRembox, "Soapline");
    m_pReminner->addWidget(m_pSoapline, 3, 1);
    m_pSoapcheck = new QCheckBox(m_pRembox, "Corbacheckbox");
    m_pReminner->addWidget(m_pSoapcheck, 4, 1);
    m_pRemboxlay->addLayout(m_pReminner);

    m_pRemlayout->addWidget(m_pRembox);
    m_pTabselect->insertTab(m_pRemotepage, "");

    // Start of color page UI pieces
    m_pColourpage = new QWidget(m_pTabselect, "Colourpage");
    m_pColourlayout = new QHBoxLayout(m_pColourpage, 11, 6, "Colourlayout");
    m_pShadebox = new QGroupBox(m_pColourpage, "Shadegroupbox");
    m_pShadebox->setColumnLayout(0, Qt::Horizontal);
    m_pShadebox->layout()->setSpacing(6);
    m_pShadebox->layout()->setMargin(11);
    m_pShadeboxlay = new QVBoxLayout(m_pShadebox->layout());
    m_pShadeboxlay->setAlignment(Qt::AlignTop);

    m_pShadelayout = new QGridLayout(0, 2, 2, 6, 16, "Shadelayout");
    m_pLightlab = new QLabel(m_pShadebox, "Lightlabel");
    m_pLightlab->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred, this->sizePolicy().hasHeightForWidth());
    m_pShadelayout->addWidget(m_pLightlab, 0, 0);
    m_pLightbutt = new QToolButton(m_pShadebox, "Lightbutton");
    m_pLightbutt->setFocusPolicy(QToolButton::TabFocus);
    m_pLightbutt->setCursor(QCursor(13));
    m_pShadelayout->addWidget(m_pLightbutt, 0, 1);

    m_pDarklab = new QLabel(m_pShadebox, "Darklabel");
    m_pDarklab->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred, this->sizePolicy().hasHeightForWidth());
    m_pShadelayout->addWidget(m_pDarklab, 1, 0);
    m_pDarkbutt = new QToolButton(m_pShadebox, "Darkbutton");
    m_pDarkbutt->setFocusPolicy(QToolButton::TabFocus);
    m_pDarkbutt->setCursor(QCursor(13));
    m_pShadelayout->addWidget(m_pDarkbutt, 1, 1);

    m_pRevertlab = new QLabel(m_pShadebox, "Revertlabel");
    m_pShadelayout->addWidget(m_pRevertlab, 2, 0);
    m_pRevertbutt = new QToolButton(m_pShadebox, "Revertbutton");
    m_pRevertbutt->setFocusPolicy(QToolButton::TabFocus);
//    m_pRevertbutt->setIconSet(QIconSet(QPixmap(s_kpcRevcolour_xpm)));
    m_pRevertbutt->setCursor(QCursor(13));
    m_pShadelayout->addWidget(m_pRevertbutt, 2, 1);

    m_pThemebox = new QGroupBox(m_pColourpage, "Themegroupbox");
    m_pThemebox->setColumnLayout(0, Qt::Vertical);
    m_pThemebox->layout()->setSpacing(6);
    m_pThemebox->layout()->setMargin(11);
    m_pThemeboxlay = new QVBoxLayout(m_pThemebox->layout());

    QLabel *pPlaceholder = new QLabel(m_pThemebox, "Placeholderlabel");
    pPlaceholder->setText(trUtf8("Reserved for future use", "Comment for Placeholderlabel"));
    m_pThemeboxlay->addWidget(pPlaceholder, 1, Qt::AlignCenter);

    m_pShadeboxlay->addLayout(m_pShadelayout);
    m_pColourlayout->addWidget(m_pShadebox);
    m_pColourlayout->addWidget(m_pThemebox);
    m_pTabselect->insertTab(m_pColourpage, "");

    connect(m_pLightbutt, SIGNAL(clicked(void)), SLOT(shadeLight(void)));
    connect(m_pDarkbutt, SIGNAL(clicked(void)), SLOT(shadeDark(void)));
    connect(m_pRevertbutt, SIGNAL(clicked(void)), SLOT(shadeRevert(void)));

    // Finally add the damn pages to the tab selector widget!
    m_pVlayout->addWidget(m_pTabselect);

    // Start of bottom buttons for aknowlegement and commital of changes
    m_pButtlay = new QHBoxLayout(0, 0, 12, "Buttonlayout");
    m_pOkaybutton = new QPushButton(this, "Okaybutton");
    m_pOkaybutton->setMinimumWidth(120);
    m_pOkaybutton->setPaletteBackgroundColor(QColor(202, 194, 182));
    m_pOkaybutton->setCursor(QCursor(13));
    m_pButtlay->addWidget(m_pOkaybutton);
    m_pApplybutton = new QPushButton(this, "Applybutton");
    m_pApplybutton->setMinimumWidth(120);
    m_pApplybutton->setPaletteBackgroundColor(QColor(198, 196, 186));
    m_pApplybutton->setCursor(QCursor(13));
    m_pButtlay->addWidget(m_pApplybutton);
//    QSpacerItem *Spacey = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
//    m_pButtlay->addItem(Spacey);
    m_pCancelbutton = new QPushButton(this, "Cancelbutton");
    m_pCancelbutton->setMinimumWidth(120);
    m_pCancelbutton->setPaletteBackgroundColor(QColor(198, 196, 186));
    m_pCancelbutton->setCursor(QCursor(13));
    m_pButtlay->addWidget(m_pCancelbutton);
    m_pCancelbutton->setDefault(true);
    m_pVlayout->addLayout(m_pButtlay);

    // Connect our signals to slots, accept() and reject() are Qt implicit
    connect(m_pOkaybutton, SIGNAL(clicked(void)), SLOT(accept(void)));
    connect(m_pApplybutton, SIGNAL(clicked(void)), SIGNAL(applied(void)));
    connect(m_pCancelbutton, SIGNAL(clicked(void)), SLOT(reject(void)));
    connect(m_pCorbacheck, SIGNAL(toggled(bool)), SLOT(enableCorba(bool)));
    connect(m_pSoapcheck, SIGNAL(toggled(bool)), SLOT(enableSoap(bool)));
    this->textChange();
    this->resize(QSize(400, 264).expandedTo(minimumSizeHint()));
}

//
// Sets the light shading colour for rows
//
void Prefpanel::shadeLight(void)
{
    QColorDialog::setCustomColor(0, this->getRevlight()->rgb());
    QColorDialog::setCustomColor(1, this->getRevdark()->rgb());
    QColorDialog::setCustomColor(2, this->getAltlight()->rgb());
    QColorDialog::setCustomColor(3, this->getAltdark()->rgb());
    QColor Colortemp = QColorDialog::getColor(*m_pLight);
    if (Colortemp.isValid()) {
        delete m_pLight;
        m_pLight = new QColor(Colortemp);
        m_pLightbutt->setPaletteBackgroundColor(*m_pLight);
    }
}

//
// Sets the dark shading colour for rows
//
void Prefpanel::shadeDark(void)
{
    QColorDialog::setCustomColor(0, m_pLight->rgb());
    QColorDialog::setCustomColor(1, m_pDark->rgb());
    QColor Colortemp = QColorDialog::getColor(*m_pDark);
    if (Colortemp.isValid()) {
        delete m_pDark;
        m_pDark = new QColor(Colortemp);
        m_pDarkbutt->setPaletteBackgroundColor(*m_pDark);
    }
}

//
// Reverts the shading colours to default values
//
void Prefpanel::shadeRevert(void)
{
    delete m_pLight;
    m_pLight = new QColor(*this->getRevlight());
    m_pLightbutt->setPaletteBackgroundColor(*m_pLight);
    delete m_pDark;
    m_pDark = new QColor(*this->getRevdark());
    m_pDarkbutt->setPaletteBackgroundColor(*m_pDark);
}

//
// Polish off ending changes after creating and before widget shows
//
void Prefpanel::polish(void)
{
    // Set our buttons now that user has probably set their colours
    m_pLightbutt->setPaletteBackgroundColor(*m_pLight);
    m_pDarkbutt->setPaletteBackgroundColor(*m_pDark);
}

//
// Sets the strings of the subwidgets using the current language
//
void Prefpanel::textChange(void)
{
    this->setCaption(trUtf8("AS Applicate Preferences", "Personal preferences are persistent across sessions"));

    m_pTabselect->changeTab(m_pBoolpage, trUtf8("General"));
    m_pTabselect->changeTab(m_pGeneralpage, trUtf8("Paths"));
    m_pTabselect->changeTab(m_pRemotepage, trUtf8("Hosts"));
    m_pTabselect->changeTab(m_pColourpage, trUtf8("Colour"));

    m_pOkaybutton->setText(trUtf8("Okay", "Comment for Okaybutton"));
    QToolTip::add(m_pOkaybutton, trUtf8("Applies and saves changes", "Comment for tooltip Okaybutton"));
    QWhatsThis::add(m_pOkaybutton, trUtf8("The okay button applies and saves changes", "Comment for whatsThis Okaybutton"));

    m_pApplybutton->setText(trUtf8("Apply", "Comment for Applybutton"));
    QToolTip::add(m_pApplybutton, trUtf8("Apply changes immediately", "Comment for toolTip Applybutton"));
    QWhatsThis::add(m_pApplybutton, trUtf8("The apply button applies changes immediately", "Comment for whatsThis Applybutton"));

    m_pCancelbutton->setText(trUtf8("Cancel", "Comment for Cancelbutton"));
    QToolTip::add(m_pCancelbutton, trUtf8("Cancel any changes", "Comment for toolTip Cancelbutton"));
    QWhatsThis::add(m_pCancelbutton, trUtf8("The cancel button cancels any changes", "Comment for whatsThis Cancelbutton"));

    m_pGenbox->setTitle(trUtf8("File and directory paths", "Comment for Genbox"));
    m_pAcctlabel->setText(trUtf8("Accounts path", "Comment for Acctlabel"));
    m_pEventlabel->setText(trUtf8("Events directory", "Comment for Eventlabel"));
    m_pUserlabel->setText(trUtf8("User name", "Comment for Userlabel"));
    m_pHomelabel->setText(trUtf8("Home directory", "Comment for Homelabel"));
    QToolTip::add(m_pAcctline, trUtf8("The accounts file pathname", "Comment for toolTip Acctline"));
    QToolTip::add(m_pEventline, trUtf8("The default events directory", "Comment for toolTip Eventline"));
    QToolTip::add(m_pUserline, trUtf8("The user name", "Comment for toolTip Userline"));
    QToolTip::add(m_pHomeline, trUtf8("The home directory", "Comment for toolTip Homeline"));

    m_pRembox->setTitle(trUtf8("Remote host names", "Comment for Rembox"));
    m_pCorbalabel->setText(trUtf8("CORBA host", "Comment for Corbalabel"));
    m_pSoaplabel->setText(trUtf8("SOAP host", "Comment for Soaplabel"));
    QToolTip::add(m_pCorbaline, trUtf8("The CORBA host name", "Comment for toolTip Corbaline"));
    QToolTip::add(m_pSoapline, trUtf8("The SOAP host name", "Comment for toolTip Soapline"));

    m_pCorbacheck->setText(trUtf8("Enable IIOP transmission", "Comment for Corbacheck"));
    QToolTip::add(m_pCorbacheck, trUtf8("Will enable transmission over IIOP", "Comment for toolTip Corbacheck"));
    QWhatsThis::add(m_pCorbacheck, trUtf8("Check this box to enable transmission to a IIOP host", "Comment whatsThis for Corbacheck"));

    m_pSoapcheck->setText(trUtf8("Enable SOAP transmission", "Comment for Soapcheck"));
    QToolTip::add(m_pSoapcheck, trUtf8("Will enable transmission over SOAP", "Comment for toolTip Soapcheck"));
    QWhatsThis::add(m_pSoapcheck, trUtf8("Check this box to enable transmission to a SOAP host", "Comment whatsThis for Soapcheck"));

    m_pShadebox->setTitle(trUtf8("Row shading", "Comment for Shadebox"));
    m_pThemebox->setTitle(trUtf8("Skin themes", "Comment for Themebox"));

    m_pLightlab->setText(trUtf8("Light", "Comment for Lightlabel"));
    m_pDarklab->setText(trUtf8("Dark", "Comment for Darklabel"));
    m_pRevertlab->setText(trUtf8("Revert", "Comment for Revertlabel"));
    QToolTip::add(m_pLightbutt, trUtf8("Light row shading colour", "Comment for tooltip Lightbutt"));
    QWhatsThis::add(m_pLightbutt, trUtf8("The Light button sets the light row shading colour.", "Comment for whatsThis Lightbutt"));
    QToolTip::add(m_pDarkbutt, trUtf8("Dark row shading colour", "Comment for tooltip Darkbutt"));
    QWhatsThis::add(m_pDarkbutt, trUtf8("The Dark button sets the light row shading colour.", "Comment for whatsThis Darkbutt"));

    m_pStylegroup->setTitle(trUtf8("Available styles", "Comment for Stylebuttons"));
    m_pReportgroup->setTitle(trUtf8("Report options", "Comment for Reportbox"));
    m_pSwitchgroup->setTitle(trUtf8("Other options", "Comment for Switchbox"));
}
