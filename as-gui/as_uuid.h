//
//  OSSP asgui - Accounting system graphical user interface
//  Copyright (c) 2002-2004 The OSSP Project (http://www.ossp.org/)
//  Copyright (c) 2002-2004 Ralf S. Engelschall <rse@engelschall.com>
//  Copyright (c) 2002-2004 Michael Schloh von Bennewitz <michael@schloh.com>
//  Copyright (c) 2002-2004 Cable & Wireless Telecommunications Services GmbH
//
//  This file is part of OSSP asgui, an accounting system graphical user
//  interface which can be found at http://www.ossp.org/pkg/tool/asgui/.
//
//  Permission to use, copy, modify, and distribute this software for
//  any purpose with or without fee is hereby granted, provided that
//  the above copyright notice and this permission notice appear in all
//  copies.
//
//  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
//  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
//  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
//  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
//  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
//  SUCH DAMAGE.
//
//  as_uuid.h: ISO C++ interface
//

#ifndef UUIDGEN_H
#define UUIDGEN_H

#include <string>

#include "as_gui.h"


namespace AS {

//
// A DCE standard UUID, can generate (multiple)
// IDs and format them for a printed output
//
class Uuid
{
private:
    std::string m_Fmtstr;               // Human readable format
    U32 time_low;                       //
    U16 time_mid;                       // Fields according to
    U16 time_hi_and_version;            // the IETF specification
    U16 clock_seq;                      // draft-mealling-uuid-urn-00
    U8  node[6];                        //

public:
    void genId(void);                   // Generate an UUID
    void setString(void);               // Sets the formatted representation
    std::string getString(void);        // Return a formatted representation

private:
    void setId(const unsigned char *);  // Helper method to set data members
    int setMac(unsigned char *);        // Helper method to set MAC node ID
};
} // namespace AS

#endif // UUIDGEN_H
