//
//  OSSP asgui - Accounting system graphical user interface
//  Copyright (c) 2002-2004 The OSSP Project (http://www.ossp.org/)
//  Copyright (c) 2002-2004 Ralf S. Engelschall <rse@engelschall.com>
//  Copyright (c) 2002-2004 Michael Schloh von Bennewitz <michael@schloh.com>
//  Copyright (c) 2002-2004 Cable & Wireless Telecommunications Services GmbH
//
//  This file is part of OSSP asgui, an accounting system graphical user
//  interface which can be found at http://www.ossp.org/pkg/tool/asgui/.
//
//  Permission to use, copy, modify, and distribute this software for
//  any purpose with or without fee is hereby granted, provided that
//  the above copyright notice and this permission notice appear in all
//  copies.
//
//  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
//  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
//  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
//  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
//  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
//  SUCH DAMAGE.
//
//  as_reportpanel.h: ISO C++ interface
//

#ifndef REPORTPANEL_H
#define REPORTPANEL_H

#include <qvariant.h>
#include <qdialog.h>
#include <qdatetime.h>


class TiTable;
class Preferences;
class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QPushButton;
class QTextEdit;
class QToolButton;
class QButtonGroup;
class QPopupMenu;
class QPrinter;

namespace AS {

class Reportpanel : public QDialog
{
    Q_OBJECT

public:
    Reportpanel(TiTable *, Preferences *, QWidget *pParent = 0,
                const char *kszName = 0, bool bModal = true, WFlags Flags = 0);
//    ~Reportpanel(void); // No need to destroy widgets, because qt does it for us

public:
    QButtonGroup *m_pWeekmonthgroup;
    QToolButton  *m_pWeeklybutt;
    QToolButton  *m_pMonthlybutt;
    QTextEdit    *m_pBrowser;
    QPushButton  *m_pSavebutt;
    QPushButton  *m_pDismissbutt;
    QPushButton  *m_pPrintbutt;

protected:
    QVBoxLayout *m_pFormlay;
    QVBoxLayout *m_pGrouplay;
    QHBoxLayout *m_pToolay;
    QHBoxLayout *m_pPushlay;
    QHBoxLayout *m_pWeekmonthlay;
    QPopupMenu  *m_pWeekpop;
    QPopupMenu  *m_pMonthpop;

private:
    int          m_nWeeks;
    int          m_nMonths;
    TiTable     *m_pReptable;
    Preferences *m_pReprefs;
#ifndef QT_NO_PRINTER
    QPrinter    *m_pPrinter;
#endif

public slots:
    int exec(void);

protected slots:
    virtual void saveReport(void);
    virtual void printReport(void);
    virtual void textChange(void);
    virtual void reportWeeks(int nMenuid = -1);
    virtual void reportMonths(int nMenuid = -1);

private:
    void writeHeader(int);
    void writeHeader(int, int);
    void writeHeader(QString);
    void writeHeader(QString, QString);
    void writeFooter(void);
    QString getWeektotals(QDate Refer = QDate::currentDate(), int nWeeks = 1);
    QString getWeekdetails(QDate Refer = QDate::currentDate(), int nWeeks = 1);
    QString getMonthtotals(QDate Refer = QDate::currentDate(), int nMonths = 1);
    QString getMonthdetails(QDate Refer = QDate::currentDate(), int nMonths = 1);
};
} // namespace AS

#endif // REPORTPANEL_H
