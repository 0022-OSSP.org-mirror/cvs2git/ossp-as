//
//  OSSP asgui - Accounting system graphical user interface
//  Copyright (c) 2002-2004 The OSSP Project (http://www.ossp.org/)
//  Copyright (c) 2002-2004 Ralf S. Engelschall <rse@engelschall.com>
//  Copyright (c) 2002-2004 Michael Schloh von Bennewitz <michael@schloh.com>
//  Copyright (c) 2002-2004 Cable & Wireless Telecommunications Services GmbH
//
//  This file is part of OSSP asgui, an accounting system graphical user
//  interface which can be found at http://www.ossp.org/pkg/tool/asgui/.
//
//  Permission to use, copy, modify, and distribute this software for
//  any purpose with or without fee is hereby granted, provided that
//  the above copyright notice and this permission notice appear in all
//  copies.
//
//  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
//  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
//  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
//  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
//  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
//  SUCH DAMAGE.
//
//  as_numdial.cpp: ISO C++ implementation
//

#include "as_numdial.h"

#include <qvariant.h>
#include <qframe.h>
#include <qlabel.h>
#include <qpushbutton.h>
#include <qspinbox.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

#include "as_const.h"


namespace AS {

//
// Constructs a Numdial as a child of 'pParent', with the 
// name 'kszName' and widget flags set to 'Flags'.
//
// The dialog will by default be modal, unless you set 'bModal' to
// false to construct a modeless dialog.
//
Numdial::Numdial(QWidget *pParent, const char *kszName, bool bModal, WFlags Flags)
    : QDialog(pParent, kszName, bModal, Flags)
{
    // Boilerplate code to initialize the panel
    if (!kszName)
        this->setName("Numberdialog");

    // Fix dialog size
    this->setSizeGripEnabled(false);
    this->setSizePolicy(QSizePolicy((QSizePolicy::SizeType)0,
        (QSizePolicy::SizeType)0, 0, 0, this->sizePolicy().hasHeightForWidth()));

    m_pFrame = new QFrame(this, "Mainframe");
    m_pFrame->setFrameShape(QFrame::StyledPanel);
    m_pFrame->setFrameShadow(QFrame::Raised);
    m_pFramelay = new QVBoxLayout(m_pFrame, 11, 6, "Framelay"); 
    m_pInputlay = new QHBoxLayout(0, 0, 6, "Horizontallayout"); 
    m_pFormlayout = new QVBoxLayout(this, 11, 6, "Formlayout"); 

    m_pReportlabel = new QLabel(m_pFrame, "Reportlabel");
    m_pReportlabel->setSizePolicy(QSizePolicy((QSizePolicy::SizeType)4, (QSizePolicy::SizeType)5, 0, 0, m_pReportlabel->sizePolicy().hasHeightForWidth()));
    m_pInputlay->addWidget(m_pReportlabel);

    m_pSpinbox = new QSpinBox(m_pFrame, "Spinbox");
    m_pSpinbox->setMinValue(1);                 // Must choose at least one week
    m_pSpinbox->setMaxValue(TITRAQ_BIGMAGIC);   // Must choose less than magic
//    m_pSpinbox->setButtonSymbols(QSpinBox::PlusMinus);
//    m_pSpinbox->editor()->setAlignment(Qt::AlignRight);
    m_pInputlay->addWidget(m_pSpinbox);

    m_pWeeklabel = new QLabel(m_pFrame, "Weeklabel");
    m_pWeeklabel->setSizePolicy(QSizePolicy((QSizePolicy::SizeType)4, (QSizePolicy::SizeType)5, 0, 0, m_pWeeklabel->sizePolicy().hasHeightForWidth()));
    m_pInputlay->addWidget(m_pWeeklabel);
    m_pFramelay->addLayout(m_pInputlay);

    // Push button suite
    m_pButtlay = new QHBoxLayout(0, 0, 6, "Buttlay");
    m_pOkaybutt = new QPushButton(m_pFrame, "Okaybutton");
    m_pOkaybutt->setPaletteBackgroundColor(QColor(198, 196, 186));
    m_pOkaybutt->setCursor(QCursor(13));
    m_pOkaybutt->setDefault(true);
    m_pButtlay->addWidget(m_pOkaybutt);
    m_pCancelbutt = new QPushButton(m_pFrame, "Cancelbutton");
    m_pCancelbutt->setPaletteBackgroundColor(QColor(198, 196, 186));
    m_pCancelbutt->setCursor(QCursor(13));
    m_pButtlay->addWidget(m_pCancelbutt);
    m_pFramelay->addLayout(m_pButtlay);

    // Add all the junk to our main frame
    m_pFormlayout->addWidget(m_pFrame);

    // Connect signals to slots, accept() and reject() are Qt implicit
    connect(m_pOkaybutt, SIGNAL(clicked(void)), SLOT(accept(void)));
    connect(m_pCancelbutt, SIGNAL(clicked(void)), SLOT(reject(void)));
    this->resize(QSize(272, 111).expandedTo(minimumSizeHint()));
    this->textChange();
}

//
//  Sets the strings of the subwidgets using the current language
//
void Numdial::textChange()
{
    this->setCaption(trUtf8("Numberdialog", "For general input of numbers followed by an accept or cancel operation"));
    m_pReportlabel->setText(trUtf8("Report"));
    m_pWeeklabel->setText(trUtf8("weeks of data"));

    // Top level push buttons associated with accept slot
    m_pCancelbutt->setText(trUtf8("Cancel", "Comment for Cancelbutton"));
    QToolTip::add(m_pCancelbutt, trUtf8("Closes the report panel", "Comment for tooltip Cancelbutton"));
    QWhatsThis::add(m_pCancelbutt, trUtf8("The cancel button closes the report panel without further action", "Comment for whatsThis Cancelbutton"));
    m_pOkaybutt->setText(trUtf8("Okay", "Comment for Okaybutton"));
    QToolTip::add(m_pOkaybutt, trUtf8("Aknowleges your input", "Comment for tooltip Okaybutton"));
    QWhatsThis::add(m_pOkaybutt, trUtf8("The okay button aknowleges your input and prepares for action", "Comment for whatsThis Okaybutton"));

    // Text help for spinbox number input
    QToolTip::add(m_pSpinbox, trUtf8("Input a number", "Comment for tooltip Spinbox"));
    QWhatsThis::add(m_pSpinbox, trUtf8("Click on the arrows or type to input a number", "Comment for whatsThis Spinbox"));
}
} // namespace AS
