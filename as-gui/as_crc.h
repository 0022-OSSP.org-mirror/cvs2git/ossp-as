//
//  OSSP asgui - Accounting system graphical user interface
//  Copyright (c) 2002-2004 The OSSP Project (http://www.ossp.org/)
//  Copyright (c) 2002-2004 Ralf S. Engelschall <rse@engelschall.com>
//  Copyright (c) 2002-2004 Michael Schloh von Bennewitz <michael@schloh.com>
//  Copyright (c) 2002-2004 Cable & Wireless Telecommunications Services GmbH
//
//  This file is part of OSSP asgui, an accounting system graphical user
//  interface which can be found at http://www.ossp.org/pkg/tool/asgui/.
//
//  Permission to use, copy, modify, and distribute this software for
//  any purpose with or without fee is hereby granted, provided that
//  the above copyright notice and this permission notice appear in all
//  copies.
//
//  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
//  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
//  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
//  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
//  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
//  SUCH DAMAGE.
//
//  as_crc.h: ISO C++ interface
//

#ifndef CRC_H
#define CRC_H

#include <string>       // For copy constructor using string
#include <qstring.h>    // For deriving from QString

#include "as_gui.h"     // General definitions like U32 typedefs


class Qualistring : public QString
{
private:
    U32 m_pkHash[256];          // Used for CRC32 value generation

private:
    U32 mirrorBits(U32, U8);    // Helper method, mirror a bitstream
    void initCrc(void);         // Helper method, fill hash table

public:                                                                 // Constructors
    Qualistring(void) : QString() {initCrc();};                          // Default
    Qualistring(const QString &kCopy) : QString(kCopy) {initCrc();};     // Copy
    Qualistring(const char *pkcCopy) : QString(pkcCopy) {initCrc();};    // Copy
    Qualistring(const std::string &kCopy) : QString(kCopy) {initCrc();}; // Copy

    U32 getCrc(void);                           // Generate and return a CRC32
    Qualistring &operator=(const QString &);    // Overload equals operator
};

#endif // CRC_H
