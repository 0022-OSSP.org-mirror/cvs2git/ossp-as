//
//  OSSP asgui - Accounting system graphical user interface
//  Copyright (c) 2002-2004 The OSSP Project (http://www.ossp.org/)
//  Copyright (c) 2002-2004 Ralf S. Engelschall <rse@engelschall.com>
//  Copyright (c) 2002-2004 Michael Schloh von Bennewitz <michael@schloh.com>
//  Copyright (c) 2002-2004 Cable & Wireless Telecommunications Services GmbH
//
//  This file is part of OSSP asgui, an accounting system graphical user
//  interface which can be found at http://www.ossp.org/pkg/tool/asgui/.
//
//  Permission to use, copy, modify, and distribute this software for
//  any purpose with or without fee is hereby granted, provided that
//  the above copyright notice and this permission notice appear in all
//  copies.
//
//  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
//  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
//  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
//  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
//  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
//  SUCH DAMAGE.
//
//  as_const.h: ISO C++ interface
//

#ifndef TITCONST_H
#define TITCONST_H


// General preferences
#define TITRAQ_PREFNAME         ".asgui"
#define TITRAQ_APPTITLE         "AS Accounting System"
#define TITRAQ_STR_ID           "OSSPASGUI"                         /* APPID */
#define TITRAQ_UID_ID           0x84fae747e1a64016bf049e98bbc4bd96  /* UUID */
#define TITRAQ_PREFVER          "0.6"
#define TITRAQ_PREFHOME         "homedir"
#define TITRAQ_DEFHOME          "."
#define TITRAQ_PREFUSER         "user"
#define TITRAQ_DEFUSER          "username"
#define TITRAQ_PREFASDIR        "asdir"
#define TITRAQ_DEFASDIR         "~/.as/"
#define TITRAQ_PREFACCOUNTS     "accounts"
#define TITRAQ_DEFACCOUNTS      "~/.as/accounts"
#define TITRAQ_PREFSTYLE        "uistyle"
#define TITRAQ_PREFVIEW         "view"
#define TITRAQ_DEFVIEW          "normal"
#define TITRAQ_PREFREMOTELOG    "logremote"
#define TITRAQ_DEFREMOTELOG     "no"
#define TITRAQ_PREFLOCALLOG     "loglocal"
#define TITRAQ_DEFLOCALLOG      "no"
#define TITRAQ_PREFCORBHOST     "corbahost"
#define TITRAQ_DEFCORBHOST      "localhost:8914"
#define TITRAQ_PREFSOAPHOST     "soaphost"
#define TITRAQ_DEFSOAPHOST      "localhost/cgi-bin/asdbserv"
#define TITRAQ_PREFCORBON       "corbaenable"
#define TITRAQ_DEFCORBON        0
#define TITRAQ_PREFSOAPON       "soapenable"
#define TITRAQ_DEFSOAPON        0
#define TITRAQ_PREFBAKON        "autobackup"
#define TITRAQ_DEFBAKON         1
#define TITRAQ_PREFEXTENDON     "filextension"
#define TITRAQ_DEFEXTENDON      1
#define TITRAQ_PREFDETAILON     "detailisting"
#define TITRAQ_DEFDETAILON      0
#define TITRAQ_PREFSIGNATON     "signatureline"
#define TITRAQ_DEFSIGNATON      0
#define TITRAQ_PREFREPORTYPE    "reportperiod"
#define TITRAQ_DEFREPORTYPE     TITRAQ_REPORTWEEK
#define TITRAQ_PREFREPORTWEEKS  "reportweeks"
#define TITRAQ_DEFREPORTWEEKS   1
#define TITRAQ_PREFREPORTMONTHS "reportmonths"
#define TITRAQ_DEFREPORTMONTHS  1
#define TITRAQ_PREFLIGHTRED     "lightred"
#define TITRAQ_DEFLIGHTRED      248
#define TITRAQ_PREFLIGHTGREEN   "lightgreen"
#define TITRAQ_DEFLIGHTGREEN    248
#define TITRAQ_PREFLIGHTBLUE    "lightblue"
#define TITRAQ_DEFLIGHTBLUE     240
#define TITRAQ_PREFDARKRED      "darkred"
#define TITRAQ_DEFDARKRED       224
#define TITRAQ_PREFDARKGREEN    "darkgreen"
#define TITRAQ_DEFDARKGREEN     224
#define TITRAQ_PREFDARKBLUE     "darkblue"
#define TITRAQ_DEFDARKBLUE      216
#define TITRAQ_PREFLTALTRED     "lightred"
#define TITRAQ_DEFLTALTRED      240
#define TITRAQ_PREFLTALTGREEN   "lightgreen"
#define TITRAQ_DEFLTALTGREEN    240
#define TITRAQ_PREFLTALTBLUE    "lightblue"
#define TITRAQ_DEFLTALTBLUE     212
#define TITRAQ_PREFDKALTRED     "darkred"
#define TITRAQ_DEFDKALTRED      214
#define TITRAQ_PREFDKALTGREEN   "darkgreen"
#define TITRAQ_DEFDKALTGREEN    210
#define TITRAQ_PREFDKALTBLUE    "darkblue"
#define TITRAQ_DEFDKALTBLUE     160

// Column show preferences
#define TITRAQ_PREFSTATCOLON    "statcolshow"
#define TITRAQ_DEFSTATCOLON     true
#define TITRAQ_PREFLCOLON       "linecolshow"
#define TITRAQ_DEFLCOLON        false
#define TITRAQ_PREFUCOLON       "usercolshow"
#define TITRAQ_DEFUCOLON        false
#define TITRAQ_PREFGCOLON       "guidcolshow"
#define TITRAQ_DEFGCOLON        false
#define TITRAQ_PREFCCOLON       "crccolshow"
#define TITRAQ_DEFCCOLON        false
#define TITRAQ_PREFREVCOLON     "revcolshow"
#define TITRAQ_DEFREVCOLON      false
#define TITRAQ_PREFDCOLON       "datecolshow"
#define TITRAQ_DEFDCOLON        true
#define TITRAQ_PREFSTARTCOLON   "startcolshow"
#define TITRAQ_DEFSTARTCOLON    false
#define TITRAQ_PREFFCOLON       "finishcolshow"
#define TITRAQ_DEFFCOLON        false
#define TITRAQ_PREFACOLON       "amountcolshow"
#define TITRAQ_DEFACOLON        true
#define TITRAQ_PREFTCOLON       "taskcolshow"
#define TITRAQ_DEFTCOLON        true
#define TITRAQ_PREFREMCOLON     "remarkcolshow"
#define TITRAQ_DEFREMCOLON      true

// Frame geometry preferences
#define TITRAQ_PREFFRAMELAY     "framelayout"
#define TITRAQ_PREFFRAMEWIDTH   "framewidth"
#define TITRAQ_DEFFRAMEWIDTH    640
#define TITRAQ_PREFFRAMEHEIGHT  "frameheight"
#define TITRAQ_DEFFRAMEHEIGHT   400

// Column width preferences
#define TITRAQ_PREFSTATCOLWIDTH   "statcolwidth"
#define TITRAQ_DEFSTATCOLWIDTH    24
#define TITRAQ_PREFLCOLWIDTH      "linecolwidth"
#define TITRAQ_DEFLCOLWIDTH       32
#define TITRAQ_PREFUCOLWIDTH      "usercolwidth"
#define TITRAQ_DEFUCOLWIDTH       32
#define TITRAQ_PREFGCOLWIDTH      "guidcolwidth"
#define TITRAQ_DEFGCOLWIDTH       254
#define TITRAQ_PREFCCOLWIDTH      "crccolwidth"
#define TITRAQ_DEFCCOLWIDTH       84
#define TITRAQ_PREFREVCOLWIDTH    "revcolwidth"
#define TITRAQ_DEFREVCOLWIDTH     32
#define TITRAQ_PREFDCOLWIDTH      "datecolwidth"
#define TITRAQ_DEFDCOLWIDTH       98
#define TITRAQ_PREFSTARTCOLWIDTH  "startcolwidth"
#define TITRAQ_DEFSTARTCOLWIDTH   60
#define TITRAQ_PREFFCOLWIDTH      "finishcolwidth"
#define TITRAQ_DEFFCOLWIDTH       60
#define TITRAQ_PREFACOLWIDTH      "amountcolwidth"
#define TITRAQ_DEFACOLWIDTH       60
#define TITRAQ_PREFTCOLWIDTH      "taskcolwidth"
#define TITRAQ_DEFTCOLWIDTH       148
#define TITRAQ_PREFREMCOLWIDTH    "remarkcolwidth"
#define TITRAQ_DEFREMCOLWIDTH     120

// Column appearance preferences
#define TITRAQ_PREFFILEBAR      "filetoolbar"
#define TITRAQ_DEFFILEBAR       true
#define TITRAQ_PREFEDITBAR      "edittoolbar"
#define TITRAQ_DEFEDITBAR       true
#define TITRAQ_PREFVIEWBAR      "viewtoolbar"
#define TITRAQ_DEFVIEWBAR       true
#define TITRAQ_PREFPREFBAR      "preftoolbar"
#define TITRAQ_DEFPREFBAR       true
#define TITRAQ_PREFWHATBAR      "whattoolbar"
#define TITRAQ_DEFWHATBAR       true

// Other value preferences
#define TITRAQ_PREFSORTCOL      "sortordering"
#define TITRAQ_DEFSORTCOL       TITRAQ_IDXDATE
#define TITRAQ_PREFSORTDIR      "sortascending"
#define TITRAQ_DEFSORTDIR       true

// Other value constants
#define TITRAQ_DATAVERSIONMAJ   0
#define TITRAQ_DATAVERSIONMIN   6

// Environment string constants
#define TITRAQ_ENVUSERNAME      "USER"
#define TITRAQ_ENVHOMEDIR       "HOME"

// Falsified incoming ORB initilization arguments
#define TITRAQ_ORBINIT          "-ORBGIOPVersion 1.2 -ORBIIOPVersion 1.2 -ORBInitRef"
#define TITRAQ_COSSPART1        "NameService=corbaloc::"
#define TITRAQ_COSSPART2        "/NameService"
#define TITRAQ_SOAPSPACE        "http://soap.europalab.com/asdb"
#define TITRAQ_PREFIXHTTP       "http://"

// Style string constants
#define TITRAQ_STRCDE           "CDE"
#define TITRAQ_STRSGI           "SGI"
#define TITRAQ_STRMOTIF         "Motif"
#define TITRAQ_STRMPLUS         "MotifPlus"
#define TITRAQ_STRPLAT          "Platinum"
#define TITRAQ_STRMSOFT         "Windows"

// Other string constants
#define TITRAQ_SEPARATORTOK     " "
#define TITRAQ_HOMEDIRTOK       "~/"
#define TITRAQ_FEXTENSION       ".as"
#define TITRAQ_REFHELP          "as-gui.html"
#define TITRAQ_DATAPATTERN      "%!AS-EVENTS-"
#define TITRAQ_SAVEFIRST        "The timesheet contains unsaved changes.\nDo you want to save the changes or discard them?"
#define TITRAQ_OVERWRITE        "A file already exists with the chosen name.\nDo you want to overwrite it with new data?"
#define TITRAQ_NOPATTERNFOUND   "This data file appears to be invalid,\nbecause the AS data symbol\n        "
#define TITRAQ_WASNOTFOUNDIN    "\nwas not found inside of it."
#define TITRAQ_BADVERSIONMAJ    "Incompatible data format. Please\neither upgrade this application or\nthe data you are using with it."
#define TITRAQ_BADVERSIONMIN    "Incompatible data format. Please either\nupgrade this application or downgrade\nthe data you are using with it."
#define TITRAQ_INCOMPATDATA     "Error: incompatible data format."
#define TITRAQ_INVALIDDATA      "Error: invalid data format."
#define TITRAQ_SAVECANCELLED    "Warning: save operation failed, because the user cancelled."
#define TITRAQ_READPFILFAIL     "Could not open personal data file for reading."
#define TITRAQ_READAFILFAIL     "Could not open account file at %1 for reading."

// Indexes of table columns
#define TITRAQ_IDXALLCTRLS     -1
#define TITRAQ_IDXSTATUS        0
#define TITRAQ_IDXLINE          1
#define TITRAQ_IDXUSER          2
#define TITRAQ_IDXGUID          3
#define TITRAQ_IDXCRC           4
#define TITRAQ_IDXREV           5
#define TITRAQ_IDXDATE          6
#define TITRAQ_IDXSTART         7
#define TITRAQ_IDXFINISH        8
#define TITRAQ_IDXAMOUNT        9
#define TITRAQ_IDXTASK          10
#define TITRAQ_IDXREMARK        11
#define TITRAQ_IDXTAIL          12

// Indexes of col popup menu items
#define TITRAQ_IDXSTATCOL       1
#define TITRAQ_IDXLCOL          2
#define TITRAQ_IDXUCOL          3
#define TITRAQ_IDXGCOL          4
#define TITRAQ_IDXCCOL          5
#define TITRAQ_IDXREVCOL        6
#define TITRAQ_IDXDCOL          7
#define TITRAQ_IDXSTARTCOL      8
#define TITRAQ_IDXFCOL          9
#define TITRAQ_IDXACOL          10
#define TITRAQ_IDXTCOL          11
#define TITRAQ_IDXREMCOL        12

// Indexes of popup menu submenus
#define TITRAQ_IDXFILEBAR       1
#define TITRAQ_IDXEDITBAR       2
#define TITRAQ_IDXVIEWBAR       3
#define TITRAQ_IDXPREFBAR       4
#define TITRAQ_IDXWHATBAR       5

// Indexes of character tokens in strings
#define TITRAQ_IDXSTATERROR     0
#define TITRAQ_IDXSTATCOLOR     1

// Offsets
#define TITRAQ_OFFSETYEAR       4
#define TITRAQ_OFFSETMONTH      7
#define TITRAQ_OFFSETHOUR       2

// Report periods
#define TITRAQ_REPORTWEEK       0
#define TITRAQ_REPORTMONTH      1
#define TITRAQ_POPUPMSECS       200

// Values
#define TITRAQ_MARGIN           10          // Default layout margin
#define TITRAQ_SPACING          6           // Default layout spacing
#define TITRAQ_NUMBLOCKS        32          // Block size of a timesheet
#define TITRAQ_MAXAMOUNT        960         // Maximum valid amount
#define TITRAQ_MINAMOUNT        0           // Minimum valid amount
#define TITRAQ_STEPAMOUNT       15          // Line step interval
#define TITRAQ_MINSINHOUR       60          // Only idiots don't know this
#define TITRAQ_BRIGHT           'H'         // For coloring rows
#define TITRAQ_DARK             'D'         // For coloring rows
#define TITRAQ_BIGMAGIC         700000      // For unreasonably large numbers
#define TITRAQ_ACCTBOXHEIGHT    24          // Number rows in account combobox

#endif // TITCONST_H
