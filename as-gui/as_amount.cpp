//
//  OSSP asgui - Accounting system graphical user interface
//  Copyright (c) 2002-2004 The OSSP Project (http://www.ossp.org/)
//  Copyright (c) 2002-2004 Ralf S. Engelschall <rse@engelschall.com>
//  Copyright (c) 2002-2004 Michael Schloh von Bennewitz <michael@schloh.com>
//  Copyright (c) 2002-2004 Cable & Wireless Telecommunications Services GmbH
//
//  This file is part of OSSP asgui, an accounting system graphical user
//  interface which can be found at http://www.ossp.org/pkg/tool/asgui/.
//
//  Permission to use, copy, modify, and distribute this software for
//  any purpose with or without fee is hereby granted, provided that
//  the above copyright notice and this permission notice appear in all
//  copies.
//
//  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
//  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
//  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
//  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
//  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
//  SUCH DAMAGE.
//
//  as_amount.cpp: ISO C++ implementation
//

//#include <qregexp.h>

//#include "as_amount.h"
//#include "as_const.h"


//// Sets a text formatted representation
//void AmountBox::setText(const QString &Strval)
//{
//    int nTotal = 0;                                 // The total amount of minutes
//    QRegExp Strexpr("(\\d+):(\\d+)");               // Pattern in amount text
//    QString Stramount = QRegExp::escape(Strval);    // Incoming string escaped
//
//    if (Strval.isEmpty()) {                         // Shortcircuit in the
//        this->setValue(0);                          // case of empty string
//    }
//    else {                                          // Do the real work then
//        Strexpr.search(Stramount);
//        nTotal = Strexpr.cap(Strexpr.numCaptures() - 1).toInt() * TITRAQ_MINSINHOUR;
//        nTotal += Strexpr.cap(Strexpr.numCaptures()).toInt();
//        this->setValue(nTotal);
//    }
//}

//// Return a text formatted representation
//QString AmountBox::text(void) const
//{
//    QString Strfirst, Strsecond;
//    int nHours = 0;
//    int nMins = this->value();
//
//    nHours    = nMins / TITRAQ_MINSINHOUR;      // Calculate total hours
//    nMins     = nMins % TITRAQ_MINSINHOUR;      // Calculate total minutes
//    Strfirst  = trUtf8("%1:").arg(nHours);      // Format the first part
//    Strsecond = trUtf8("%1").arg(nMins);        // Format the second part
//
//    // Pad the resulting concatination before sending it out the back
//    return Strfirst.rightJustify(3, '0') + Strsecond.rightJustify(2, '0');
//}
