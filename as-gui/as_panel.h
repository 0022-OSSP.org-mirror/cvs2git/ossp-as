//
//  OSSP asgui - Accounting system graphical user interface
//  Copyright (c) 2002-2004 The OSSP Project (http://www.ossp.org/)
//  Copyright (c) 2002-2004 Ralf S. Engelschall <rse@engelschall.com>
//  Copyright (c) 2002-2004 Michael Schloh von Bennewitz <michael@schloh.com>
//  Copyright (c) 2002-2004 Cable & Wireless Telecommunications Services GmbH
//
//  This file is part of OSSP asgui, an accounting system graphical user
//  interface which can be found at http://www.ossp.org/pkg/tool/asgui/.
//
//  Permission to use, copy, modify, and distribute this software for
//  any purpose with or without fee is hereby granted, provided that
//  the above copyright notice and this permission notice appear in all
//  copies.
//
//  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
//  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
//  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
//  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
//  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
//  SUCH DAMAGE.
//
//  as_panel.h: ISO C++ interface
//

#ifndef PREFPANEL_H
#define PREFPANEL_H

#include <qvariant.h>
#include <qdialog.h>
#include <qbuttongroup.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qtoolbutton.h>
#include <qcolordialog.h>
#include <qradiobutton.h>
#include <qcheckbox.h>
#include <qtabwidget.h>
#include <qwidget.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qstylefactory.h>
#include <qlayout.h>

#include "as_gui.h"     // For usage of style_t


class Prefpanel : public QDialog
{
    Q_OBJECT

public:
    Prefpanel(QWidget *pParent = 0, const char *kszName = 0, bool bModal = true, WFlags Flags = 0);
//    ~Prefpanel(void); // No need to destroy widgets, because qt does it for us

private:
    Preferences *m_pPanprefs;
    QColor *m_pLight, *m_pDark;
    QColorGroup *m_pOrigcolours, *m_pAltcolours;

public:
    QGroupBox *m_pGenbox, *m_pRembox, *m_pShadebox, *m_pThemebox;
    QVBoxLayout *m_pStylelay, *m_pReportlay, *m_pSwitchlay;
    QVBoxLayout *m_pVlayout, *m_pBinlayout;
    QHBoxLayout *m_pBoolayout, *m_pGenlayout, *m_pRemlayout;
    QVBoxLayout *m_pShadeboxlay, *m_pThemeboxlay;
    QHBoxLayout *m_pGenboxlay, *m_pRemboxlay, *m_pColourlayout;
    QGridLayout *m_pGeninner, *m_pReminner, *m_pShadelayout;

    QWidget *m_pBoolpage, *m_pGeneralpage, *m_pRemotepage, *m_pColourpage;

    QHBoxLayout *m_pButtlay;
    QPushButton *m_pOkaybutton;
    QPushButton *m_pApplybutton;
    QPushButton *m_pCancelbutton;
    QTabWidget *m_pTabselect;

    QLabel *m_pAcctlabel, *m_pEventlabel;
    QLabel *m_pUserlabel, *m_pHomelabel;
    QLabel *m_pCorbalabel, *m_pSoaplabel;
    QLineEdit *m_pAcctline, *m_pEventline;
    QLineEdit *m_pUserline, *m_pHomeline;
    QLineEdit *m_pCorbaline, *m_pSoapline;
    QCheckBox *m_pCorbacheck, *m_pSoapcheck;

    QLabel *m_pLightlab, *m_pDarklab, *m_pRevertlab;
    QToolButton *m_pLightbutt, *m_pDarkbutt, *m_pRevertbutt;

    QButtonGroup *m_pStylegroup, *m_pReportgroup, *m_pSwitchgroup;
    QCheckBox *m_pBackupcheck, *m_pExtendcheck;
    QCheckBox *m_pDetailcheck, *m_pSigncheck;

public:
    // Accessor get methods
    QString getStyle(void)     {return (m_pStylegroup->selected()->text());};
    QString getAccounts(void)  {return (m_pAcctline->text());};
    QString getEvents(void)    {return (m_pEventline->text());};
    QString getUser(void)      {return (m_pUserline->text());};
    QString getHome(void)      {return (m_pHomeline->text());};
    QString getCorbahost(void) {return (m_pCorbaline->text());};
    QString getSoaphost(void)  {return (m_pSoapline->text());};
    bool getCorbaon(void)      {return (m_pCorbacheck->isChecked());};
    bool getSoapon(void)       {return (m_pSoapcheck->isChecked());};
    bool getBackon(void)       {return (m_pBackupcheck->isChecked());};
    bool getExtendon(void)     {return (m_pExtendcheck->isChecked());};
    bool getDetailon(void)     {return (m_pDetailcheck->isChecked());};
    bool getSignaton(void)     {return (m_pSigncheck->isChecked());};
    QColor *getLight(void)     {return m_pLight;};
    QColor *getDark(void)      {return m_pDark;};
    QColor *getRevlight(void)  {return (QColor *)&m_pOrigcolours->foreground();};
    QColor *getRevdark(void)   {return (QColor *)&m_pOrigcolours->background();};
    QColor *getAltlight(void)  {return (QColor *)&m_pAltcolours->foreground();};
    QColor *getAltdark(void)   {return (QColor *)&m_pAltcolours->background();};

    // Accessor set methods
    void setAccounts(const QString &kInstr)  {m_pAcctline->setText(kInstr);};
    void setEvents(const QString &kInstr)    {m_pEventline->setText(kInstr);};
    void setUser(const QString &kInstr)      {m_pUserline->setText(kInstr);};
    void setHome(const QString &kInstr)      {m_pHomeline->setText(kInstr);};
    void setCorbahost(const QString &kInstr) {m_pCorbaline->setText(kInstr);};
    void setSoaphost(const QString &kInstr)  {m_pSoapline->setText(kInstr);};
    void setBackon(const bool &kbEnable = true)   {m_pBackupcheck->setChecked(kbEnable);};
    void setExtendon(const bool &kbEnable = true) {m_pExtendcheck->setChecked(kbEnable);};
    void setDetailon(const bool &kbEnable = true) {m_pDetailcheck->setChecked(kbEnable);};
    void setSignaton(const bool &kbEnable = true) {m_pSigncheck->setChecked(kbEnable);};
    void lockCorba(const bool &kbLock = true)     {m_pCorbacheck->setEnabled(!kbLock);};
    void lockSoap(const bool &kbLock = true)      {m_pSoapcheck->setEnabled(!kbLock);};
    void setLight(const QColor *pLight)           {m_pLight = (QColor *)pLight;};
    void setDark(const QColor *pDark)             {m_pDark = (QColor *)pDark;};
    void setOrigcolour(QColorGroup *pGroup)       {m_pOrigcolours = pGroup;};
    void setAltcolour(QColorGroup *pGroup)        {m_pAltcolours  = pGroup;};
    void setCorbaon(const bool &kbEnable = true)  {
        m_pCorbaline->setEnabled(kbEnable);
        m_pCorbacheck->setChecked(kbEnable);
    };
    void setSoapon(const bool &kbEnable = true) {
        m_pSoapline->setEnabled(kbEnable);
        m_pSoapcheck->setChecked(kbEnable);
    };
    void setStyle(const char *pkszStyle) {
        for (int nIter = 0; nIter < m_pStylegroup->count(); nIter++)
            if (m_pStylegroup->find(nIter)->text() == pkszStyle)
                m_pStylegroup->setButton(nIter);
    };

protected slots:
    virtual void polish(void);
    virtual void textChange(void);
    virtual void shadeLight(void);
    virtual void shadeDark(void);
    virtual void shadeRevert(void);
    void enableCorba(bool bOn) {m_pCorbaline->setEnabled(bOn);};
    void enableSoap(bool bOn) {m_pSoapline->setEnabled(bOn);};

signals:
    void applied(void);
};

#endif // PREFPANEL_H
