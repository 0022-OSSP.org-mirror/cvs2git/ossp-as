/*
**  as_version.cpp -- Version Information for OSSP as-gui (syntax: C/C++)
**  [automatically generated and maintained by GNU shtool]
*/

#ifdef _AS_VERSION_CPP_AS_HEADER_

#ifndef _AS_VERSION_CPP_
#define _AS_VERSION_CPP_

#define ASGUI_VERSION 0x007207

typedef struct {
    const int   v_hex;
    const char *v_short;
    const char *v_long;
    const char *v_tex;
    const char *v_gnu;
    const char *v_web;
    const char *v_sccs;
    const char *v_rcs;
} asgui_version_t;

extern asgui_version_t asgui_version;

#endif /* _AS_VERSION_CPP_ */

#else /* _AS_VERSION_CPP_AS_HEADER_ */

#define _AS_VERSION_CPP_AS_HEADER_
#include "as_version.cpp"
#undef  _AS_VERSION_CPP_AS_HEADER_

asgui_version_t asgui_version = {
    0x007207,
    "0.7.7",
    "0.7.7 (24-Aug-2004)",
    "This is OSSP as-gui, Version 0.7.7 (24-Aug-2004)",
    "OSSP as-gui 0.7.7 (24-Aug-2004)",
    "OSSP as-gui/0.7.7",
    "@(#)OSSP as-gui 0.7.7 (24-Aug-2004)",
    "$Id: as_version.cpp,v 1.45 2004/08/24 22:00:30 ms Exp $"
};

#endif /* _AS_VERSION_CPP_AS_HEADER_ */

