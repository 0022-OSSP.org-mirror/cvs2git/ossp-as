//
//  OSSP asgui - Accounting system graphical user interface
//  Copyright (c) 2002-2004 The OSSP Project (http://www.ossp.org/)
//  Copyright (c) 2002-2004 Ralf S. Engelschall <rse@engelschall.com>
//  Copyright (c) 2002-2004 Michael Schloh von Bennewitz <michael@schloh.com>
//  Copyright (c) 2002-2004 Cable & Wireless Telecommunications Services GmbH
//
//  This file is part of OSSP asgui, an accounting system graphical user
//  interface which can be found at http://www.ossp.org/pkg/tool/asgui/.
//
//  Permission to use, copy, modify, and distribute this software for
//  any purpose with or without fee is hereby granted, provided that
//  the above copyright notice and this permission notice appear in all
//  copies.
//
//  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
//  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
//  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
//  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
//  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
//  SUCH DAMAGE.
//
//  as_gui.h: ISO C++ interface
//

#ifndef AS_GUI_MWIN_H
#define AS_GUI_MWIN_H

#include <qmainwindow.h>

#ifdef HAVE_CONFIG_H
#include "ac_config.h"
#endif

#include "as_except.h"  // Exception classes

// Version information
#define _AS_VERSION_CPP_AS_HEADER_
#include "as_version.cpp"
#undef  _AS_VERSION_CPP_AS_HEADER_

// Trolltech class prototypes
class QAction;
class QHeader;
class QTimeEdit;
class QComboBox;
class QLineEdit;
class QLabel;
class QDate;
class QFile;

// Local class prototypes
class ASTimeEdit;
class Daydatedit; // Might be replaced with ASTimeEdit throughout
class TiTable;
class Preferences;
class Prefpanel;

// Intentional no operation
#define TITRAQ_NOP ((void)0)

#ifdef HAVE_CONFIG_H
#if (SIZEOF_CHAR == 1)
typedef unsigned char        U8;
#else
?==Error: Undefined 8 bit type
#endif // U8

#if (SIZEOF_SHORT == 2)
typedef unsigned short       U16;
#elif (SIZEOF_INT == 2)
typedef unsigned int         U16;
#elif (SIZEOF_LONG == 2)
typedef unsigned long        U16;
#else
?==Error: Undefined 16 bit type
#endif // U16

#if (SIZEOF_SHORT == 4)
typedef unsigned short       U32;
#elif (SIZEOF_INT == 4)
typedef unsigned int         U32;
#elif (SIZEOF_LONG == 4)
typedef unsigned long        U32;
#else
?==Error: Undefined 32 bit type
#endif // U32

#else // HAVE_CONFIG_H (not defined)
typedef unsigned char        U8;

#if (2 == 2)
typedef	unsigned short       U16;
#elif (4 == 2)
typedef	unsigned int         U16;
#else
?==Error: Undefined 16 bit type
#endif

#if (2 == 4)
typedef	unsigned short       U32;
#elif (4 == 4)
typedef	unsigned int         U32;
#elif (4 == 4)
typedef	unsigned long        U32;
#else
?== Error: Undefined 32 bit type
#endif
#endif // HAVE_CONFIG_H


// Main table size
const int g_knBlocks = 32;

// Styles enumeration
typedef enum
{
    TITRAQ_STYLECDE,
    TITRAQ_STYLESGI,
    TITRAQ_STYLEMOTIF,
    TITRAQ_STYLEMPLUS,
    TITRAQ_STYLEPLAT,
    TITRAQ_STYLEMSOFT
} styles_t;

// Main application form window
class Titraqform : public QMainWindow
{
    Q_OBJECT    // Generate MOC object model stubs

public:
    Titraqform(QWidget *pParent = 0, const char *kszName = 0, WFlags Flags = 0);
    ~Titraqform(void);

    // Accessor methods
    const QString getRowdata(void) const;                   // Get a whole row of data
    void setRowdata(QString &) const;                       // Set a whole row of data
    const int getFirstcol(void) const;                      // Learn which col is first
    const bool isOpen(void) {return (m_szFilename == NULL) ? false : true;}; // Doc state
    void setOpen(bool bDocopen = true)                      // Set state of document
    {
        if (bDocopen) {
            if (m_szFilename == NULL)
                m_szFilename = new QString();
        }
        else
            if (m_szFilename != NULL) {
                delete m_szFilename;
                m_szFilename = NULL;
            }
    };
    const QString *getFilename(void) // Get the current file name
    {
        if (this->isOpen())
            return m_szFilename;
        else
            throw Genexcept("File name does not exist.");
    };
    void setFilename(const QString &Fname) // Set file name
    {
        if (this->isOpen()) // If a file name exists
            setOpen(false); // then get rid of it first
        m_szFilename = new QString(Fname);
    };

    // Utility members
    void loadInit(void);    // Parse cmds and open initial file

    // Top level members
    Preferences *m_pPrefs;

    // Main application actions
    QAction     *m_pFilenewact,   *m_pFileopenact;
    QAction     *m_pFilesaveact,  *m_pFilesaveasact;
    QAction     *m_pFilecloseact, *m_pFilequitact;
    QAction     *m_pAddrowact,    *m_pDelrowact;
    QAction     *m_pRefreshact,   *m_pCutact;
    QAction     *m_pCopyact,      *m_pPasteact;
    QAction     *m_pSyncact,      *m_pPrefsact;
    QAction     *m_pReportact;

    // Table, cells, and entries in package layout
    TiTable     *m_pMaintable;
    QHeader     *m_pTablehead;
    QStringList *m_pTaskentries;

    // Edition widgets
    QLabel      *m_pStatusedit;         // Control to show entry status
    QLineEdit   *m_pLineedit;           // Control to line number
    QLineEdit   *m_pUseredit;           // Control to edit user name
    QLineEdit   *m_pGuidedit;           // Control to edit GUID
    QLineEdit   *m_pCrcedit;            // Control to edit CRC
    QLineEdit   *m_pRevedit;            // Control to edit revision
    Daydatedit  *m_pDateedit;           // Control to edit date
    QTimeEdit   *m_pStarttime;          // Control to edit start time
    QTimeEdit   *m_pEndtime;            // Control to edit finish time
//    QTimeEdit   *m_pAmount;             // Control to edit total time
    ASTimeEdit  *m_pAmount;             // Control to edit total time
    QComboBox   *m_pTasks;              // Control to choose a task
    QLineEdit   *m_pRemark;             // Control to edit remark

protected slots:
    void cutEntry(void);                // Cut task entries from the list
    void copyEntry(void);               // Copy task entries from the list
    void pasteEntry(void);              // Paste task entries to the list
    void addEntry(int nRows= -1);       // Add task entries to the list
    void delEntry(int nRows= -1);       // Delete task entries from the list
    void refreshDisplay(void);          // Refresh the display of all data items
    void syncIiop(void);                // Syncronize data with server using IIOP
    void syncSoap(void);                // Syncronize data with server using SOAP
    void newDoc(void);                  // Make and display a new document window
    void openDoc(void);                 // Open and display an existing document
    void quitApp(void);                 // Close the current document and quit
    void saveFile(void);                // Serialize to the current file
    void saveAs(void);                  // Serialize to a selected file
    void saveName(void);                // Implicitly serialize to a selected file
    void helpContents(void);            // Use the help contents
    void aboutTitraq(void);             // Learn more about this program itself
    void aboutOSSP(void);               // Learn more about the OSSP
    void aboutQt(void);                 // Learn more about Qt
    void selAll(void);                  // Edit menu select all
    void onClick(int, int, int, const QPoint &); // Table was clicked
    void inplaceEdit(int, int, int, const QPoint &); // Enter in place edit mode
    void updEdit(int, int nCol = 0);    // Update edit controls
    void validateRow(void);             // Validate current row of matrix
    void validateRow(int, int);         // Validate specified row of matrix
    void updSizes(int, int, int);       // Update edit sizes
    void calcCrc(void);                 // Calculate CRC of current row
    void calcCrc(int, int);             // Calculate CRC of specified row
    void dataChanged(int, int);         // Timesheet data changed
    void updateLine(const QString &);   // Update line number column
    void updateUser(const QString &);   // Update user column
    void updateGuid(const QString &);   // Update GUID column
    void updateCrc(const QString &);    // Update CRC column
    void updateRev(const QString &);    // Update rev column
    void updateDate(const QDate &);     // Update date column
    void updateStart(const QTime &);    // Update start column
    void updateFinish(const QTime &);   // Update finish column
    void updateAmount(const QTime &);   // Update amount column
    void updateTask(const QString &);   // Update task column
    void updateRemark(const QString &); // Update remark column
    void confirmEdit(void);             // Confirm whole row edition
    void configPrefs(void);             // Edit menu configure preferences
    void normalView(void);              // View menu normal
    void editingView(void);             // View menu editing
    void timingView(void);              // View menu timing
    void showFilebar(void);             // View menu show file toolbar
    void showEditbar(void);             // View menu show edit toolbar
    void showViewbar(void);             // View menu show view toolbar
    void showPrefsbar(void);            // View menu show prefs toolbar
    void showWhatsbar(void);            // View menu show whats this toolbar
    void showStatcol(void);             // View menu show status column
    void showLinecol(void);             // View menu show line numbers column
    void showUsercol(void);             // View menu show Users column
    void showGuidcol(void);             // View menu show GUIDs column
    void showCrccol(void);              // View menu show CRC column
    void showRevcol(void);              // View menu show Rev column
    void showDatecol(void);             // View menu show Dates column
    void showStartcol(void);            // View menu show Start time column
    void showFinishcol(void);           // View menu show Finish time column
    void showAmountcol(void);           // View menu show Amounts column
    void showTaskcol(void);             // View menu show Tasks column
    void showRemarkcol(void);           // View menu show Remarks column
    void genReport(void);               // Generate a local formatted report
    void savePrefs(void);               // Save user preferences
    void applyPrefs(void);              // Apply preferences from applied()
    void applyPrefs(Prefpanel *);       // Apply preferences from accept()

protected:
    // Application main events
    void closeEvent(QCloseEvent *);     // Close current document in main window

    // Application main window widgets
    QFrame      *m_pCenframe;           // Central frame
    QVBoxLayout *m_pMainlayout;         // Package and control layouts
    QVBoxLayout *m_pPackagelayout;      // Main table control
    QHBoxLayout *m_pEditlayout;         // Lower edition widgets

private:
    // Owned by QMainWindow
    QMenuBar    *m_pMenubar;
    QPopupMenu  *m_pViewpopup;          // Saved for manipulating views
    QPopupMenu  *m_pTbarspopup;         // Saved for manipulating toolbars
    QPopupMenu  *m_pColspopup;          // Saved for manipulating columns
    QStatusBar  *m_pStatbar;

    // Belong to us
    QToolBar    *m_pFiletools;
    QToolBar    *m_pEdittools;
    QToolBar    *m_pViewtools;
    QToolBar    *m_pPrefstools;
    QToolBar    *m_pWhatstools;
    QString     *m_szFilename;

    // Constructor helpers
    void setupPrefs(void);              // Preferences
    void setupActions(void);            // Actions
    void setupMenubar(void);            // Menu bar
    void setupToolbars(void);           // Tool bars
    void setupStatusbar(void);          // Status bar
    void setupCentralwidget(void);      // Assemble main Frame widget
    void setupTable(void);              // Table widget
    void setupEditlay(void);            // Editing lay
    void setupPieces(void);             // Assemble widget pieces
    void setupColumns(void);            // Arrange and configure columns
    void enableIface(bool);             // [En/dis]able most ui pieces

    // Data processing
    void loadAccounts(QFile &);         // Load accounts from file
    void loadAccounts(QTextStream &);   // Load accounts from stream
    void loadData(QFile &);             // Load personal data from file
    void loadData(QTextStream &);       // Load personal data from stream
    void saveData(QFile &);             // Save accounting data to file
    void saveData(QTextStream &);       // Save accounting data to stream
    const bool validateData(QFile &) const;   // Validate personal data in file
    const bool validateData(QString &) const; // Validate personal data from string
};

#endif // AS_GUI_MWIN_H
