//
//  OSSP asgui - Accounting system graphical user interface
//  Copyright (c) 2002-2004 The OSSP Project (http://www.ossp.org/)
//  Copyright (c) 2002-2004 Ralf S. Engelschall <rse@engelschall.com>
//  Copyright (c) 2002-2004 Michael Schloh von Bennewitz <michael@schloh.com>
//  Copyright (c) 2002-2004 Cable & Wireless Telecommunications Services GmbH
//
//  This file is part of OSSP asgui, an accounting system graphical user
//  interface which can be found at http://www.ossp.org/pkg/tool/asgui/.
//
//  Permission to use, copy, modify, and distribute this software for
//  any purpose with or without fee is hereby granted, provided that
//  the above copyright notice and this permission notice appear in all
//  copies.
//
//  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
//  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
//  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
//  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
//  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
//  SUCH DAMAGE.
//
//  as_generic.cpp: ISO C++ implementation
//

#include <qobject.h>

#include "as_generic.h"


// Construct a Prototype object
Prototype::Prototype()
{
    // For display of a prototype implementation
    QString Namever = QString("AS Accounting System");
    m_pProtomsg = new QMessageBox(Namever,
        QObject::trUtf8("This method is not implemented yet."),
        QMessageBox::NoIcon, QMessageBox::Ok | QMessageBox::Default,
        QMessageBox::Cancel | QMessageBox::Escape, QMessageBox::NoButton,
        NULL, "Prototypemsg", true, Qt::WStyle_NormalBorder);
}

// Destroy a Prototype object
Prototype::~Prototype()
{
    // Destroy our QMessageBox
    delete m_pProtomsg;
}

// Display a message box, acting as an implementation prototype
void Prototype::doMbox()
{
    int nRet; // Return value

    // Launch our QMessageBox
    nRet = m_pProtomsg->exec();

    // Handle message box modality
    switch (nRet) {
    case QMessageBox::Ok:
        break;
    case QMessageBox::Cancel:
    default: // Just for sanity
        break;
    }
}
