//
//  OSSP asgui - Accounting system graphical user interface
//  Copyright (c) 2002-2004 The OSSP Project (http://www.ossp.org/)
//  Copyright (c) 2002-2004 Ralf S. Engelschall <rse@engelschall.com>
//  Copyright (c) 2002-2004 Michael Schloh von Bennewitz <michael@schloh.com>
//  Copyright (c) 2002-2004 Cable & Wireless Telecommunications Services GmbH
//
//  This file is part of OSSP asgui, an accounting system graphical user
//  interface which can be found at http://www.ossp.org/pkg/tool/asgui/.
//
//  Permission to use, copy, modify, and distribute this software for
//  any purpose with or without fee is hereby granted, provided that
//  the above copyright notice and this permission notice appear in all
//  copies.
//
//  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
//  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
//  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
//  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
//  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
//  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
//  SUCH DAMAGE.
//
//  as_table.h: ISO C++ interface
//

#ifndef TITABLE_H
#define TITABLE_H

#include <qtable.h>

#include "as_pref.h"


class TiTable : public QTable
{
    Q_OBJECT

private:
    int  m_nSortcol;    // To track current sort column
    int  m_bSortdir;    // To track current sort direction
    bool m_bDirt;       // To track dirty and clean states
    Preferences *m_pTiprefs; // To read current color values

public:
    // Try to match QTable's default constructor with an initializer list
    TiTable(Preferences *pPrefs, QWidget *pParent = 0, const char *szName = 0) : QTable(pParent, szName)
    {
        this->setSortcol(0);
        this->setSortdir(true);
        this->setDirty(false);
        this->setEdition(); // Reset edition state
        m_pTiprefs = pPrefs;
        horizontalHeader()->installEventFilter(this);
    };

    bool eventFilter(QObject *, QEvent *);

    // Standard members
    int m_nEdit;    // To track edition state

    // Accessor methods
    const bool isDirty(void) {return m_bDirt;};             // Check for changed state danger
    void setDirty(bool bDirty = true) {m_bDirt = bDirty;};  // Clean or dirty
    const int getEdition(void) {return m_nEdit;};           // Which edited column was confirmed
    void setEdition(const int nEdit = -1) {m_nEdit = nEdit;};   // Set edition status
    const int getSortcol(void) {return m_nSortcol;};
    void setSortcol(const int nColin) {m_nSortcol = nColin;};
    const bool getSortdir(void) {return m_bSortdir;};
    void setSortdir(const bool bDirection) {m_bSortdir = bDirection;};

    // Overridden accessors
    void setText(int, int, const QString &);
    void sortColumn(int nCol, bool bAscend = true, bool bWhole = true);
//    virtual QTableItem *item(int nRow, int nCol) const {return QTable::item(nRow, nCol);};

    // Deny a cell special handling of the focus rectangle
    // by overriding class QTable's paintFocus method
    virtual void paintFocus(QPainter *, const QRect &) {};

    // Override for special linewise shading according to sort key
    virtual void paintCell(QPainter *, int, int, const QRect &, bool, const QColorGroup &);

    // Override to properly handle read only attribute during edition
    virtual void endEdit(int, int, bool, bool);

    // For special focus handling on return key in edit mode
    virtual void activateNextCell(void);

signals:
    void textEdited(int, int);  // A cell was edited and data was modified
};

#endif // TITABLE_H
