--
--  AS -- Accounting System
--  Copyright (c) 2002-2003 Cable & Wireless Deutschland <http://www.cw.com/de/>
--  Copyright (c) 2002-2003 Ralf S. Engelschall <rse@engelschall.com>
--
--  This file is part of AS, an accounting system which can be
--  found at http://as.is.eu.cw.com/
--
--  This program is free software; you can redistribute it and/or
--  modify it under the terms of the GNU General Public License
--  as published by the Free Software Foundation; either version
--  2.0 of the License, or (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
--  General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program; if not, write to the Free Software
--  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
--  USA, or contact The OSSP Project <ossp@ossp.org>.
--
--  as_db.sql -- Accounting System Database Definition
--

--
--  Notice: Although this is mostly SQL/92, it is intended
--          for use with the PostgreSQL RDBMS only.
--
--  Notice: SQL comments below are converted into SQL 
--          "COMMENT ON" statements on-the-fly, so make sure 
--          that they are properly aligned.
--

--  System Global Configuration
CREATE TABLE as_config (
    cf_var         TEXT
                   NOT NULL
                   CHECK(cf_var ~ '^(version|created)$'),
                   -- configuration variable name
                   -- [version] (rw)
    cf_val         TEXT
                   NOT NULL
                   -- configuration variable value
                   -- [0.9.0] (rw)
);

--  System Global Object Identifiers
CREATE TABLE as_oid (
    id_oid         BIGSERIAL
                   UNIQUE NOT NULL
                   PRIMARY KEY,
                   -- unique identifier of object
                   -- [42] (ro)
    id_type        VARCHAR(20)
                   NOT NULL
                   CHECK(id_type ~ '^(locality|holiday|user|group|account)$'),
                   -- type of object (means: references as_<type>.<prefix>_oid)
                   -- [user] (rw)
    id_freeze      TIMESTAMP (0) WITH TIME ZONE
                   NOT NULL
                   DEFAULT '-infinity'
                   -- object is frozen before (exclusive) this time
                   -- [2002-01-01 00:00:00] (rw)
);

--  System Global Epoch Sequence
CREATE TABLE as_epoch (
    ep_epoch       BIGSERIAL
                   UNIQUE NOT NULL
                   PRIMARY KEY,
                   -- unique system epoch number
                   -- [42] (ro)
    ep_begin       TIMESTAMP (4) WITH TIME ZONE
                   NOT NULL
                   -- begin of epoch in world time
                   -- [2002-01-01 00:00:00.0000] (rw)
);

--  System Localities
CREATE TABLE as_locality (
    lo_oid         BIGINT
                   UNIQUE NOT NULL
                   REFERENCES as_oid (id_oid)
                   MATCH FULL DEFERRABLE
                   PRIMARY KEY,
                   -- unique identifier of locality
                   -- [1] (ro)
    lo_name        VARCHAR(80)
                   NOT NULL,
                   -- name of locality
                   -- [Germany/Bavaria] (re)
    lo_timezone    INTEGER
                   NOT NULL
                   CHECK(lo_timezone >= -12 AND lo_timezone <= +12),
                   -- timezone offset of locality
                   -- [2] (re)
    lo_valid_beg   TIMESTAMP (0) WITH TIME ZONE
                   NOT NULL
                   DEFAULT 'now',
                   -- begin real time of record validity range (inclusive)
                   -- [2002-01-01 00:00:00] (re)
    lo_valid_end   TIMESTAMP (0) WITH TIME ZONE
                   NOT NULL
                   DEFAULT 'infinity',
                   -- end real time of record validity range (inclusive)
                   -- [2002-01-01 00:00:00] (re)
    lo_epoch_beg   BIGINT
                   NOT NULL
                   REFERENCES as_epoch (ep_epoch)
                   MATCH FULL DEFERRABLE
                   -- begin system epoch of record creation (inclusive)
                   -- [2] (ro)
);

--  System Locality Holidays
CREATE TABLE as_holiday (
    hd_oid         BIGINT
                   UNIQUE NOT NULL
                   REFERENCES as_oid (id_oid)
                   MATCH FULL DEFERRABLE
                   PRIMARY KEY,
                   -- unique identifier of holiday
                   -- [1] (ro)
    hd_date        DATE
                   NOT NULL,
                   -- date of locality holiday
                   -- [2002-10-02] (re)
    hd_valid_beg   TIMESTAMP (0) WITH TIME ZONE
                   NOT NULL
                   DEFAULT 'now',
                   -- begin real time of record validity range (inclusive)
                   -- [2002-01-01 00:00:00] (re)
    hd_valid_end   TIMESTAMP (0) WITH TIME ZONE
                   NOT NULL
                   DEFAULT 'infinity',
                   -- end real time of record validity range (inclusive)
                   -- [2002-01-01 00:00:00] (re)
    hd_epoch_beg   BIGINT
                   NOT NULL
                   REFERENCES as_epoch (ep_epoch)
                   MATCH FULL DEFERRABLE
                   -- begin system epoch of record creation (inclusive)
                   -- [2] (ro)
);

--  System Users
CREATE TABLE as_user (
    us_oid         BIGINT
                   UNIQUE NOT NULL
                   REFERENCES as_oid (id_oid)
                   MATCH FULL DEFERRABLE
                   PRIMARY KEY,
                   -- globally unique object identifier
                   -- [42] (ro)
    us_name        VARCHAR(30)
                   NOT NULL
                   CHECK(us_name ~ '^[a-zA-Z][a-zA-Z0-9_-]+$'),
                   -- system name
                   -- [rse] (re)
    us_realname    VARCHAR(60)
                   NOT NULL,
                   -- real name
                   -- [Ralf S. Engelschall] (re)
    us_password    VARCHAR(30)
                   NOT NULL
                   CHECK(us_password ~ '^$1$.+'),
                   -- system password (MD5-based crypt format)
                   -- [$1$xxx...] (re)
    us_email       VARCHAR(60)
                   NOT NULL
                   CHECK(us_email ~ '^.+@[^.]+(\.[^.]+)+$'),
                   -- email address
                   -- [rse@de.cw.com] (re)
    us_home        VARCHAR(128),
                   CHECK(us_home ~ '^|(https?://.+)$'),
                   -- homepage URL 
                   -- [http://dev.de.cw.net/~rse/] (re)
    us_workhours   INTEGER
                   NOT NULL
                   CHECK(us_workhours > 0 AND us_workhours <= 16),             
                   -- contract-based daily working hours
                   -- [8] (re)
    us_workdays    INTEGER
                   NOT NULL
                   CHECK(us_workdays > 0 AND us_workdays <= 7),             
                   -- contract-based weekly working days 
                   -- [5] (re)
    us_notify      BOOLEAN
                   NOT NULL
                   DEFAULT 'true', 
                   -- whether user is notified about outstanding accounting
                   -- [true] (re)
    us_notify_min  INTEGER
                   NOT NULL
                   DEFAULT '40'
                   CHECK(us_notify_min >= 0 AND us_notify_min <= 100),             
                   -- percent of covered minimum workhours for notification
                   -- [40] (re)
    us_notify_after INTEGER
                   NOT NULL
                   DEFAULT '24'
                   CHECK(us_notify_after >= 0),
                   -- hours after end of day for which user is notified
                   -- [12] (re)
    us_enabled     BOOLEAN
                   NOT NULL
                   DEFAULT 'true',
                   -- whether user is enabled or not
                   -- [true] (re)
    us_valid_beg   TIMESTAMP (0) WITH TIME ZONE
                   NOT NULL
                   DEFAULT 'now',
                   -- begin real time of record validity range (inclusive)
                   -- [2002-01-01 00:00:00] (re)
    us_valid_end   TIMESTAMP (0) WITH TIME ZONE
                   NOT NULL
                   DEFAULT 'infinity',
                   -- end real time of record validity range (inclusive)
                   -- [2002-01-01 00:00:00] (re)
    us_epoch_beg   BIGINT
                   NOT NULL
                   REFERENCES as_epoch (ep_epoch)
                   MATCH FULL DEFERRABLE
                   -- begin system epoch of record creation (inclusive)
                   -- [2] (ro)
);

--  System Groups
CREATE TABLE as_group (
    gr_oid         BIGINT
                   UNIQUE NOT NULL
                   REFERENCES as_oid (id_oid)
                   MATCH FULL DEFERRABLE
                   PRIMARY KEY,
                   -- globally unique object identifier
                   -- [66] (ro)
    gr_name        VARCHAR(30),
                   -- system name of group 
                   -- [dev] (re)
    gr_realname    VARCHAR(60),
                   -- real name of group
                   -- [Development Team] (re)
    gr_enabled     BOOLEAN
                   NOT NULL
                   DEFAULT 'true',
                   -- whether group is enabled or not
                   -- [true] (re)
    gr_valid_beg   TIMESTAMP (0) WITH TIME ZONE
                   NOT NULL
                   DEFAULT 'now',
                   -- begin real time of record validity range (inclusive)
                   -- [2002-01-01 00:00:00] (re)
    gr_valid_end   TIMESTAMP (0) WITH TIME ZONE
                   NOT NULL
                   DEFAULT 'infinity',
                   -- end real time of record validity range (inclusive)
                   -- [2002-01-01 00:00:00] (re)
    gr_epoch_beg   BIGINT
                   NOT NULL
                   REFERENCES as_epoch (ep_epoch)
                   MATCH FULL DEFERRABLE
                   -- begin system epoch of record creation (inclusive)
                   -- [2] (ro)
);

--  Available Accounts
CREATE TABLE as_account (
    ac_oid         BIGINT
                   UNIQUE NOT NULL
                   REFERENCES as_oid (id_oid)
                   MATCH FULL DEFERRABLE
                   PRIMARY KEY,
                   -- globally unique object identifier
                   -- [7] (ro)
    ac_name        VARCHAR(30)
                   NOT NULL,
                   -- name of account (locally unique below parent only)
                   -- [pmod] (re)
    ac_description VARCHAR(80)
                   DEFAULT '', 
                   -- short description of account
                   -- [OpenPKG PMOD/PSOD Tasks] (re)
    ac_enabled     BOOLEAN
                   NOT NULL
                   DEFAULT 'true', 
                   -- whether account is enabled or not
                   -- [true] (re)
    ac_abstract    BOOLEAN
                   NOT NULL
                   DEFAULT 'true', 
                   -- whether account exists for structuring reasons only
                   -- [true] (re)
    ac_type        INTEGER
                   NOT NULL
                   DEFAULT 0
                   CHECK(ac_type >= 0 AND ac_type <= 2), 
                   -- type of account: 0=absence 1=internal?! 2=earning
                   -- [0] (re)
    ac_valid_beg   TIMESTAMP (0) WITH TIME ZONE
                   NOT NULL
                   DEFAULT 'now',
                   -- begin real time of record validity range (inclusive)
                   -- [2002-01-01 00:00:00] (re)
    ac_valid_end   TIMESTAMP (0) WITH TIME ZONE
                   NOT NULL
                   DEFAULT 'infinity',
                   -- end real time of record validity range (inclusive)
                   -- [2002-01-01 00:00:00] (re)
    ac_epoch_beg   BIGINT
                   NOT NULL
                   REFERENCES as_epoch (ep_epoch)
                   MATCH FULL DEFERRABLE
                   -- begin system epoch of record creation (inclusive)
                   -- [2] (ro)
);

--  Accounting Events
CREATE TABLE as_event (
    ev_uuid        CHAR(36)
                   PRIMARY KEY,
                   -- ISO-11578 Universally Unique Identifier (UUID)
                   -- [f81d4fae-7dec-11d0-a765-00a0c91e6bf6] (rw)
    ev_user        BIGINT
                   NOT NULL    
                   REFERENCES as_user (us_oid) 
                   MATCH FULL DEFERRABLE,
                   -- reference to user submitting the event
                   -- [42] (rw)
    ev_date        DATE
                   NOT NULL,      
                   -- date of accounting
                   -- [2002-10-01] (rw)
    ev_time_begin  TIME (0) WITH TIME ZONE
                   NOT NULL
                   DEFAULT '00:00:00',
                   -- time range begin of accounting (inclusive)
                   -- [09:00:00] (rw)
    ev_time_end    TIME (0) WITH TIME ZONE
                   NOT NULL
                   DEFAULT '00:00:00',
                   -- time range end of accounting (exclusive)
                   -- [17:00:00] (rw)
    ev_time_amount INTERVAL (0)
                   NOT NULL,
                   -- number of seconds accounted within time range
                   -- [1800] (rw)
    ev_account     BIGINT
                   NOT NULL
                   REFERENCES as_account (ac_oid)
                   MATCH FULL DEFERRABLE,
                   -- reference to account receiving the event
                   -- [7] (rw)
    ev_remark      TEXT
                   DEFAULT '',
                   -- optional remark describing event in detail
                   -- [vim, screen, bash] (rw)
    ev_status      VARCHAR(3)
                   NOT NULL
                   CHECK(ev_status ~ '^(new|mod|del)$'),
                   -- update status of event
                   -- [new] (rw)
    ev_revision    INTEGER
                   NOT NULL
                   DEFAULT 0
                   -- revision counter of event (for detecting update collisions)
                   -- [0] (rw)
);

--  System Logbook
CREATE TABLE as_log (
    lg_time        TIMESTAMP (2) WITHOUT TIME ZONE 
                   DEFAULT 'now', 
                   -- logging time
                   -- [2002-10-01 14:42:00] (ro)
    lg_entry       TEXT
                   NOT NULL       
                   -- logging entry
                   -- [login by user rse from dt1.dev.de.cw.net] (rw)
);

--  Object Relationships: Structure
CREATE TABLE as_rel_struct (
    rs_src         BIGINT
                   NOT NULL    
                   REFERENCES as_oid (id_oid) 
                   MATCH FULL DEFERRABLE,
                   -- reference to source object having relationship
                   -- [64] (re)
    rs_rel         VARCHAR(20)
                   NOT NULL
                   CHECK(rs_rel ~ '^(is-valid-for|is-located-in|has-parent|diverts-to|is-member-of|is-proxy-for|reports-to)$'),
                   -- name of relationship between source and destination object
                   -- [member-of] (re)
    rs_dst         BIGINT
                   NOT NULL    
                   REFERENCES as_oid (id_oid) 
                   MATCH FULL DEFERRABLE,
                   -- reference to destination object having relationship
                   -- [66] (re)
    rs_valid_beg   TIMESTAMP (0) WITH TIME ZONE
                   NOT NULL
                   DEFAULT 'now',
                   -- begin real time of record validity range (inclusive)
                   -- [2002-01-01 00:00:00] (re)
    rs_valid_end   TIMESTAMP (0) WITH TIME ZONE
                   NOT NULL
                   DEFAULT 'infinity',
                   -- end real time of record validity range (inclusive)
                   -- [2002-01-01 00:00:00] (re)
    rs_epoch_beg   BIGINT
                   NOT NULL
                   REFERENCES as_epoch (ep_epoch)
                   MATCH FULL DEFERRABLE
                   -- begin system epoch of record creation (inclusive)
                   -- [2] (ro)
);

--  Object Relationships: Access Control
CREATE TABLE as_rel_access (
    ra_src         BIGINT
                   NOT NULL    
                   REFERENCES as_oid (id_oid) 
                   MATCH FULL DEFERRABLE,
                   -- reference to source object (having access rights)
                   -- [64] (re)
    ra_src_depth   INTEGER
                   NOT NULL    
                   DEFAULT '-1',
                   -- graph depth starting from source object for security equivalence
                   -- [0] (re)
    ra_rel         VARCHAR(20)
                   NOT NULL
                   CHECK(ra_rel ~ '^(read|write)$'),
                   -- name of relation between source and destination object (i.e. right to grant)
                   -- [read] (re)
    ra_dst_depth   INTEGER
                   NOT NULL    
                   DEFAULT '-1',
                   -- graph depth ending at destination object for application scope
                   -- [0] (re)
    ra_dst         BIGINT
                   NOT NULL    
                   REFERENCES as_oid (id_oid) 
                   MATCH FULL DEFERRABLE,
                   -- reference to destination object (receiving access rights)
                   -- [66] (re)
    ra_valid_beg   TIMESTAMP (0) WITH TIME ZONE
                   NOT NULL
                   DEFAULT 'now',
                   -- begin real time of record validity range (inclusive)
                   -- [2002-01-01 00:00:00] (re)
    ra_valid_end   TIMESTAMP (0) WITH TIME ZONE
                   NOT NULL
                   DEFAULT 'infinity',
                   -- end real time of record validity range (inclusive)
                   -- [2002-01-01 00:00:00] (re)
    ra_epoch_beg   BIGINT
                   NOT NULL
                   REFERENCES as_epoch (ep_epoch)
                   MATCH FULL DEFERRABLE
                   -- begin system epoch of record creation (inclusive)
                   -- [2] (ro)
);

--  Object Relationships: Effective [EXTERNALLY CALCULATED]
CREATE TABLE as_rel_effective (
    re_src         BIGINT
                   NOT NULL
                   REFERENCES as_oid (id_oid)
                   MATCH FULL DEFERRABLE,
                   -- reference to source object
                   -- [64] (rw)
    re_rel         VARCHAR(20)
                   NOT NULL,
                   -- name of relation between source and destination object
                   -- [read] (rw)
    re_dst         BIGINT
                   NOT NULL
                   REFERENCES as_oid (id_oid)
                   MATCH FULL DEFERRABLE
                   -- reference to destination object
                   -- [64] (rw)
);

--  Stored Procedure: Maximum Epoch Number
CREATE FUNCTION as_epoch_max ()
RETURNS as_epoch_ep_epoch_seq.max_value%TYPE
LANGUAGE 'sql' IMMUTABLE AS '
    SELECT max_value FROM as_epoch_ep_epoch_seq;
';

--  Stored Procedure: Current Epoch Number
CREATE FUNCTION as_epoch_current ()
RETURNS as_epoch_ep_epoch_seq.max_value%TYPE
LANGUAGE 'sql' STABLE AS '
    SELECT currval(''as_epoch_ep_epoch_seq'');
';

--  Stored Procedure: Current OID Number
CREATE FUNCTION as_oid_current ()
RETURNS as_oid_id_oid_seq.max_value%TYPE
LANGUAGE 'sql' STABLE AS '
    SELECT currval(''as_oid_id_oid_seq'');
';

--  Stored Procedure: Map Account Absolute Name to Account OID
CREATE FUNCTION as_account_name2oid(TEXT, TIMESTAMP (0) WITH TIME ZONE) 
RETURNS BIGINT LANGUAGE 'plpgsql' STABLE AS '
DECLARE
    comp TEXT;
    path TEXT;
    time TIMESTAMP (0) WITH TIME ZONE;
    oid BIGINT;
BEGIN
    -- RAISE NOTICE ''as_account_name2oid(%,%)'', $1, $2;
    path := $1;
    time := $2;
    IF substr(path, 1, 1) != ''/'' THEN
        RAISE EXCEPTION ''as_account_name2oid(%,%): path not starting with slash'', $1, $2;
    END IF;
    oid := (SELECT ac_oid FROM as_account
        WHERE     ac_name = ''''
              AND ac_parent = ac_oid
              AND (time BETWEEN ac_valid_beg AND ac_valid_end));
    IF oid IS NULL THEN
        RAISE EXCEPTION ''as_account_name2oid(%,%): root path component not found'', $1, $2;
    END IF;
    path := substr(path, 2);
    -- RAISE NOTICE ''as_account_name2oid: "/" -> %'', oid;
    WHILE path != '''' LOOP
        comp := substring(path from ''^([^/]+)/*'');
        path := substring(path from ''^[^/]+/*(.*)$'');
        oid := (SELECT ac_oid FROM as_account
                WHERE     ac_name = comp 
                      AND ac_parent = oid
                      AND epoch BETWEEN ac_epoch_start AND ac_epoch_end);
        IF oid IS NULL THEN
            RAISE EXCEPTION ''as_account_name2oid(%,%): path component "%" not found'', $1, $2, comp;
        END IF;
        -- RAISE NOTICE ''as_account_name2oid: "%" -> %'', comp, oid;
    END LOOP;
    -- RAISE NOTICE ''as_account_name2oid(%,%) = %'', $1, $2, oid;
    RETURN oid;
END;
';

--  Stored Procedure: Map Account OID to Account Absolute Name
CREATE FUNCTION as_account_oid2name(TEXT, TIMESTAMP (0) WITH TIME ZONE) 
RETURNS TEXT LANGUAGE 'plpgsql' STABLE AS '
DECLARE
    oid BIGINT;
    name TEXT;
    time TIMESTAMP (0) WITH TIME ZONE;
    obj RECORD;
BEGIN
    -- RAISE NOTICE ''as_account_oid2name(%,%)'', $1, $2;
    oid := $1;
    time := $2;
    name := '''';
    LOOP
        SELECT * INTO obj FROM as_account
            WHERE     ac_oid = oid);
                  AND (epoch BETWEEN ac_epoch_start AND ac_epoch_end);
        IF NOT FOUND THEN
            RAISE EXCEPTION ''as_account_oid2name(%,%): object % not found'', $1, $2, oid;
        END IF;
        -- RAISE NOTICE ''as_account_oid2name: % -> "%"'', oid, obj.ac_name;
        IF obj.ac_parent = oid THEN
            EXIT;
        END IF;
        name := ''/'' || obj.ac_name || name;
        oid := obj.ac_parent;
    END LOOP;
    IF name = '''' THEN
        name = ''/'';
    END IF;
    -- RAISE NOTICE ''as_account_oid2name(%,%) = %'', $1, $2, name;
    RETURN name;
END;
';

