
DROP TABLE bar;
CREATE TABLE bar (
    id     INTEGER,
    parent INTEGER,
    name   TEXT
);

CREATE UNIQUE INDEX bar_idx1 ON bar USING btree (id);
CREATE UNIQUE INDEX bar_idx2 ON bar USING btree (name);
CREATE UNIQUE INDEX bar_idx3 ON bar USING btree (id, name);

INSERT INTO bar VALUES (1, 1, '');
INSERT INTO bar VALUES (2, 1, 'a');
INSERT INTO bar VALUES (3, 2, 'b');
INSERT INTO bar VALUES (4, 3, 'c');
INSERT INTO bar VALUES (5, 3, 'C');
INSERT INTO bar VALUES (6, 1, 'x');
INSERT INTO bar VALUES (7, 6, 'y');
INSERT INTO bar VALUES (8, 7, 'z');
INSERT INTO bar VALUES (9, 7, 'Z');


SELECT * FROM bar;

