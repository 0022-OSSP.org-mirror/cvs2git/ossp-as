#!/bin/sh -- # -*- perl -*-
eval 'exec perl -S $0 ${1+"$@"}'
    if $running_under_some_shell;
##
##  AS -- Accounting System
##  Copyright (c) 2002 Cable & Wireless Deutschland <http://www.cw.com/de/>
##  Copyright (c) 2002 Ralf S. Engelschall <rse@engelschall.com>
##
##  This file is part of AS, an accounting system which can be
##  found at http://as.is.eu.cw.com/
##
##  This program is free software; you can redistribute it and/or
##  modify it under the terms of the GNU General Public License
##  as published by the Free Software Foundation; either version
##  2.0 of the License, or (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
##  USA, or contact The OSSP Project <ossp@ossp.org>.
##
##  orm.pl: object relation model
##

require 5.006;
use strict;
use Getopt::Long;
use IO;

#   default configuration
my $cfg = {
    "verbose" => 0
};

#   command line parsing
Getopt::Long::Configure("bundling");
my $result = GetOptions(
    'v|verbose' => \$cfg->{verbose},
) || die "option parsing failed";

#     RS_i ns    b R     nt RT_i
# A_i ----->* S =====> T *<---- Z_i
#
# A_i    equivalence object(s)
# RS_i   source equivalence relationship(s)
# ns     equivalence maximum distance from source S
# S      source object
# b      relationship include/exclude
# R      relationship
# T      target object
# RT_i   target scope relationship(s)
# nt     scope maximum distance from target T
# Z_i    scope object(s)

# class / uuid[] mapping
my $CU = {
    "user"      => [ "u1", "u2", "u3", "u4" ],
    "group"     => [ "g1", "g2" ],
    "account"   => [ "a1", "a2" ],
    "role"      => [ "r1" ],
    "container" => [ "c1", "c2", "c3" ]
};

# uuid / class mapping
my $UC = {};
foreach my $c (keys(%{$CU})) {
    foreach my $u (@{$CU->{$c}}) {
        $UC->{$u} = $c;
    }
};

sub getclass
{
    my ($UC, $u) = @_;
    my $c = $UC->{$u};
    return $c;
}

# defined class relations
# @DR := { <RS_i,S,R,T,RT_i> } = { ... }
# S,R,T primary key
# S,T possibly wildcards

my $DR = [
    # direct user and recursive search for proxy
    # has read and write access
    # account and every object having this account as_parent
    {
    "RS_i" => [ "proxy" ],
    "S"    => [ "user" ],
    "R"    => [ "read", "write" ],
    "T"    => [ "account" ],
    "RT_i" => [ "parent" ]
    },
    # group and every object that is_member_of
    # has _read/write_access_to an
    # account and every object having this account as_parent
    {
    "RS_i" => [ "member" ],
    "S"    => [ "group" ],
    "R"    => [ "read", "write" ],
    "T"    => [ "account" ],
    "RT_i" => [ "parent" ]
    },
    # group and every object that is_member_of
    # has _read/write_access_to an
    # account and every object having this account as_parent
    {
    "RS_i" => [ "occupant", "proxy" ],
    "S"    => [ "role" ],
    "R"    => [ "read", "write", "manage" ],
    "T"    => [ "account" ],
    "RT_i" => [ "parent" ]
    },
    # group and every object that is_member_of
    # has _read/write_access_to an
    # account and every object having this account as_parent
    {
    "RS_i" => [ "occupant" ],
    "S"    => [ "role" ],
    "R"    => [ "manage" ],
    "T"    => [ "*" ],
    "RT_i" => [ "parent" ]
    },
    # user
    # is_proxy_of
    # user
    {
    "RS_i" => [ "proxy" ],
    "S"    => [ "user" ],
    "R"    => [ "proxy" ],
    "T"    => [ "user" ],
    "RT_i" => [ ]
    },
    # user
    # is_occupant_of
    # role
    {
    "RS_i" => [ ],
    "S"    => [ "user" ],
    "R"    => [ "occupant" ],
    "T"    => [ "role" ],
    "RT_i" => [ ]
    },
    # account
    # has as_parent an
    # account
    {
    "RS_i" => [ "account" ],
    "S"    => [ "account" ],
    "R"    => [ "*" ],
    "T"    => [ "account" ],
    "RT_i" => [ ]
    },
    # user and group
    # can be memberof
    # group
    {
    "RS_i" => [ ],
    "S"    => [ "user", "group" ],
    "R"    => [ "member" ],
    "T"    => [ "group" ],
    "RT_i" => [ ]
    },
    # user and every object that is_proxy_of
    # can_manage
    # anything and every object having this thing as parent
    {
    "RS_i" => [ "proxy" ],
    "S"    => [ "user" ],
    "R"    => [ "manage" ],
    "T"    => [ "*" ],
    "RT_i" => [ "parent" ]
    }
];
dumpdefinedrelations($DR);
sub dumpdefinedrelations
{
    my ($DR) = @_;
    print "\ndumping defined relations\n";
    dumpdefinedrelation("RS_i", "S"   , "R",    "T"   , "RT_i");
    dumpdefinedrelation("-"x80, "-"x80, "-"x80, "-"x80, "-"x80);
    foreach my $i (@{$DR}) {
        foreach my $rsi (defined @{$i->{"RS_i"}} ? @{$i->{"RS_i"}} : " - ") {
        foreach my $s   (@{$i->{"S"}}) {
        foreach my $r   (@{$i->{"R"}}) {
        foreach my $t   (@{$i->{"T"}}) {
        foreach my $rti (defined @{$i->{"RT_i"}} ? @{$i->{"RT_i"}} : " - ") {
            dumpdefinedrelation($rsi, $s, $r, $t, $rti);
        }
        }
        }
        }
        }
    }
}

sub dumpdefinedrelation
{
    my ($RS_i, $S, $R, $T, $RT_i) = @_;
    print " " . substr $RS_i . " "x80, 0, 15;
    print " " . substr $S    . " "x80, 0, 15;
    print " " . substr $R    . " "x80, 0, 20;
    print " " . substr $T    . " "x80, 0, 15;
    print " " . substr $RT_i . " "x80, 0, 15;
    print " \n";
}

# set instance relations
# @SR := { <S,ns,b,R,nt,T> } = { ... }
# SR = n
my $SR = [
    # trustees
    { "S" => "g1", "ns" => "oo", "b" => "+", "R" => "read",   "nt" => "oo", "T" => "a1" },
    { "S" => "u1", "ns" => "oo", "b" => "+", "R" => "write",  "nt" => "oo", "T" => "a2" },
    { "S" => "r1", "ns" => "oo", "b" => "+", "R" => "manage", "nt" => "oo", "T" => "a1" },
    { "S" => "u4", "ns" => "oo", "b" => "-", "R" => "read",   "nt" => "oo", "T" => "a1" },
    { "S" => "u4", "ns" => "oo", "b" => "+", "R" => "read",   "nt" => "oo", "T" => "a2" },
    # account structure
    { "S" => "a1", "ns" => "oo", "b" => "+", "R" => "parent", "nt" => "oo", "T" => "a2" },
    # group membership
    { "S" => "u1", "ns" => "oo", "b" => "+", "R" => "member", "nt" => "oo", "T" => "g1" },
    { "S" => "g2", "ns" => "oo", "b" => "+", "R" => "member", "nt" => "oo", "T" => "g1" },
    { "S" => "u2", "ns" => "oo", "b" => "+", "R" => "member", "nt" => "oo", "T" => "g2" },
    { "S" => "u4", "ns" => "oo", "b" => "+", "R" => "member", "nt" => "oo", "T" => "g2" },
    # proxies                                                                   
    { "S" => "u2", "ns" => "oo", "b" => "+", "R" => "proxy",  "nt" => "oo", "T" => "u1" },
    { "S" => "u3", "ns" => "oo", "b" => "+", "R" => "proxy",  "nt" => "oo", "T" => "u2" }
];
dumpsetrelations($SR);
sub dumpsetrelations
{
    my ($SR) = @_;
    print "\ndumping set relations\n";
    dumpsetrelation("S"   , "ns"  , "b"   , "R"   , "nt"  , "T"   );
    dumpsetrelation("-"x80, "-"x80, "-"x80, "-"x80, "-"x80, "-"x80);
    foreach my $i (@{$SR}) {
        dumpsetrelation($i->{S}, $i->{ns}, $i->{b}, $i->{R}, $i->{nt}, $i->{T});
    }
}

sub dumpsetrelation
{
    my ($S, $ns,$b, $R, $nt, $T) = @_;
    print " " . substr $S  . " "x80, 0, 15;
    print " " . substr $ns . " "x80, 0,  2;
    print " " . substr $b  . " "x80, 0,  1;
    print " " . substr $R  . " "x80, 0, 20;
    print " " . substr $nt . " "x80, 0,  2;
    print " " . substr $T  . " "x80, 0, 15;
    print " \n";
}

#   FUNCTION querysetrelations($SR, $S, $R, $T, @Q);
#   INPUT
#    function takes $SR (set relations),
#    a $S (source), $R (relation), $T (target) triple and
#    an array listing the desired result fields as input.
#    Any of $S, $R and $T may be wildcards.
#   OUTPUT
#    It returns an array of arrays holding the desired fields.
#   EXAMPLE
#    querysetrelations($SR, "*", "*", "g1", ["S", "R"]); exit;
sub querysetrelations
{
    my ($SR, $S, $R, $T, $Q) = @_;
    my @O = ();

    #printf "querysetrelations(..., %s, %s, %s, ...)\n", rightstr(" ", $S, 3), rightstr(" ", $R, 10), rightstr(" ", $T, 3) if($cfg->{verbose} == 1);
    foreach my $sr (@{$SR}) {
        if (   ($S eq "*" || $S eq $sr->{S})
            && ($R eq "*" || $R eq $sr->{R})
            && ($T eq "*" || $T eq $sr->{T})
              ) {
            my @o = ();
            foreach my $q (@{$Q}) {
                push @o, $sr->{$q};
            }
            push @O, \@o;
        }
    }

    if($cfg->{verbose} == 1) {
        foreach my $o (@O) {
            print "querysetrelations=";
            foreach my $i (@{$o}) {
                print "***$i*** ";
            }
            print "\n";
        }
    }

    return @O;
}

# FUNCTION getimmediateequiv($UC, $DR, $S, $R, $T);
# INPUT
#  function takes $UC (uuid/class lookup), $DR (defined relations) and
#  a $S (source), $R (relation), $T (target) triple as input.
# OUTPUT
#  It returns an array of all immediate (non recursive) equivalences
# FIXME
# strategy issue: when hunting down $DR for relationships multiple matches
# could be found. Three options to handle them came into my mind
# 1.) the closest match is a default only and the most general match is taken
# 2.) the closest match is the most individual and is taken over more general matches
# 3.) the summary of all matches are taken
# The first two will require a definition of what a "closest match" is.
# Currently, only the third is implemented
# 
sub getimmediateextensions
{
    my ($UC, $DR, $S, $R, $T) = @_;

    #printf "getimmediateextensions(...) object relation %s %s %s ", rightstr(" ", $S, 3), rightstr(" ", $R, 6), rightstr(" ", $T, 3);
    $S = getclass($UC, $S);
    $T = getclass($UC, $T);
    #printf "becomes class relation %s %s %s:", rightstr(" ", $S, 7), rightstr(" ", $R, 6), rightstr(" ", $T, 7);
    my $rsi = {};
    my $rti = {};
    foreach my $dr (@{$DR}) {
        foreach my $s (@{$dr->{"S"}}) {
        foreach my $r (@{$dr->{"R"}}) {
        foreach my $t (@{$dr->{"T"}}) {
            if (   ($s eq "*" || $s eq $S)
                && ($r eq "*" || $r eq $R)
                && ($t eq "*" || $t eq $T)
                  ) {
                foreach my $i (@{$dr->{"RS_i"}}) {
                    $rsi->{$i} = 1;
                };
                foreach my $i (@{$dr->{"RT_i"}}) {
                    $rti->{$i} = 1;
                };
            }
        }
        }
        }
    }
    my @RSI = ();
    foreach my $i (keys %{$rsi}) {
        push @RSI, $i;
        #print " $i";
        }
    #print "; ";
    my @RTI = ();
    foreach my $i (keys %{$rti}) {
        push @RTI, $i;
        #print " $i";
        }
    #print "\n";
    return \@RSI, \@RTI;
}

sub rightstr
{
        my ($f, $s, $l) = @_;
        my $r = $f x $l . $s;
        $r = substr($r, length($r) - $l, $l);
        return $r;
}

# configured to effective relationship algorithm
#function drcr2er(@DR, @SR): @ER { // O(n^3)
my $ER = geteffectiverelations($UC, $DR, $SR);
dumpeffectiverelations($ER);
sub dumpeffectiverelations
{
    my ($ER) = @_;
    print "\ndumping effective relations\n";
    dumpeffectiverelation("S"   , "ns"  , "b"   , "R"   , "nt"  , "T"   );
    dumpeffectiverelation("-"x80, "-"x80, "-"x80, "-"x80, "-"x80, "-"x80);
    foreach my $i (sort keys %{$ER}) {
        my $x = $ER->{$i};
        dumpeffectiverelation($x->{S}, $x->{ns}, $x->{b}, $x->{R}, $x->{nt}, $x->{T});
    }
}
sub dumpeffectiverelation
{
    my ($S, $ns, $b, $R, $nt, $T,) = @_;
    print " " . substr $S  . " "x80, 0, 15;
    print " " . substr $ns . " "x80, 0,  2;
    print " " . substr $b  . " "x80, 0,  1;
    print " " . substr $R  . " "x80, 0, 20;
    print " " . substr $nt . " "x80, 0,  2;
    print " " . substr $T  . " "x80, 0, 15;
    print " \n";
}

#    effective relationships (calculated result)
#    @ER := { <ds,S,b,R,T,dt> } = 0
sub geteffectiverelations
{
    my ($UC, $DR, $SR) = @_;

    my $ER = {};
    foreach my $sr (@{$SR}) {
        if($cfg->{verbose} == 1) {
            print "geteffectiverelations processing set relationship";
            dumpsetrelation($sr->{"S"}, $sr->{"ns"}, $sr->{"b"}, $sr->{"R"}, $sr->{"nt"}, $sr->{"T"});
        }
        # equivalence
        my $EQR = {};
        addequivrelation($EQR, $sr->{"S"}, 0);
        resolveequivrelations($EQR, $UC, $DR, $SR, $sr->{"S"}, $sr->{"ns"}, $sr->{"b"}, $sr->{"R"}, $sr->{"nt"}, $sr->{"T"}, 1);
        if($cfg->{verbose} == 1) {
            foreach my $eqr (keys %{$EQR}) {
                print "geteffectiverelations eqr=$eqr $EQR->{$eqr}\n";
            }
        }
        # relation
        if($cfg->{verbose} == 1) {
            print "geteffectiverelations $sr->{b}, $sr->{R}\n";
        }
        # scope
        my $SCR = {};
        addscoperelation($SCR, $sr->{"T"}, 0);
        resolvescoperelations($SCR, $UC, $DR, $SR, $sr->{"S"}, $sr->{"ns"}, $sr->{"b"}, $sr->{"R"}, $sr->{"nt"}, $sr->{"T"}, 1);
        if($cfg->{verbose} == 1) {
            foreach my $scr (keys %{$SCR}) {
                print "geteffectiverelations scr=$scr $SCR->{$scr}\n";
            }
        }
        # effective
        foreach my $eqr (keys %{$EQR}) {
            foreach my $scr (keys %{$SCR}) {
                print "geteffectiverelations $eqr, $EQR->{$eqr}, $sr->{b}, $sr->{R}, $SCR->{$scr}, $scr\n" if($cfg->{verbose} == 1);
                addeffectiverelation($ER, $eqr, $EQR->{$eqr}, $sr->{"b"}, $sr->{"R"}, $SCR->{$scr}, $scr);
            }
        }
    }
    return $ER;
}

sub resolveequivrelations
{
    my ($EQR, $UC, $DR, $SR, $S, $ns, $b, $R, $nt, $T, $d) = @_;

    print "resolveequivrelations(..., $S, $ns, $b, $R, $nt, $T, $d)\n" if($cfg->{verbose} == 1);
    if (($ns ne "oo") && ($ns < $d)) {
        print "resolveequivrelations reached or exceeded max nesting $ns > $d\n" if($cfg->{verbose} == 1);
        return;
    }
    my ($IE, $DUMMY) = getimmediateextensions($UC, $DR, $S, $R, $T);
    print "resolveequivrelations looking at @{$IE} -> $S\n" if($cfg->{verbose} == 1);
    foreach my $ie (@{$IE}) {
        my @QR = querysetrelations($SR, "*", $ie, $S, ["S"]);
        foreach my $qr (@QR) {
            printf "resolveequivrelations query for %s -> %s found %s receives implicit relation %s to %s\n", 
             rightstr(" ", $ie, 6), rightstr(" ", $S, 3), rightstr(" ", $qr->[0], 3), rightstr(" ", $R, 6), rightstr(" ", $T, 3) if($cfg->{verbose} == 1);
            addequivrelation($EQR, $qr->[0], $d);
            $d = $d + 1;
            print "resolveequivrelations running a recursion >>>$d resolveequivrelations(..., $qr->[0], $R, $T, $d)\n" if($cfg->{verbose} == 1);
            resolveequivrelations($EQR, $UC, $DR, $SR, $qr->[0], $ns, $b, $R, $nt, $T, $d);
            print "resolveequivrelations back from recursion <<<$d resolveequivrelations(..., $qr->[0], $R, $T, $d)\n" if($cfg->{verbose} == 1);
            $d = $d - 1;
        }
    }
}

sub resolvescoperelations
{
    my ($SCR, $UC, $DR, $SR, $S, $ns, $b, $R, $nt, $T, $d) = @_;

    print "resolvescoperelations(..., $S, $ns, $b, $R, $nt, $T, $d)\n" if($cfg->{verbose} == 1);
    if (($nt ne "oo") && ($nt < $d)) {
        print "resolvescoperelations reached or exceeded max nesting $nt > $d\n" if($cfg->{verbose} == 1);
        return;
    }
    my ($DUMMY, $IS) = getimmediateextensions($UC, $DR, $S, $R, $T);
    print "resolvescoperelations looking at $T <- @{$IS}\n" if($cfg->{verbose} == 1);
    foreach my $is (@{$IS}) {
        my @QR = querysetrelations($SR, $T, $is, "*", ["T"]);
        foreach my $qr (@QR) {
            printf "resolvescoperelations query for %s <- %s found %s receives implicit relation %s to %s\n", 
             rightstr(" ", $is, 6), rightstr(" ", $T, 3), rightstr(" ", $qr->[0], 3), rightstr(" ", $R, 6), rightstr(" ", $T, 3) if($cfg->{verbose} == 1);
            addscoperelation($SCR, $qr->[0], $d);
            $d = $d + 1;
            print "resolvescoperelations running a recursion >>>$d resolvescoperelations(..., $qr->[0], $R, $T, $d)\n" if($cfg->{verbose} == 1);
            resolvescoperelations($SCR, $UC, $DR, $SR, $S, $ns, $b, $R, $nt, $qr->[0], $d);
            print "resolvescoperelations back from recursion <<<$d resolvescoperelations(..., $qr->[0], $R, $T, $d)\n" if($cfg->{verbose} == 1);
            $d = $d - 1;
        }
    }
}

sub addequivrelation
{
    my ($EQR, $S, $ns) = @_;
    $EQR->{"$S"} = $ns;
}

sub addscoperelation
{
    my ($SCR, $T, $nt) = @_;
    $SCR->{"$T"} = $nt;
}

sub addeffectiverelation
{
    my ($ER, $S, $ns, $b, $R, $nt, $T) = @_;
    
    my $key = $T . "*" . $R . "*" . $S;
    my $er = $ER->{$key};
    if (not defined($er)) {
        $ER->{$key} = { "S" => $S,  "ns" => $ns, "b" => $b, "R" => $R, "nt" => $nt, "T" => $T };
    } else {
        print "addeffectiverelation $er->{ns} <=> $ns  &&  $er->{nt} <=> $nt\n" if($cfg->{verbose} == 1);
        if (($er->{"ns"} == $ns) && ($er->{"nt"} == $nt)) { print "addeffectiverelation #1!\n" if($cfg->{verbose} == 1);                                                                                          } #<ds,S,b,R,T,dt>, resolve b via R-specific conflict flag (prefer include or exlude) FIXME
        if (($er->{"ns"} == $ns) && ($er->{"nt"} <  $nt)) { print "addeffectiverelation #2!\n" if($cfg->{verbose} == 1);                                                                                          } #<ds,S,b,R,T,dt> (nop)
        if (($er->{"ns"} == $ns) && ($er->{"nt"} >  $nt)) { print "addeffectiverelation #3!\n" if($cfg->{verbose} == 1);                                     $er->{"b"} = $b; $er->{"T"} = $T; $er->{"nt"} = $nt; } #<ds,S,new-b,R,new-T,new-dt>
        if (($er->{"ns"} <  $ns) && ($er->{"nt"} == $nt)) { print "addeffectiverelation #4!\n" if($cfg->{verbose} == 1);                                                                                          } #<ds,S,b,R,T,dt> (nop)
        if (($er->{"ns"} <  $ns) && ($er->{"nt"} <  $nt)) { print "addeffectiverelation #5!\n" if($cfg->{verbose} == 1);                                                                                          } #<ds,S,b,R,T,dt> (nop)
        if (($er->{"ns"} <  $ns) && ($er->{"nt"} >  $nt)) { print "addeffectiverelation #6!\n" if($cfg->{verbose} == 1);                                     $er->{"b"} = $b; $er->{"T"} = $T; $er->{"nt"} = $nt; } #<ds,S,new-b,R,new-T,new-dt>
        if (($er->{"ns"} >  $ns) && ($er->{"nt"} == $nt)) { print "addeffectiverelation #7!\n" if($cfg->{verbose} == 1); $er->{"ns"} = $ns; $er->{"S"} = $S; $er->{"b"} = $b;                                     } #<new-ds,new-S,new-b,R,T,dt>
        if (($er->{"ns"} >  $ns) && ($er->{"nt"} <  $nt)) { print "addeffectiverelation #8!\n" if($cfg->{verbose} == 1); $er->{"ns"} = $ns; $er->{"S"} = $S; $er->{"b"} = $b;                                     } #<new-ds,new-S,new-b,R,T,dt>
        if (($er->{"ns"} >  $ns) && ($er->{"nt"} >  $nt)) { print "addeffectiverelation #9!\n" if($cfg->{verbose} == 1); $er->{"ns"} = $ns; $er->{"S"} = $S; $er->{"b"} = $b; $er->{"T"} = $T; $er->{"nt"} = $nt; } #<new-ds,new-S,new-b,R,new-T,new-dt>
    }
}
