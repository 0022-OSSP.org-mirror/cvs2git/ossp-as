
  Object Relationship Model (can be used for ACLs)
  ================================================

  Title:    Object Relationship Model
  Subtitle: A flexible mechanism for modeling object relationships
  Author:   Ralf S. Engelschall <rse@engelschall.com>
  Author:   Thomas Lotterer     <thomas@lotterer.net>
  Genesis:  28-Oct-2002
  Modified: 28-Oct-2002

  Background
  ----------

    In the end of 2002 Cable&Wireless introduced a "Management Operation
    Framework". Part of it was a system which allows employees to log
    their working hours to certain accounts. This enabled them and their
    managers to generate reports and figure out on which tasks they
    spent their time. The Application services development team were
    faced with the challenge to implement a solution which accomplishes
    the requirements of collecting and reporting in an electronical
    form.

  Abstract
  --------

    An accounting system by nature stores information which needs
    controlled access. So we didn't come far along the design phase util
    we hit the point to introduce access control. In the first place we
    thought about another quick'n'dirty implementation of the usual "who
    has_access_to where" tripple using users to specify who is doing
    something, rights to specify the type of access and use the accounts
    as the ultimate target where the rights are being applied.  But that
    was not enough.
    
  Equivalence
  -----------

    Obviously users need to be grouped to limit the number of rights
    assignments. Groups are also helpful to add users to and remove
    users from them and disregard the actual rights which were or will
    be given to the groups at some point in time. Well, this already
    introduced a concept of implicit rights. Those are not limited to
    users in groups. Users might have vacation or sickness proxies,
    managers have secretaries acting on behalf of them, groups might be
    nested in groups. Other types of grouping like a hierarchy of users
    could be useful and other types of indirection like users occupying
    organizational roles could be useful as well. This means there is a
    whole bunch of things which extend the security equivalence of user.

  Scope
  -----

    On the other side, granting rights to just one account is a tedious
    task when there are many of them. So accounts need to be organized
    in some way using groups or a hierarchy as well. Access control can
    then be configured by granting rights to some superaccount which
    means the assignment implicitly extends its scope to all
    subaccounts.
  
  Objects
  -------

    We quickly found out that access control will not be limited to
    granting users rights to accounts. The users themselves need to be
    managed as well and again not anyone should be able to manage
    everyone. As the same concepts of security equivalence and scope
    could be applied we decided to treat all elements of the security
    system, including all of them used to create the "who" and the
    "where", as uniform objects. Any object could be the one "who"
    receives rights and any object could be the target of a rights
    assignment. This makes the "who has_access_to where" tripple a
    "object has_access_to object" construct. This enabled us apply the
    same concepts for account management and for user management. Please
    note not all possible assignments make sense, but useless one don't
    cause harm. You could mindlessly assign the rights to manage a bunch
    of users to an account but the account, being some dumb storage area
    in the system, will never notice.

  Relationship
  ------------

    As we went deeper into the topic we found that the system must have
    some definitions on how security equivalence and scope extension
    work. It's obvious for a human that a group membership extends
    security equivalence and and a hierarchy extends scope down the
    tree. More examples have been given in the previous paragraphs. We
    finally found a method to describe those relations between objects
    and the original requirement to build an access control system
    became just a special application of that concept.

  Ruleset
  -------

      RS_i ns    b R     nt RT_i
  A_i ----->* S =====> T *<---- Z_i

  Definitions
  -----------

    o ER (effective relations)

        Effective relations are computed from relation assignments by
        evaluating set relations and taking equivalences and scopes into
        account. The ER tripple entries can be stored in a simple table
        and queried using SQL "join".

                            who             what          where
                        source_object --- relation --> target_object

        The source (who) of the relation can be any object. The relation
        (what) must have meaning to the application.  The target (where)
        can be any object, too. it defines where the relation is to be
        applied to.

        Given n objects and m relations the maximum number of ER entries
        will be n^2*m which is certainly a scalability limitation of
        this model when the ER are required to be precalculated rather
        than effective relations being calculated on the fly.

        Example: when relations are about access control, effective
                 relations are effective rights.

    o DR (defined relations)

        Defined relations identify the relation schema of an application. They
        are usually fixed during the lifetime of a release.

        RS_i   extension on source
        S      source object
        R      relationship
        T      target object
        RT_i   extension on target

        They describe the possible relation R, a source object S can
        have to a target object T. A relation might be extended on
        the source side by equivalence RS_i or extended on the target
        side by scope RT_i.

        Example one from the Perl implementation orm.pl

        # group (source object S) and every object that is member of
        # (source extension RS_i) has read or write (relation R) access
        # to an account (target object T) and every object having this
        # account as parent (scope extension RT_i)
        #
        "RS_i" => [ "member" ],
        "S"    => [ "group" ],
        "R"    => [ "read", "write" ],
        "T"    => [ "account" ],
        "RT_i" => [ "parent" ]
        #
        # note that leaving out scope extention RT_i would mean that
        # read and write rights are only granted directly to an object
        # and rights do not flow down the tree (the existence of a
        # "parent" obviously tells a story about a tree structure)
        #
        # note that leaving out source extension RS_i would mean that
        # read and write rights can be assigned to and are valid for a
        # group object itself but not for its members.
        
        Example two from the Perl implementation orm.pl

        # user (source object S) and every object that is proxy of
        # (source extension RS_i) can be a proxy of (relation R) another
        # user (target object T). There is no scope extension.
        #
        "RS_i" => [ "proxy" ],
        "S"    => [ "user" ],
        "R"    => [ "proxy" ],
        "T"    => [ "user" ],
        "RT_i" => [ ]
        #
        # note that leaving out source extension RS_i would mean that
        # the declaration of a proxy does not mean that a proxy of that
        # proxy is also a proxy of the object the proxy proxies.

    o SR (set relations)

        Set relations are assignments. They, together with the defined
        relations, are the input to the algorithm which computes the
        effective relations.

        A_i    equivalence object(s)
        RS_i   source equivalence relationship(s)
        ns     equivalence maximum distance from source S
        S      source object
        b      relationship include/exclude
        R      relationship
        T      target object
        RT_i   target scope relationship(s)
        nt     scope maximum distance from target T
        Z_i    scope object(s)

        Example: when relations are about access control, set relations
                 are rights assignmens.

    o Equivalence

        Equivalences must be resolved to cover any equivalent objects.
        This requires a recursive search for all equivalent relations
        "RS_i". Recursion stops if no more relations are found or the
        search hit the "ns" maximum nesting mark. An "ns" of 0 inhibits
        the search for equivalent relations.

    o Scope

        Scope must be resolved to cover any scoped objects.  This
        requires a recursive search for all scoped relations "RT_i".
        Recursion stops if no more scopes are found or the search hit
        the "nt" maximum nesting mark.  An "nt" of 0 inhibits the search
        for equivalent relations.

    o Path

        The model can handle inclusive and exclusive trustees. When the
        effective relations are calculated every trustee being applied
        to a scope requires a strength to be used. Stronger relations
        override weaker relations. Strength is computed from the path on
        the equivalence graph where shorter pathes mean stronger
        relations.

  Algorithm
  ---------

  ...

