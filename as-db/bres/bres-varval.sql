
create table as_cfg (hid INTEGER DEFAULT 1, var TEXT UNIQUE, val TEXT);
create table as_cfg_tmp (hid INTEGER DEFAULT 2, var TEXT UNIQUE, val TEXT) inherits (as_cfg);

# select single variable
select var,val from as_cfg where var = 'foo' order by hid desc limit 1;

# select all variables
select as_cfg.var,as_cfg.val from as_cfg, ( select var,max(hid) as hid
from as_cfg group by var ) as foo where foo.var = as_cfg.var and foo.hid = as_cfg.hid;

