#!/bin/sh -- # -*- perl -*-
eval 'exec perl -S $0 ${1+"$@"}'
    if $running_under_some_shell;
##
##  AS -- Accounting System
##  Copyright (c) 2002-2003 Cable & Wireless Deutschland <http://www.cw.com/de/>
##  Copyright (c) 2002-2003 Ralf S. Engelschall <rse@engelschall.com>
##
##  This file is part of AS, an accounting system which can be
##  found at http://as.is.eu.cw.com/
##
##  This program is free software; you can redistribute it and/or
##  modify it under the terms of the GNU General Public License
##  as published by the Free Software Foundation; either version
##  2.0 of the License, or (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
##  USA, or contact The OSSP Project <ossp@ossp.org>.
##
##  as_db.pl: AS Database Setup Utility
##

require 5.006;
use strict;
use Getopt::Long;
use lib ".";
require "as_db.pm";
import AS::DB;

#   program information
my $progname = "as_db";
my $progvers = "0.0.1";

#   option defaults
my $cfg = {
    "su_username" => "",
    "su_password" => "",
    "su_database" => "",
    "as_username" => "",
    "as_password" => "",
    "as_database" => "",
    "as_expiring" => "",
    "db_schema"   => "",
    "db_host"     => "",
    "db_port"     => "",
};
my $opt = {
    "verbose"     => 0,
    "version"     => 0,
    "help"        => 0,
};

#   fill in defaults from module
my $as = new AS::DB;
foreach my $attr (keys(%{$cfg})) {
    $cfg->{$attr} = $as->attr($attr);
}

#   exception handling support 
$SIG{__DIE__} = sub {
    my ($err) = @_;
    # $err =~ s|\s+at\s+.*||s if (not $opt->{verbose});
    print STDERR "$progname:ERROR: $err ". ($! ? "($!)" : "") . "\n";
    exit(1);
};

#   print usage message
sub usage {
    print "Usage: $progname [options] create|destroy\n" .
          "Available options:\n" .
          " -U,--su-username=STR set super-user username (default: \"".$cfg->{'su_username'}."\")\n" .
          " -P,--su-password=STR set super-user password (default: \"".$cfg->{'su_password'}."\")\n" .
          " -D,--su-database=STR set super-user database (default: \"".$cfg->{'su_database'}."\")\n" .
          " -u,--as-username=STR set AS user username (default: \"".$cfg->{'as_username'}."\")\n" .
          " -p,--as-password=STR set AS user password (default: \"".$cfg->{'as_password'}."\")\n" .
          " -d,--as-database=STR set AS user database (default: \"".$cfg->{'as_database'}."\")\n" .
          " -e,--as-expiring=STR set AS user expiring (default: \"".$cfg->{'as_expiring'}."\")\n" .
          " -s,--db-schema=FILE  set file name of AS schema definition (default: \"".$cfg->{'db_schema'}."\")\n" .
          " -h,--db-host=HOST    set host name of PostgreSQL RBMS (default: \"".$cfg->{'db_host'}."\")\n" .
          " -l,--db-port=PORT    set port number of PostgreSQL RBMS (default: \"".$cfg->{'db_port'}."\")\n" .
          " -v,--verbose         enable verbose run-time mode\n" .
          " -V,--version         print program version\n" .
          " -h,--help            print this usage page\n";
}

#   command line parsing
Getopt::Long::Configure("bundling");
my $result = GetOptions(
    'U|su-username=s' => \$cfg->{su_username},
    'P|su-password=s' => \$cfg->{su_password},
    'D|su-database=s' => \$cfg->{su_database},
    'u|as-username=s' => \$cfg->{as_username},
    'p|as-password=s' => \$cfg->{as_password},
    'd|as-database=s' => \$cfg->{as_database},
    'e|as-expiring=s' => \$cfg->{as_expiring},
    's|db-schema=s'   => \$cfg->{db_schema},
    'h|db-host=s'     => \$cfg->{db_host},
    'l|db-port=s'     => \$cfg->{db_port},
    'v|verbose'       => \$opt->{verbose},
    'V|version'       => \$opt->{version},
    'h|help'          => \$opt->{help},
) || die "option parsing failed";
if ($opt->{help}) {
    &usage();
    exit(0);
}
if ($opt->{version}) {
    print "$progname $progvers\n";
    exit(0);
}
if (@ARGV != 1) {
    die "invalid number of argument (use --help for help)";
}
if ($ARGV[0] !~ m/^(create|destroy)$/) {
    die "invalid command \"$ARGV[0]\"";
}

#   create AS DB object
my $as = new AS::DB
    or die "unable to create AS DB object";

#   configure AS DB object
foreach my $attr (keys(%{$cfg})) {
    $as->attr($attr, $cfg->{$attr});
}

if ($ARGV[0] eq 'create') {
    #   create AS database
    $as->schema_create();
}
elsif ($ARGV[0] eq 'destroy') {
    #   destroy AS database
    $as->schema_destroy();
}

#   destroy AS DB object
$as->destroy;

exit(0);

