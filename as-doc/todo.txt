20021029 Thomas

- access control inside the as system uses object relationships. An
  event is not an object in the access control so there is no way to
  directly grant rights to an entry. My idea is to use a simple and
  hardcoded relation to control access to an entry. In order to keep the
  generic words read and write untouched i'll recommend we call them more
  accurately account and report.
  
  The hardcoded relation to "account" an event requires the
  authenticated user to have "account" rights to the user object being
  accounted for (usually itself, but this is a special case only) *and*
  to have "account" rights to the "account" object being accounted to.

  if (   check(auth, "account", user   )
      && check(auth, "account", account)
        ) {
      allow account aka write
      }

  The hardcoded relation to "report" an
  event requires the authenticated user to have "report" rights to the
  user object being reported for (usually itself for users and a larger
  quantity for managers) *or* to have "report" rights to the "account"
  object being reported from.

  if (   check(auth, "account", user   )
      || check(auth, "account", account)
        ) {
      allow report aka read
      }

  Access control is always current, there is no history information
  about access control. This means that a past report might not be
  repeated later by the same persons if these have lost their rights to
  do so. This is the way i would expect security to work. Regarding past
  reports it might be necessary for a person to have the job done by
  someone who is now in the position to have proper read access.
  Because of the hardcoded account/report described above a user
  will only be inhibited from (re)creating a report from the past if
  he looses report rights from both the user object and the account
  object. If this is the case, security would have been modified for
  some good reason.

  These two simple hardcoded relations solve the example of the problem
  for "implicit *different* rights" discussed on "20021027 Brainstorming
  with Ralf and Thomas" by nature. There is no need of a implicit
  different right to solve the example problem. However, we should
  still consider those implicit different rights being used
  somewhere else in the future.

  These two simple hardcoded relations also solve the "revoking rights"
  problem being discussed on "20021029 Brainstorming with Stephan,
  Christoph, Ralf and Thomas" in a predictive and convenient way.



20021029 Brainstorming with Stephan, Christoph, Ralf and Thomas

- see /petra/project below but think of Petra revoking rights for "Thomas" from "/petra/project"
  How does "Ralf" see "Thomas"'s events when creating reports for the past? Problem is that
  rights are current and have no history.
  [solved, see "20021029 Thomas"]

- what about time-controlled actions i.e. schedule diversion based on future time when the
  teamleader is on vacation?
  [FIXME]

- what about different models of working time, i.e. 4*10h/week vs. 5*8h/week
  [FIXME]

- need for revision control to create past reports
  [FIXME]



20021027 Brainstorming with Ralf and Thomas

- implicit *different* rights.

  1.) teamleader Petra grants "Thomas" "write" access to her "/petra/project"
  2.) "Thomas" writes event to "/petra/project"
  3.) "Ralf" is teamleader of "Thomas"
  4.) "Ralf" should receive implicit "read" rights to "Thomas"'s events in "/petra/project"
  [solved, see "20021029 Thomas"]

