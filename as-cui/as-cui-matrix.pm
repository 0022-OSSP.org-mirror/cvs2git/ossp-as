##
##  AS -- Accounting System
##  Copyright (c) 2002 Cable & Wireless Deutschland <http://www.cw.com/de/>
##  Copyright (c) 2002 Ralf S. Engelschall <rse@engelschall.com>
##
##  This file is part of AS, an accounting system which can be
##  found at http://as.is.eu.cw.com/
##
##  This program is free software; you can redistribute it and/or
##  modify it under the terms of the GNU General Public License
##  as published by the Free Software Foundation; either version
##  2.0 of the License, or (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
##  USA, or contact The OSSP Project <ossp@ossp.org>.
##
##  as-cui-matrix.pm: Curses::UI based matrix/spreadsheet widget
##

package as_cui_matrix;

use strict;
use Curses;             # OpenPKG perl-curses
use Curses::UI;         # OpenPKG perl-curses
use Curses::UI::Common; # OpenPKG perl-curses

my $matrixdata = {};
my $verbose  = 0;

my $breakout = 0; #flag to break out from inner loop
my $vcscroll = 0; #callback tells inner loop to scroll wholly view n steps horizontally (neg = left, pos = right)
my $vrscroll = 0; #callback tells inner loop to scroll wholly view n steps vertically (neg = up, pos = down)
my $datacoll = 0; #callback tells inner loop to redraw wholly view while trying to reuse widgets if size permits
my $vcpos    = 0; #callback tells inner loop to scroll wholly view horizontally (neg = left, pos = right)
my $vrpos    = 0; #callback tells inner loop to scroll wholly view vertically (neg = up, pos = down)
my $editcell = 0; #flags edit or movement mode
my $escapeit = 0; #flags users desire to escape from popups and cancel the action
my $widget = {};
my $matrixwidget = {};
my $matrixwidgetvisualcell = {};
my $matrixwidgetdatacell = {};
my $cui = undef;

sub rightmostvisiblefit {
    my $c;
    my $lastc;
    my $wremain;
    my $realw;
    my $visualc;
    my $go;

    #   find leftmost column for the rightmost visible fit
    $visualc = 0;
    $lastc = $matrixdata->{"Columns"} - 1;
    $wremain = $matrixwidget->{"Width"};
    #printf STDERR "rightmostvisiblefit lastc=%d\n", (defined $lastc) ? $lastc : "undef";
    #printf STDERR "rightmostvisiblefit wremain=%d\n", (defined $wremain) ? $wremain : "undef";
    $go = 1;
    $c = -1;
    do {
        if (not $matrixdata->{"CH.$c"}->{"Colhide"}) {
            my $w = $matrixdata->{"CH.$c"}->{"Width"};
            $realw = $w + $matrixdata->{"CH.$c"}->{"Colgap"};
            $wremain -= $realw;
            $visualc++ if ($c >= 0 and $wremain >= 0);
        }
        if ($wremain < 0) {
            $go = 0;
        }
        else {
            $c = $c == -1 ? $lastc : --$c;
            $go = 0 if ($c < 0);
        }
    } while ($go);
    $c++;
    #printf STDERR "rightmostvisiblefit c=%d, visualc=%d\n", $c, $visualc;
    return ($c, $visualc);
}

sub bottommostvisiblefit {
    my $r;
    my $lastr;
    my $hremain;
    my $realh;
    my $visualr;
    my $go;

    #   find topmost column for the bottommost visible fit
    $visualr = 0;
    $lastr = $matrixdata->{"Rows"} - 1;
    $hremain = $matrixwidget->{"Height"};
    #printf STDERR "bottommostvisiblefit lastr=%d\n", (defined $lastr) ? $lastr : "undef";
    #printf STDERR "bottommostvisiblefit hremain=%d\n", (defined $hremain) ? $hremain : "undef";
    $go = 1;
    $r = -1;
    do {
        if (not $matrixdata->{"RH.$r"}->{"Rowhide"}) {
            my $h = $matrixdata->{"RH.$r"}->{"Height"};
            $realh = $h + $matrixdata->{"RH.$r"}->{"Rowgap"};
            $hremain -= $realh;
            $visualr++ if ($r >= 0 and $hremain >= 0);
        }
        #printf STDERR "r=%2d, realh=%2d, hremain=%2d, visualr=%2d\n", $r, $realh, $hremain, $visualr;
        if ($hremain < 0) {
            $go = 0;
        }
        else {
            $r = $r == -1 ? $lastr : --$r;
            $go = 0 if ($r < 0);
        }
    } while ($go);
    $r++;
    #printf STDERR "bottommostvisiblefit r=%d, visualr=%d\n", $r, $visualr;
    return ($r, $visualr);
}

sub optimalcolwidth {
    my $x;
    my $c;
    my $cols;
    my $lastc;
    my $visualc;

    #   column heading, sizing, counting and cachetag creation
    $cols = $matrixdata->{"Columns"};
    $x = $matrixwidget->{"XOffset"};
    $visualc = 0;
    my $wremain = $matrixwidget->{"Width"};
    my $wweight = 0;
    for ($c = -1; $c < $cols; $c = $c >= 0 ? ++$c : $matrixwidget->{"FirstVisualCol"}) {
        next if ($matrixdata->{"CH.$c"}->{"Colhide"});
        my $w = $matrixdata->{"CH.$c"}->{"Width"};
        my $realw = $w + $matrixdata->{"CH.$c"}->{"Colgap"};
        last if ($wremain < $realw);
        if ($c >= 0) {
            $visualc++;
        }
        $x += $realw;
        $wremain -= $realw;
        if ($matrixdata->{"CH.$c"}->{"Widthmin"} == $matrixdata->{"CH.$c"}->{"Widthmax"}) {
            $matrixdata->{"CH.$c"}->{"Widthweight"} = 0;
        }
        $matrixdata->{"CH.$c"}->{"Widthweight"} = 1 unless (defined $matrixdata->{"CH.$c"}->{"Widthweight"});
        $wweight += $matrixdata->{"CH.$c"}->{"Widthweight"}
    }
    $wweight = 1 if ($wweight == 0); #if no weight was given at all, treat all equal and avoid div by zero
    $lastc = $c;
    if ($wremain >= 1) {
        my $wfraction = $wremain / $wweight;
        my $wfractrem = 0;
        $x = $matrixwidget->{"XOffset"};
        my $wremain = $matrixwidget->{"Width"};
        my $wweight = 0;
        for ($c = -1; $c < $cols; $c = $c >= 0 ? ++$c : $matrixwidget->{"FirstVisualCol"}) {
            next if ($matrixdata->{"CH.$c"}->{"Colhide"});
            my $w = $matrixdata->{"CH.$c"}->{"Width"};
            my $fadditional = $matrixdata->{"CH.$c"}->{"Widthweight"} * $wfraction + $wfractrem;
            my $iadditional = int($fadditional);
            $wfractrem = $fadditional - $iadditional;
            $w += $iadditional;
            my $realw = $w + $matrixdata->{"CH.$c"}->{"Colgap"};
            $matrixdata->{"CH.$c"}->{"Width"} = $w;
            last if ($wremain < $realw);
            if ($c >= 0) {
                $visualc++;
            }
            $x += $realw;
            $wremain -= $realw;
        }
    }
    $matrixwidget->{"NumVisualCol"} = $visualc;
    return;
}

sub optimalrowheight {
    my $y;
    my $r;
    my $rows;
    my $lastr;
    my $visualr;

    #   row heading, sizing, counting and cachetag creation
    $rows = $matrixdata->{"Rows"};
    $y = $matrixwidget->{"YOffset"};
    $visualr = 0;
    my $hremain = $matrixwidget->{"Height"};
    my $hweight = 0;
    for ($r = -1; $r < $rows; $r = $r >= 0 ? ++$r : $matrixwidget->{"FirstVisualRow"}) {
        next if ($matrixdata->{"RH.$r"}->{"Rowhide"});
        my $h = $matrixdata->{"RH.$r"}->{"Height"};
        my $realh = $h + $matrixdata->{"RH.$r"}->{"Rowgap"};
        last if ($hremain < $realh);
        if ($r >= 0) {
            $visualr++;
        }
        $y += $realh;
        $hremain -= $realh;
        if ($matrixdata->{"RH.$r"}->{"Heightmin"} == $matrixdata->{"RH.$r"}->{"Heightmax"}) {
            $matrixdata->{"RH.$r"}->{"Heightweight"} = 0;
        }
        $matrixdata->{"RH.$r"}->{"Heightweight"} = 1 unless (defined $matrixdata->{"RH.$r"}->{"Heightweight"});
        $hweight += $matrixdata->{"RH.$r"}->{"Heightweight"}
    }
    $hweight = 1 if ($hweight == 0); #if no weight was given at all, treat all equal and avoid div by zero
    $lastr = $r;
    if ($hremain >= 1) {
        my $hfraction = $hremain / $hweight;
        my $hfractrem = 0;
        $y = $matrixwidget->{"YOffset"};
        my $hremain = $matrixwidget->{"Height"};
        my $hweight = 0;
        for ($r = -1; $r < $rows; $r = $r >= 0 ? ++$r : $matrixwidget->{"FirstVisualRow"}) {
            next if ($matrixdata->{"RH.$r"}->{"Rowhide"});
            my $h = $matrixdata->{"RH.$r"}->{"Height"};
            my $fadditional = $matrixdata->{"RH.$r"}->{"Heightweight"} * $hfraction + $hfractrem;
            my $iadditional = int($fadditional);
            $hfractrem = $fadditional - $iadditional;
            $h += $iadditional;
            my $realh = $h + $matrixdata->{"RH.$r"}->{"Rowgap"};
            $matrixdata->{"RH.$r"}->{"Height"} = $h;
            last if ($hremain < $realh);
            if ($r >= 0) {
                $visualr++;
            }
            $y += $realh;
            $hremain -= $realh;
        }
    }
    $matrixwidget->{"NumVisualRow"} = $visualr;
    return;
}

sub colheading {
    my ($doit) = @_;
    my $x;
    my $y;
    my $c;
    my $cols;
    my $dataxoffset;
    my $lastc;
    my $visualc;
    my $colcachetag;

    #   column heading, sizing, counting and cachetag creation
    $cols = $matrixdata->{"Columns"};
    $dataxoffset = 0;
    $x = $matrixwidget->{"XOffset"};
    $y = $matrixwidget->{"YOffset"};
    $colcachetag = '';
    $visualc = 0;
    my $wremain = $matrixwidget->{"Width"};
    for ($c = -1; $c < $cols; $c = $c >= 0 ? ++$c : $matrixwidget->{"FirstVisualCol"}) {
        next if ($matrixdata->{"CH.$c"}->{"Colhide"});
        my $w = $matrixdata->{"CH.$c"}->{"Width"};
        my $realw = $w + $matrixdata->{"CH.$c"}->{"Colgap"};
        my $realh = $matrixdata->{"RH.-1"}->{"Heigth"};
        last if ($wremain < $realw);
        if ($c >= 0) {
            $dataxoffset = $x if ($dataxoffset == 0);
            $matrixwidget->{"CH.$visualc"} = $widget->{'container'}->add("CH.$visualc", 'Label',
                -x => $x,
                -y => $y,
                -text => $matrixdata->{"CH.$c"}->{"Labelhide"} ? " " : $matrixdata->{"CH.$c"}->{"Label"},
                -width => $realw,
                -height => $realh
            ) if ($doit);
            $visualc++;
            $colcachetag .= ".$realw";
        }
        $x += $realw;
        $wremain -= $realw;
    }
    $lastc = $c;
    if ($wremain >= 1) {
        $matrixwidget->{"CH.fillright"} = $widget->{'container'}->add("CH.fillright", 'Label',
            -x => $x,
            -y => $y,
            -text => '#'x$wremain,
            -width => $wremain,
            -heigth => 1
        ) if ($doit);
    }
    return ($dataxoffset, $lastc, $colcachetag);
}

sub rowheading {
    my ($doit) = @_;
    my $x;
    my $y;
    my $r;
    my $rows;
    my $datayoffset;
    my $lastr;
    my $visualr;
    my $rowcachetag;

    #   row heading, sizing, counting and cachetag creation
    $rows = $matrixdata->{"Rows"};
    $datayoffset = 0;
    $x = $matrixwidget->{"XOffset"};
    $y = $matrixwidget->{"YOffset"};
    $rowcachetag = '';
    $visualr = 0;
    my $hremain = $matrixwidget->{"Height"};
    for ($r = -1; $r < $rows; $r = $r >= 0 ? ++$r : $matrixwidget->{"FirstVisualRow"}) {
        next if ($matrixdata->{"RH.$r"}->{"Rowhide"});
        my $h = $matrixdata->{"RH.$r"}->{"Height"};
        my $realw = $matrixdata->{"CH.-1"}->{"Width"};
        my $realh = $h + $matrixdata->{"RH.$r"}->{"Rowgap"};
        last if ($hremain < $realh);
        if ($r >= 0) {
            $datayoffset = $y if ($datayoffset == 0);
            $matrixwidget->{"RH.$visualr"} = $widget->{'container'}->add("RH.$visualr", 'Label',
                -x => $x,
                -y => $y,
                -text => $matrixdata->{"RH.$r"}->{"Labelhide"} ? " " : $matrixdata->{"RH.$r"}->{"Label"},
                -width => $realw,
                -height => $realh
            ) if ($doit);
            $visualr++;
            $rowcachetag .= ".$realh";
        }
        $y += $realh;
        $hremain -= $realh;
    }
    $lastr = $r;
    if ($hremain >= 1) {
        $matrixwidget->{"RH.fillbottom"} = $widget->{'container'}->add("RH.fillbottom", 'Label',
            -x => $x,
            -y => $y,
            -text => '#'x$hremain,
            -width => 1,
            -height => $hremain
        ) if($doit);
    }
    return ($datayoffset, $lastr, $rowcachetag);
}

sub updatecolheading {
    my $c;
    my $n;
    my $visualc;

    #   update column heading label
    $visualc = 0;
    $c = $matrixwidget->{"FirstVisualCol"};
    $n = $matrixwidget->{"NumVisualCol"};
    for (; $n > 0; $c++) {
        next if ($matrixdata->{"CH.$c"}->{"Colhide"});
        $n--;
        die "col Baeh!" if (not defined $matrixwidget->{"CH.$visualc"});
        $matrixwidget->{"CH.$visualc"}->{-text} = $matrixdata->{"CH.$c"}->{"Labelhide"} ? " " : $matrixdata->{"CH.$c"}->{"Label"};
        $matrixwidget->{"CH.$visualc"}->draw;
        #$matrixwidget->{"CH.$visualc"}->focus;
        $visualc++;
    }
}

sub updaterowheading {
    my $r;
    my $n;
    my $visualr;

    #   update row heading label
    $visualr = 0;
    $r = $matrixwidget->{"FirstVisualRow"};
    $n = $matrixwidget->{"NumVisualRow"};
    for (; $n > 0; $r++) {
        next if ($matrixdata->{"RH.$r"}->{"Rowhide"});
        $n--;
        die "row Baeh!" if (not defined $matrixwidget->{"RH.$visualr"});
        $matrixwidget->{"RH.$visualr"}->{-text} = $matrixdata->{"RH.$r"}->{"Labelhide"} ? " " : $matrixdata->{"RH.$r"}->{"Label"};
        $matrixwidget->{"RH.$visualr"}->draw;
        #$matrixwidget->{"RH.$visualr"}->focus;
        $visualr++;
    }
}

sub newmatrixwidget {
    my ($dc, $dr, $vc, $vr) = @_;
    $matrixwidget->{"XOffset"}        = 0;    #horizontal position relative to screen
    $matrixwidget->{"YOffset"}        = 0;    #vertical position relative to screen
    $matrixwidget->{"Width"}          = $widget->{'container'}->width  - $matrixwidget->{"XOffset"};
    $matrixwidget->{"Height"}         = $widget->{'container'}->height - $matrixwidget->{"YOffset"};
    $matrixwidget->{"FirstVisualCol"} = $dc || 0; #leftmost data column presented on screen
    $matrixwidget->{"FirstVisualRow"} = $dr || 0; #topmost data row presented on screen
    $matrixwidget->{"Cursorcol"}      = $vc || 0; #cursor column
    $matrixwidget->{"Cursorcol"}      = $vr || 0; #cursor row
    $matrixwidget->{"NumVisualCol"}   = undef; #number of visible column (calculated later)
    $matrixwidget->{"NumVisualRow"}   = undef; #number of visible row (calculated later)
    $matrixwidget->{"Cachetag"}       = undef; #tracks if widgets can be reused (must start undefined)
}

sub layoutmatrixwidget {
    my ($dc, $dr, $vc, $vr) = @_;
    $matrixwidget->{"FirstVisualCol"} = $dc if (defined $dc); #leftmost data column presented on screen
    $matrixwidget->{"FirstVisualRow"} = $dr if (defined $dr); #topmost data row presented on screen
    $matrixwidget->{"Cursorcol"} = $vc if (defined $vc); #cursor column
    $matrixwidget->{"Cursorrow"} = $vr if (defined $vr); #cursor row

    my $c;
    my $r;
    my $x;
    my $y;
    my $lastc;
    my $lastr;
    my $dataxoffset;
    my $datayoffset;
    my $colcachetag;
    my $rowcachetag;
    my $cachetag;
    my $cachehit;

    $cachehit = 0;
    if (defined $matrixwidget->{"Cachetag"}) {
        ($dataxoffset, $lastc, $colcachetag) = &colheading(0);
        ($datayoffset, $lastr, $rowcachetag) = &rowheading(0);
        $cachetag = $colcachetag . '\\' . $rowcachetag;
        if ($cachetag eq $matrixwidget->{"Cachetag"}) {
            $cachehit = 1;
        }
        else {
            &destroymatrixwidgetcells();
        }
    }
    else {
        &optimalcolwidth();
        &optimalrowheight();
    }

    if (not $cachehit) {
        ($dataxoffset, $lastc, $colcachetag) = &colheading(1);
        ($datayoffset, $lastr, $rowcachetag) = &rowheading(1);
        $cachetag = $colcachetag . '\\' . $rowcachetag;
        $matrixwidget->{"Cachetag"} = $cachetag;
    }

    #   now do the matrix content
    my $visualr;
    my $visualc;
    $y = $datayoffset;
    $visualr = 0;
    for ($r = $matrixwidget->{"FirstVisualRow"}; $r < $lastr; ++$r) {
        next if ($matrixdata->{"RH.$r"}->{"Rowhide"});
        my $h = $matrixdata->{"RH.$r"}->{"Height"};
        my $realh = $h + $matrixdata->{"RH.$r"}->{"Rowgap"};

        $x = $dataxoffset;
        $visualc = 0;
        for ($c = $matrixwidget->{"FirstVisualCol"}; $c < $lastc; ++$c) {
            next if ($matrixdata->{"CH.$c"}->{"Colhide"});
            my $w = $matrixdata->{"CH.$c"}->{"Width"};
            my $realw = $w + $matrixdata->{"CH.$c"}->{"Colgap"};

            if (not $cachehit) {
                $matrixwidget->{"VC.$visualc.$visualr"} = $widget->{'container'}->add("VC.$visualc.$visualr", 'TextEntry',
                    -x => $x,
                    -y => $y,
                    -text => (defined $matrixdata->{"CD.$c.$r"}->{"Data"}) ? $matrixdata->{"CD.$c.$r"}->{"Data"} : "<?>",
                    -width => $realw,
                    #FIXME -height => $realh,
                    -maxlength => $matrixdata->{"CH.$c"}->{"Widthmax"}
                    #-maxdown => $matrixdata->{"RH.$r"}->{"Heightmax"}
                    );
                $matrixwidgetvisualcell->{$matrixwidget->{"VC.$visualc.$visualr"}} = [ $visualc, $visualr ];
            }
            else {
                $matrixwidget->{"VC.$visualc.$visualr"}->{-text} = (defined $matrixdata->{"CD.$c.$r"}->{"Data"}) ? $matrixdata->{"CD.$c.$r"}->{"Data"} : "<?>";
            }
            $matrixwidgetdatacell->{$matrixwidget->{"VC.$visualc.$visualr"}} = [ $c, $r ];
            $visualc++;
            $x += $realw;
        }
        $visualr++;
        $y += $realh;
    }
    $matrixwidget->{"NumVisualCol"} = $visualc;
    $matrixwidget->{"NumVisualRow"} = $visualr;

    &updatecolheading();
    &updaterowheading();

    #   underlines on primary sort key
    my $ulc = $matrixdata->{"Sortorder"}->[0];
    $dr = $matrixwidget->{"FirstVisualRow"};
    my $data;
    my $next = $matrixdata->{"CD.$ulc.$dr"}->{"Data"};
    for ($r = 0; $r < $visualr; $r++) {
        $data = $next;
        $dr++;
        $next = $matrixdata->{"CD.$ulc.$dr"}->{"Data"};
        if (not defined $next or $data eq $next) {
            for ($c = 0; $c < $visualc; $c++) {
                $matrixwidget->{"VC.$c.$r"}->{-canvasscr}->attroff(A_UNDERLINE);
            }
        }
        else {
            for ($c = 0; $c < $visualc; $c++) {
                $matrixwidget->{"VC.$c.$r"}->{-canvasscr}->attron(A_UNDERLINE);
            }
        }
    }

    #   key bindings and event handling
    if (not $cachehit) {
        for ($c = 0; $c < $visualc; $c++) {
            for ($r = 0; $r < $visualr; $r++) {
                $matrixwidget->{"VC.$c.$r"}->set_binding(\&tabbinder, CUI_TAB(), KEY_ENTER(), KEY_F(2), CUI_ESCAPE());
                $matrixwidget->{"VC.$c.$r"}->set_event("-onfocus",
                    sub {
                        my $this = shift;
                        my ($vc, $vr) = @{$matrixwidgetvisualcell->{$this}};
                        my ($dc, $dr) = @{$matrixwidgetdatacell->{$this}};
                        $matrixwidget->{"VC.$vc.$vr"}->{-canvasscr}->attron(A_STANDOUT) unless($editcell);
                        if (defined $matrixdata->{"CD.$dc.$dr"}->{"Focuscallback"}) {
                            &{$matrixdata->{"CD.$dc.$dr"}->{"Focuscallback"}}($matrixwidget, $dc, $dr, $vc, $vr);
                        }
                    }
                );
                $matrixwidget->{"VC.$c.$r"}->set_event("-onblur",
                    sub {
                        my $this = shift;
                        my ($vc, $vr) = @{$matrixwidgetvisualcell->{$this}};
                        my ($dc, $dr) = @{$matrixwidgetdatacell->{$this}};
                        $matrixwidget->{"VC.$vc.$vr"}->{-canvasscr}->attroff(A_STANDOUT);
                        if ($editcell) {
                            if (defined $matrixdata->{"CD.$dc.$dr"}->{"Blurcallback"}) {
                                $datacoll = &{$matrixdata->{"CD.$dc.$dr"}->{"Blurcallback"}}($matrixwidget, $dc, $dr, $vc, $vr);
                            }
                        }
                        $editcell = 0;
                    }
                );
            }
        }
    }
}

sub tabbinder {
    my $this = shift;
    my $key = shift;
    $key = "\cD" if ($key eq KEY_DC()); #make CTRL-D a normal key

    my $nvc = $matrixwidget->{"NumVisualCol"} - 1;
    my $nvr = $matrixwidget->{"NumVisualRow"} - 1;

    if ((not $editcell) or (not defined $matrixwidgetvisualcell->{$this})) {
        return 'DELEGATE';
    }

    my ($vc, $vr) = @{$matrixwidgetvisualcell->{$this}};
    my ($dc, $dr) = @{$matrixwidgetdatacell->{$this}};

    if    ($editcell and $key eq CUI_TAB()) {
        my $datacollabel = $matrixdata->{"CH.$dc"}->{"Keyname"};
        if ($datacollabel =~ m/^(date)$/) {
            $this->{-text} = &{$matrixdata->{"completedate"}}($matrixwidget, $dc, $dr, $vc, $vr, $this->{-text});
            $this->{-pos} = length($this->{-text});
        }
        elsif ($datacollabel =~ m/^(begin|end|amount)$/) {
            $this->{-text} = &{$matrixdata->{"completetime"}}($matrixwidget, $dc, $dr, $vc, $vr, $this->{-text});
            $this->{-pos} = length($this->{-text});
        }
        elsif ($datacollabel eq "account") {
            my $accounts = [];
            my $values = [];
            my $labels = {};

            if (defined $matrixdata->{"completeaccount"}) {
                my ($vc, $vr) = @{$matrixwidgetvisualcell->{$this}};
                my ($dc, $dr) = @{$matrixwidgetdatacell->{$this}};
                $accounts = &{$matrixdata->{"completeaccount"}}($matrixwidget, $dc, $dr, $vc, $vr, $this->{-text});
            }

            my $count = @{$accounts};
            if ($count == 0) {
                #FIXME error
            }
            elsif ($count == 1) {
                if (defined $matrixdata->{"CD.$dc.$dr"}->{"Blurcallback"}) {
                    $matrixwidget->{"VC.$vc.$vr"}->{-text} = $accounts->[0];
                    &{$matrixdata->{"CD.$dc.$dr"}->{"Blurcallback"}}($matrixwidget, $dc, $dr, $vc, $vr);
                }
                $editcell = 0;
                $datacoll = 1;
            }
            else {
                my $i = 0;
                foreach my $a (@{$accounts}) {
                    $values->[$i] = $i;
                    $labels->{$i} = $a;
                    $i++;
                }

                my $w = $this->parent->add("accountselection", 'Listbox',
                    -x          => $matrixwidget->{"XOffset"},
                    -y          => $matrixwidget->{"YOffset"},
                    -values     => $values,
                    -labels     => $labels,
                    -width      => $matrixwidget->{"Width"},
                    -height     => $matrixwidget->{"Height"},
                    -border     => 1,
                    #-multi      => 1,
                    -title      => $matrixdata->{"CH.$dc"}->{"Label"},
                    -vscrollbar => 1,
                    #-onchange   => \&listbox_callback,
                );
                $escapeit = 0;
                $w->set_binding(
                    sub {
                        my $this = shift;
                        my $key = shift;
                        $escapeit = 1 if ($key eq CUI_ESCAPE());
                        $this->{-has_modal_focus} = 0;
                    },
                    KEY_ENTER(), CUI_ESCAPE());
                $w->draw;
                $w->modalfocus;
                $this->parent->delete("accountselection");
                $this->parent->draw();
                if ($escapeit) {
                    $editcell = 1;
                    $datacoll = 0;
                }
                else {
                    if (defined $w->{-selected}) {
                        $matrixwidget->{"VC.$vc.$vr"}->{-text} = $accounts->[$w->{-selected}];
                        $editcell = 0;
                        $datacoll = 1;
                    }
                    else {
                        $editcell = 1;
                        $datacoll = 0;
                    }
                    if (defined $matrixdata->{"CD.$dc.$dr"}->{"Blurcallback"}) {
                        &{$matrixdata->{"CD.$dc.$dr"}->{"Blurcallback"}}($matrixwidget, $dc, $dr, $vc, $vr);
                    }
                }
            }
        }
    }
    elsif ($key eq KEY_F(2) or $key eq KEY_ENTER()) {
        if ($editcell) {
            if (defined $matrixdata->{"CD.$dc.$dr"}->{"Blurcallback"}) {
                $datacoll = &{$matrixdata->{"CD.$dc.$dr"}->{"Blurcallback"}}($matrixwidget, $dc, $dr, $vc, $vr);
            }
        }
        $matrixwidget->{"VC.$vc.$vr"}->{-canvasscr}->attron(A_STANDOUT);
        $matrixwidget->{"VC.$vc.$vr"}->draw;
        $editcell = 0;
        return;
    }
    elsif ($key eq CUI_ESCAPE()) {
        $matrixwidget->{"VC.$vc.$vr"}->{-text} = $matrixdata->{"CD.$dc.$dr"}->{"Data"};
        $matrixwidget->{"VC.$vc.$vr"}->{-canvasscr}->attron(A_STANDOUT);
        $matrixwidget->{"VC.$vc.$vr"}->draw;
        $editcell = 0;
        return;
    }
    else {
        return 'DELEGATE';
    }
}

sub keybinder {
    my $this = shift;
    my $key = shift;
    $key = "\cD" if ($key eq KEY_DC()); #make CTRL-D a normal key

    my $nvc = $matrixwidget->{"NumVisualCol"} - 1;
    my $nvr = $matrixwidget->{"NumVisualRow"} - 1;

    if ($editcell) {
        return 'DELEGATE';
    }

    #   special handling when table is empty (dummy is focused)
    if (not defined $matrixwidgetvisualcell->{$this->getfocusobj}) {
        if    ($key eq "o") {
            if (defined $matrixdata->{"onanewline"}) {
                &{$matrixdata->{"onanewline"}}($matrixwidget, 0, 0, 0, 0);
                $datacoll = 1;
            }
            $this->root->feedkey(KEY_F(2)); #enter edit mode after datacoll was handled
        }
        elsif ($key eq "p" and defined $matrixdata->{"paste"} and $matrixdata->{"clipboardtype"} eq "line") {
            if (defined $matrixdata->{"paste"}) {
                &{$matrixdata->{"paste"}}($matrixwidget, 0, 0, 0, 0);
                $datacoll = 1;
            }
        }
        if ($datacoll) {
            #$matrixwidget->{"Cursorcol"} = 0;
            #$matrixwidget->{"Cursorrow"} = 0;
            return;
        }
        if ($key eq "s") {
            &statusline("Save data and continue editing");
            $breakout = 1;
            return;
        }
        elsif ($key eq "q") {
            $breakout = 2 if (&commonquit());
            return;
        }
        elsif ($key eq "x" or $key eq "Z") {
            $breakout = 3;
            return;
        }
        elsif ($key eq KEY_F(1)) {
            my $text = `man as-cui 2>/dev/null` || "Cannot launch man or man page for as-cui not found";
            my $w = $this->parent->add("help", 'TextViewer',
                -x          => $matrixwidget->{"XOffset"},
                -y          => $matrixwidget->{"YOffset"},
                -width      => $matrixwidget->{"Width"},
                -height     => $matrixwidget->{"Height"},
                -border     => 1,
                -title      => "Help",
                -vscrollbar => 1,
                -text       => $text
            );
            $w->set_binding(
                sub {
                    my $this = shift;
                    my $key = shift;
                    $this->{-has_modal_focus} = 0;
                },
                KEY_ENTER(), CUI_ESCAPE());
            $w->draw;
            $w->modalfocus;
            $this->parent->delete("help");
            $datacoll = 1;
        }
        return 'DELEGATE';
    }

    my ($vc, $vr) = @{$matrixwidgetvisualcell->{$this->getfocusobj}};
    my ($dc, $dr) = @{$matrixwidgetdatacell->{$this->getfocusobj}};

    if    ($key eq CUI_ESCAPE()) {
        $matrixwidget->{"VC.$vc.$vr"}->{-canvasscr}->attroff(A_STANDOUT);
        $matrixwidget->{"VC.$vc.$vr"}->draw;
        $this->root->focus('menu');
        return;
    }
    elsif ($key eq "s") {
        $matrixwidget->{"VC.$vc.$vr"}->{-canvasscr}->attroff(A_STANDOUT);
        $matrixwidget->{"VC.$vc.$vr"}->draw;
        $widget->{"dummy"}->focus;
        &statusline("Save data and continue editing");
        $breakout = 1;
        return;
    }
    elsif ($key eq "q") {
        $matrixwidget->{"VC.$vc.$vr"}->{-canvasscr}->attroff(A_STANDOUT);
        $matrixwidget->{"VC.$vc.$vr"}->draw;
        if (&commonquit()) {
            $breakout = 2
        }
        else {
            $datacoll = 1;
        }
        return;
    }
    elsif ($key eq "x" or $key eq "Z") {
        $matrixwidget->{"VC.$vc.$vr"}->{-canvasscr}->attroff(A_STANDOUT);
        $matrixwidget->{"VC.$vc.$vr"}->draw;
        $widget->{"dummy"}->focus;
        $breakout = 3;
        return;
    }
    elsif ($key eq KEY_UP() or $key eq "k") {
        $vr--;
        if ($vr < 0) {
            $vrscroll = -1;
            $vr = 0;
        }
    }
    elsif ($key eq KEY_PPAGE()) {
        my $j;
        $j = int($matrixwidget->{"NumVisualRow"} / 3 * 1);
        $j = 10 if ( $j < 10 );
        $vr-=$j;
        if ($vr < 0) {
            $vrscroll = $vr;
            $vr = 0;
        }
    }
    elsif ($key eq KEY_DOWN() or $key eq "j") {
        $vr++;
        if ($vr > $nvr) {
            $vrscroll = 1;
            $vr = $nvr;
        }
    }
    elsif ($key eq KEY_NPAGE()) {
        my $j;
        $j = int($matrixwidget->{"NumVisualRow"} / 3 * 1);
        $j = 10 if ( $j < 10);
        $vr+=$j;
        if ($vr > $nvr) {
            $vrscroll = $vr - $nvr;
            $vr = $nvr;
        }
    }
    elsif ($key eq KEY_LEFT() or $key eq "h") {
        $vc--;
        if ($vc < 0) {
            $vcscroll = -1;
            $vc = 0;
        }
    }
    elsif ($key eq KEY_RIGHT() or $key eq "l") {
        $vc++;
        if ($vc > $nvc) {
            $vcscroll = 1;
            $vc = $nvc
        }
    }
    elsif ($key eq KEY_HOME()) {
        $vr = 0;
        $vc = 0;
    }
    elsif ($key eq KEY_END()) {
        $vr = $nvr;
        $vc = $nvc;
    }
    elsif ($key eq "0") {
        my $newdc = undef;
        my $c;
        #   search jump destination begin of columns to current cell
        for ($c = 0; $c < $matrixdata->{"Columns"}; $c++) {
            if ($matrixdata->{"CH.$c"}->{"Coljump"} and not $matrixdata->{"CH.$c"}->{"Colhide"}) {
                $newdc = $c;
                last;
            }
        }
        return unless(defined $newdc); #do not change focus if no jump destination found
        my $deltadc = $newdc - $dc;
        my $stepdir = $deltadc < 0 ? -1 : +1;
        my $deltavc = 0;
        for ($c = $dc; $c != $newdc; $c += $stepdir) {
            $deltavc += $stepdir if (not $matrixdata->{"CH.$c"}->{"Colhide"});
        }
        $vc += $deltavc;
        if ($vc < 0) {
            $vcscroll = $vc;
            $vc = 0;
        }
        if ($vc > 0) {
            $vcscroll = $vc;
            $vc = 0;
        }
    }
    elsif ($key eq "^") {
        $vc = 0;
        $vcpos = -1;
    }
    elsif ($key eq "\$") {
        $vc = $nvc;
        $vcpos = +1;
    }
    elsif ($key eq "1") {
        $vr = 0;
        $vrpos = -1;
    }
    elsif ($key eq "G") {
        $vr = $nvr;
        $vrpos = +1;
    }
    elsif ($key eq "\cU") {
        my $j;
        $j = int($matrixwidget->{"NumVisualRow"} / 3 * 1);
        $j = 10 if ( $j < 10 );
        $vrscroll = -$j;
    }
    elsif ($key eq "\cD") {
        my $j;
        $j = int($matrixwidget->{"NumVisualRow"} / 3 * 1);
        $j = 10 if ( $j < 10 );
        $vrscroll = +$j;
    }
    elsif ($key eq "\cB") {
        $vrscroll = -$matrixwidget->{"NumVisualRow"};
    }
    elsif ($key eq "\cF") {
        $vrscroll = +$matrixwidget->{"NumVisualRow"};
    }
    elsif ($key eq "w" or $key eq CUI_TAB()) {
        my $newdc = undef;
        my $c;
        unless (defined $newdc) {
            #   search jump destination from next cell to the end of the columns
            for ($c = $dc + 1; $c < $matrixdata->{"Columns"}; $c++) {
                if ($matrixdata->{"CH.$c"}->{"Coljump"} and not $matrixdata->{"CH.$c"}->{"Colhide"}) {
                    $newdc = $c;
                    last;
                }
            }
        }
        unless (defined $newdc) {
            $vr++;
            if ($vr > $nvr) {
                $vrscroll = 1;
                $vr = $nvr;
            }
            #   search jump destination begin of columns to current cell
            for ($c = 0; $c < $dc; $c++) {
                if ($matrixdata->{"CH.$c"}->{"Coljump"} and not $matrixdata->{"CH.$c"}->{"Colhide"}) {
                    $newdc = $c;
                    last;
                }
            }
        }
        return unless(defined $newdc); #do not change focus if no jump destination found
        my $deltadc = $newdc - $dc;
        my $stepdir = $deltadc < 0 ? -1 : +1;
        my $deltavc = 0;
        for ($c = $dc; $c != $newdc; $c += $stepdir) {
            $deltavc += $stepdir if (not $matrixdata->{"CH.$c"}->{"Colhide"});
        }
        $vc += $deltavc;
        if ($vc < 0) {
            $vcscroll = $vc;
            $vc = 0;
        }
        if ($vc > $nvc) {
            $vcscroll = $vc;
            $vc = $nvc
        }
    }
    elsif ($key eq "b" or $key eq KEY_BTAB()) {
        my $newdc = undef;
        my $c;
        unless (defined $newdc) {
            #   search jump destination from previous cell to the begin of the columns
            for ($c = $dc - 1; $c >= 0; $c--) {
                if ($matrixdata->{"CH.$c"}->{"Coljump"} and not $matrixdata->{"CH.$c"}->{"Colhide"}) {
                    $newdc = $c;
                    last;
                }
            }
        }
        unless (defined $newdc) {
            $vr--;
            if ($vr < 0) {
                $vrscroll = -1;
                $vr = 0;
            }
            #   search jump destination end of columns to current cell
            for ($c = $matrixdata->{"Columns"}; $c > $dc; $c--) {
                if ($matrixdata->{"CH.$c"}->{"Coljump"} and not $matrixdata->{"CH.$c"}->{"Colhide"}) {
                    $newdc = $c;
                    last;
                }
            }
        }
        return unless(defined $newdc); #do not change focus if no jump destination found
        my $deltadc = $newdc - $dc;
        my $stepdir = $deltadc < 0 ? -1 : +1;
        my $deltavc = 0;
        for ($c = $dc; $c != $newdc; $c += $stepdir) {
            $deltavc += $stepdir if (not $matrixdata->{"CH.$c"}->{"Colhide"});
        }
        $vc += $deltavc;
        if ($vc < 0) {
            $vcscroll = $vc;
            $vc = 0;
        }
        if ($vc > $nvc) {
            $vcscroll = $vc;
            $vc = $nvc
        }
    }
    elsif ($key eq KEY_F(1)) {
        my $text = `man as-cui 2>/dev/null` || "Cannot launch man or man page for as-cui not found";
        my $w = $this->parent->add("help", 'TextViewer',
            -x          => $matrixwidget->{"XOffset"},
            -y          => $matrixwidget->{"YOffset"},
            -width      => $matrixwidget->{"Width"},
            -height     => $matrixwidget->{"Height"},
            -border     => 1,
            -title      => "Help",
            -vscrollbar => 1,
            -text       => $text
        );
        $w->set_binding(
            sub {
                my $this = shift;
                my $key = shift;
                $this->{-has_modal_focus} = 0;
            },
            KEY_ENTER(), CUI_ESCAPE());
        $w->draw;
        $w->modalfocus;
        $this->parent->delete("help");
        $datacoll = 1;
    }
    elsif (($key eq CUI_SPACE()) and $matrixdata->{"CH.$dc"}->{"Coleditable"}) {
        $matrixwidget->{"VC.$vc.$vr"}->{-text} = '';
        $matrixwidget->{"VC.$vc.$vr"}->{-pos} = 0;
        $matrixwidget->{"VC.$vc.$vr"}->{-canvasscr}->attroff(A_STANDOUT);
        $matrixwidget->{"VC.$vc.$vr"}->draw;
        $editcell = 1;
        return; #do not change focus
    }
    elsif (($key eq "i" or $key eq KEY_F(2) or $key eq KEY_ENTER()) and $matrixdata->{"CH.$dc"}->{"Coleditable"}) {
        $matrixwidget->{"VC.$vc.$vr"}->{-canvasscr}->attroff(A_STANDOUT);
        $matrixwidget->{"VC.$vc.$vr"}->draw;
        $editcell = 1;
        return; #do not change focus
    }
    elsif (($key eq "A") and $matrixdata->{"CH.$dc"}->{"Coleditable"}) {
        my $w = $matrixdata->{"CH.$dc"}->{"Widthmax"};
        my $p = length($matrixwidget->{"VC.$vc.$vr"}->{-text});
        $p = $w if ($p > $w);
        $matrixwidget->{"VC.$vc.$vr"}->{-pos} = $p;
        $matrixwidget->{"VC.$vc.$vr"}->{-canvasscr}->attroff(A_STANDOUT);
        $matrixwidget->{"VC.$vc.$vr"}->draw;
        $editcell = 1;
        return; #do not change focus
    }
    elsif ($key eq "o") {
        if (defined $matrixdata->{"onanewline"}) {
            &{$matrixdata->{"onanewline"}}($matrixwidget, $dc, $dr, $vc, $vr);
            $datacoll = 1;
        }
        $this->root->feedkey(KEY_F(2)); #enter edit mode after datacoll was handled
        # implicit immediate KEY_DOWN
        &layoutmatrixwidget();
        $datacoll = 0;
        &drawfocusmatrixwidget();
        $nvr = $matrixwidget->{"NumVisualRow"} - 1;
        $vr++;
        if ($vr > $nvr) {
            $vrscroll = 1;
            $vr = $nvr;
        }
    }
    elsif ($key eq "O") {
        if (defined $matrixdata->{"Onanewline"}) {
            &{$matrixdata->{"Onanewline"}}($matrixwidget, $dc, $dr, $vc, $vr);
            $datacoll = 1;
        }
        $this->root->feedkey(KEY_F(2)); #enter edit mode after datacoll was handled
        return; #do not change focus
    }
    elsif ($key eq "d") {
        if (defined $matrixdata->{"deletedelete"}) {
            if (&{$matrixdata->{"deletedelete"}}($matrixwidget, $dc, $dr, $vc, $vr)) {
                # implicit immediate KEY_UP when trailing line was deleted
                $vr--;
                if ($vr < 0) {
                    $vrscroll = -1;
                    $vr = 0;
                }
            }
            $datacoll = 1;
        }
        return; #do not change focus
    }
    elsif ($key eq "u") {
        if (defined $matrixdata->{"undo"}) {
            return unless &{$matrixdata->{"undo"}}($matrixwidget, $dc, $dr, $vc, $vr);
            $datacoll = 1;
        }
    }
    elsif ($key eq KEY_F(6)) {
        my $c;
        my $values = [];
        my $labels = {};
        my $select = {};
        for ($c = 0; $c < $matrixdata->{"Columns"}; $c++) {
            push @{$values}, "$c";
            $labels->{"$c"} = $matrixdata->{"CH.$c"}->{"Label"};
            $select->{"$c"} = $matrixdata->{"CH.$c"}->{"Coljump"} ? 1 : 0;
        }
        my $w = $this->add("coljump", 'Listbox',
            -x          => $matrixwidget->{"XOffset"},
            -y          => $matrixwidget->{"YOffset"},
            -values     => $values,
            -labels     => $labels,
            -selected   => $select,
            -width      => 20,
            -height     => 20,
            -border     => 1,
            -multi      => 1,
            -title      => 'Jump',
            -vscrollbar => 1,
            #-onchange   => \&listbox_callback,
        );
        $escapeit = 0;
        $w->set_binding(
            sub {
                my $this = shift;
                my $key = shift;
                $escapeit = 1 if ($key eq CUI_ESCAPE());
                $this->{-has_modal_focus} = 0;
            },
            KEY_ENTER(), CUI_ESCAPE());
        $w->draw;
        $w->modalfocus;
        $this->delete("coljump");
        unless ($escapeit) {
            for ($c = 0; $c < $matrixdata->{"Columns"}; $c++) {
                $matrixdata->{"CH.$c"}->{"Coljump"} = $select->{"$c"} ? 1 : 0;
            }
        }
        $this->root->focus(undef, 1);
        $datacoll = 1;
        return; #do not change focus
    }
    elsif ($key eq KEY_F(7)) {
        my $c;
        my $values = [];
        my $labels = {};
        my $select = {};
        for ($c = 0; $c < $matrixdata->{"Columns"}; $c++) {
            push @{$values}, "$c";
            $labels->{"$c"} = $matrixdata->{"CH.$c"}->{"Label"};
            $select->{"$c"} = $matrixdata->{"CH.$c"}->{"Colhide"} ? 0 : 1;
        }
        my $w = $this->add("hideandseek", 'Listbox',
            -x          => $matrixwidget->{"XOffset"},
            -y          => $matrixwidget->{"YOffset"},
            -values     => $values,
            -labels     => $labels,
            -selected   => $select,
            -width      => 20,
            -height     => 20,
            -border     => 1,
            -multi      => 1,
            -title      => 'Show',
            -vscrollbar => 1,
            #-onchange   => \&listbox_callback,
        );
        $escapeit = 0;
        $w->set_binding(
            sub {
                my $this = shift;
                my $key = shift;
                $escapeit = 1 if ($key eq CUI_ESCAPE());
                $this->{-has_modal_focus} = 0;
            },
            KEY_ENTER(), CUI_ESCAPE());
        $w->draw;
        $w->modalfocus;
        $this->delete("hideandseek");
        unless ($escapeit) {
            for ($c = 0; $c < $matrixdata->{"Columns"}; $c++) {
                $matrixdata->{"CH.$c"}->{"Colhide"} = $select->{"$c"} ? 0 : 1;
            }
        }
        $this->root->focus(undef, 1);
        $datacoll = 1;
        return; #do not change focus
    }
    elsif ($key eq KEY_F(8)) {
        my $c;
        my $i;
        my $values = [];
        my $labels = {};
        my $select = {};
        my $trackc = {};

        $i = 0;
        foreach $c (@{$matrixdata->{"Sortorder"}}) {
            $values->[$i] = "$c";
            $labels->{$c} = $matrixdata->{"CH.$c"}->{"Label"};
            $select->{$i} = 1;
            $trackc->{$c} = 1;
            $i++;
        }
        for ($c = 0; $c < $matrixdata->{"Columns"}; $c++) {
            if (not $trackc->{$c}) {
                $values->[$i] = "$c";
                $labels->{$c} = $matrixdata->{"CH.$c"}->{"Label"};
                $select->{$i} = 0;
                $i++;
            }
        }
        my $w = $this->add("sortselection", 'Listbox',
            -x          => $matrixwidget->{"XOffset"},
            -y          => $matrixwidget->{"YOffset"},
            -values     => $values,
            -selected   => $select,
            -labels     => $labels,
            -width      => 20,
            -height     => 20,
            -border     => 1,
            -multi      => 1,
            -title      => 'Sort',
            -vscrollbar => 1,
            #-onchange   => \&listbox_callback,
        );
        $w->set_binding(
            sub {
                my $this = shift;
                my $key = shift;
                $escapeit = 1 if ($key eq CUI_ESCAPE());
                $this->{-has_modal_focus} = 0;
            },
            KEY_ENTER(), CUI_ESCAPE());
        $w->draw;
        $w->modalfocus;
        $this->delete("sortselection");
        unless ($escapeit) {
            $matrixdata->{"Sortorder"} = [];
            for ($i = 0; $i <= $#{$values}; $i++) {
                $c = $w->{-values}->[$i];
                #printf STDERR "$c (at index $i) = $w->{-selected}->{$i}\n";
                push @{$matrixdata->{"Sortorder"}}, $c if ($w->{-selected}->{$i});
            }
        }
        $this->root->focus(undef, 1);
        if (defined $matrixdata->{"sort"}) {
            &{$matrixdata->{"sort"}}($matrixwidget, $dc, $dr, $vc, $vr);
            $datacoll = 1;
        }
        return; #do not change focus
    }
    elsif ($key eq "y") {
        if (defined $matrixdata->{"yankcell"}) {
            &{$matrixdata->{"yankcell"}}($matrixwidget, $dc, $dr, $vc, $vr);
            $datacoll = 1;
        }
    }
    elsif ($key eq "Y") {
        if (defined $matrixdata->{"Yankline"}) {
            &{$matrixdata->{"Yankline"}}($matrixwidget, $dc, $dr, $vc, $vr);
            $datacoll = 1;
        }
    }
    elsif ($key eq "p") {
        if (defined $matrixdata->{"paste"} and $matrixdata->{"clipboardtype"} eq "cell") {
            return unless &{$matrixdata->{"paste"}}($matrixwidget, $dc, $dr, $vc, $vr);
            $datacoll = 1;
        }
        elsif (defined $matrixdata->{"paste"} and $matrixdata->{"clipboardtype"} eq "line") {
            return unless &{$matrixdata->{"paste"}}($matrixwidget, $dc, $dr, $vc, $vr);
            $datacoll = 1;
            $vr++;
            if ($vr > $nvr) {
                $vrscroll = 1;
                $vr = $nvr;
            }
        }
    }
    elsif ($key eq "P") {
        if (defined $matrixdata->{"Paste"}) {
            return unless &{$matrixdata->{"Paste"}}($matrixwidget, $dc, $dr, $vc, $vr);
            $datacoll = 1;
        }
        #$this->root->feedkey(KEY_F(2)); #enter edit mode after datacoll was handled
        return; #do not change focus
    }
    elsif ($key eq ".") {
        if (defined $matrixdata->{"currentdatetime"}) {
            return unless &{$matrixdata->{"currentdatetime"}}($matrixwidget, $dc, $dr, $vc, $vr);
            $datacoll = 1;
        }
    }
    elsif ($key eq "=") {
        if (defined $matrixdata->{"recalculatetime"}) {
            return unless &{$matrixdata->{"recalculatetime"}}($matrixwidget, $dc, $dr, $vc, $vr);
            $datacoll = 1;
        }
    }
    elsif ($key eq "~") {
        if (defined $matrixdata->{"roundtime"}) {
            return unless &{$matrixdata->{"roundtime"}}($matrixwidget, $dc, $dr, $vc, $vr);
            $datacoll = 1;
        }
    }
    elsif ($key eq "+") {
        if (defined $matrixdata->{"incdatetime"}) {
            return unless &{$matrixdata->{"incdatetime"}}($matrixwidget, $dc, $dr, $vc, $vr);
            $datacoll = 1;
        }
    }
    elsif ($key eq "-") {
        if (defined $matrixdata->{"decdatetime"}) {
            return unless &{$matrixdata->{"decdatetime"}}($matrixwidget, $dc, $dr, $vc, $vr);
            $datacoll = 1;
        }
    }
    elsif ($key eq ":") {
        if (defined $matrixdata->{"copycellfromprevrow"}) {
            return unless &{$matrixdata->{"copycellfromprevrow"}}($matrixwidget, $dc, $dr, $vc, $vr);
            $datacoll = 1;
        }
    }
    elsif ($key eq ";") {
        if (defined $matrixdata->{"copycellfromsuccrow"}) {
            return unless &{$matrixdata->{"copycellfromsuccrow"}}($matrixwidget, $dc, $dr, $vc, $vr);
            $datacoll = 1;
        }
    }
    elsif ($key eq KEY_F(9)) {
        my $width = $matrixdata->{"CH.$dc"}->{"Width"};
        if ($width != $matrixdata->{"CH.$dc"}->{"Widthmin"}) {
            $matrixdata->{"CH.$dc"}->{"Width"} = $matrixdata->{"CH.$dc"}->{"Widthmin"};
            $datacoll = 1;
        }
        return; #do not change focus
    }
    elsif ($key eq KEY_F(10)) {
        my $width = $matrixdata->{"CH.$dc"}->{"Width"};
        $width--;
        if ($width >= $matrixdata->{"CH.$dc"}->{"Widthmin"}) {
            $matrixdata->{"CH.$dc"}->{"Width"} = $width;
            $datacoll = 1;
        }
        return; #do not change focus
    }
    elsif ($key eq KEY_F(11)) {
        my $width = $matrixdata->{"CH.$dc"}->{"Width"};
        $width++;
        if ($width <= $matrixdata->{"CH.$dc"}->{"Widthmax"}) {
            $matrixdata->{"CH.$dc"}->{"Width"} = $width;
            $datacoll = 1;
        }
        return; #do not change focus
    }
    elsif ($key eq KEY_F(12)) {
        my $width = $matrixdata->{"CH.$dc"}->{"Width"};
        if ($width != $matrixdata->{"CH.$dc"}->{"Widthmax"}) {
            $matrixdata->{"CH.$dc"}->{"Width"} = $matrixdata->{"CH.$dc"}->{"Widthmax"};
            $datacoll = 1;
        }
        return; #do not change focus
    }
    else {
        $this->dobeep;
    }
    $matrixwidget->{"Cursorcol"} = $vc;
    $matrixwidget->{"Cursorrow"} = $vr;
    if ($vcscroll == 0 && $vrscroll == 0) {
        $matrixwidget->{"VC.$vc.$vr"}->focus;
    }
    else {
        $widget->{"dummy"}->focus;
    }
}

sub commonquit() {
    my $rc = 0;
    if (    not $matrixdata->{"dirty"} 
         or $cui->dialog(-message => " Quit without saving? \n" .
                                     " Changes will be lost ",
                         -selected => 1,
                         -buttons => [
                                          { -label => "[ OK ]",     -value => 1 },
                                          { -label => "[ Cancel ]", -value => 0 }
                                     ])
           ) {
        $rc = 1;
    }
    $widget->{"dummy"}->focus;
    return $rc;
}

sub destroymatrixwidgetcells {
    foreach my $i (sort keys %{$matrixwidget}) {
        if ($i =~ m/^VC\.\d+\.\d+/ || $i =~ m/^(CH|RH)\.(\d+|fill.*)/) {
            $matrixwidget->{$i}->parent->delete($i);
            delete $matrixwidget->{$i};
        }
    }
}

sub destroymatrixwidget {
    &destroymatrixwidgetcells();
    undef $matrixwidget;
}

sub drawallmatrixwidgets {
    $widget->{'container'}->draw; return;
    foreach my $i (sort keys %{$matrixwidget}) {
        if ($i =~ m/^VC\.\d+\.\d+/ || $i =~ m/^(CH|RH)\.(\d+|fill.*)/) {
            #printf STDERR "DRAW: $i\n";
            $matrixwidget->{$i}->draw;
        }
    }
}

sub drawfocusmatrixwidget {
    my $c;
    my $r;
    my $lastc;
    my $lastr;

    &drawallmatrixwidgets();
    $c = defined $matrixwidget->{"Cursorcol"} ? $matrixwidget->{"Cursorcol"} : 0;
    $r = defined $matrixwidget->{"Cursorrow"} ? $matrixwidget->{"Cursorrow"} : 0;
    $lastc = $matrixwidget->{"NumVisualCol"} - 1; #Col0=heading, Col1...n=cursor area
    $lastr = $matrixwidget->{"NumVisualRow"} - 1; #Row0=heading, Row1...n=cursor area
    $widget->{"dummy"}->focus;
    if ($lastc >= 0 and $lastr >= 0) {
        $c = $lastc if ($c > $lastc);
        $r = $lastr if ($r > $lastr);
        $matrixwidget->{"VC.$c.$r"}->{-canvasscr}->attron(A_STANDOUT);
        $matrixwidget->{"VC.$c.$r"}->draw;
        $matrixwidget->{"VC.$c.$r"}->focus;
    }
}

sub statusline() {
    my ($textl, $textr) = (@_);
    my $wstat = $widget->{'stat'}->width;
    if (not defined $textl or $textl eq "") {
        $textl = "";
        $textl .= "Accounting System (AS), " . $matrixdata->{"progname"} . " " . $matrixdata->{"progvers"} . "";
        $textl .= " [+]" if ($matrixdata->{"dirty"});
    }
    if (not defined $textr or $textr eq "") {
        $textr .= "RET:Enter/Leave s:Save q:Quit ESC:Menu F1:Help";
    }
    my $textf = " " x ($wstat -2 -length($textl) -1 -1 -length($textr) - 2);
    my $text  = sprintf("  %s %s %s  ", $textl, $textf, $textr);
    $widget->{'info'}->{-text} = $text;
    $widget->{'info'}->draw;
    $widget->{'dummy'}->draw if (defined $widget->{'dummy'}); # cursor stay away!
}

sub ascuinew {
     ($matrixdata, $verbose) = (@_);

    #   create the root object
    $Curses::UI::ncurses_mouse = 0;
    $cui = new Curses::UI (
        -clear_on_exit => 0,
        -mouse_support => 0,
    #    -debug => $debug,
    );
    $cui->overlapping(0); #activates rse performance enhancement patch for non-overlapping layouts

    #   die handler
    $SIG{__DIE__} = sub {
        my ($err) = @_;
        $err =~ s|\s+at\s+.*||s if (not $verbose);
        my $txt = "$err ". ($! ? "($!)" : "");
        ($verbose || open STDERR, ">>2.log") || (open STDERR, ">>$ENV{HOME}/2.log") || (open STDERR, ">>/tmp/2.log");
        print STDERR "ERROR: $txt\n";
        $cui->error("$txt") if (defined $cui);
        exit(1);
    };

    #   create menu widget
    my $menu_file = [
        { -label => 'Quit',
          -value =>
            sub {
                if (&commonquit()) {
                    $breakout = 2
                }
                else {
                    $datacoll = 1;
                }
                $widget->{'dummy'}->focus;
            }
        }
    ];
    my $menu_as = [
        { -label => 'Testerli 1',         -value => sub {exit(0)}        },
        { -label => 'Testerli 2',         -value => sub {exit(0)}        },
    ];
    my $menu = [
        { -label => 'File',               -submenu => $menu_file         },
        # FIXME { -label => 'Accounting',         -submenu => $menu_as           },
    ];
    $widget->{'menu'} = $cui->add('menu', 'Menubar',
        -menu => $menu
    );

    #   add main window
    $widget->{'main'} = $cui->add('main', 'Window',
        -padtop => 1,    #leave space for menu on the top
        -padbottom => 1, #leave space for status on the bottom
        -border => 0
    );

    #   add status window
    $widget->{'stat'} = $cui->add('stat', 'Window',
        -height    => 1,
        -y         => -1,
        -border => 0
    );

    #   add a status line
    $widget->{'info'} = $widget->{'stat'}->add('info', 'Label',
        -reverse => 1
    );
    &statusline();

    #   add a container
    $widget->{'container'} = $widget->{'main'}->add(undef, 'Container');

    #   add dummy to focus and to indirectly force blur
    $widget->{'dummy'} = $widget->{'container'}->add(undef, 'TextEntry',
        -x => 0,
        -y => 0,
        -text => "",
        -width => 1,
        -height => 1
    );

    $widget->{'container'}->set_binding(\&keybinder, ''); #now we catch all chars not just @keys_move
    $cui->draw;
}

sub ascuido {
    &newmatrixwidget();
    my ($upperleftcol, $cursorcol) = ($matrixdata->{"CK.date"}, 0); #&rightmostvisiblefit();
    my ($upperleftrow, $cursorrow) = &bottommostvisiblefit();
    &layoutmatrixwidget($upperleftcol, $upperleftrow, $cursorcol, $cursorrow);
    &drawfocusmatrixwidget();
    $breakout = 0;
    do {
        $vcscroll = 0;
        $vrscroll = 0;
        $datacoll = 0;
        $vcpos    = 0;
        $vrpos    = 0;

        &statusline() unless($editcell);
        $cui->root->accessor(-cursor_mode => $editcell);
        $cui->root->do_one_event;

        if    ($vcscroll < 0) {
            $upperleftcol += $vcscroll;
            $upperleftcol = 0 if ($upperleftcol < 0);
        }
        elsif ($vcscroll > 0) {
            $upperleftcol += $vcscroll;
            my ($upperleftcolfit, $dummy) = &rightmostvisiblefit();
            $upperleftcol = $upperleftcolfit if ($upperleftcol > $upperleftcolfit);
        }
        if    ($vrscroll < 0) {
            $upperleftrow += $vrscroll;
            $upperleftrow = 0 if ($upperleftrow < 0);
        }
        elsif ($vrscroll > 0) {
            $upperleftrow += $vrscroll;
            my ($upperleftrowfit, $dummy) = &bottommostvisiblefit();
            $upperleftrow = $upperleftrowfit if ($upperleftrow > $upperleftrowfit);
        }

        if ($vcpos < 0) {
            $upperleftcol = 0;
        }
        elsif ($vcpos > 0) {
            ($upperleftcol, my $dummy) = &rightmostvisiblefit();
        }

        if ($vrpos < 0) {
            $upperleftrow = 0;
        }
        elsif ($vrpos > 0) {
            ($upperleftrow, my $dummy) = &bottommostvisiblefit();
        }

        if ($vcscroll != 0 or $vrscroll != 0 or $datacoll != 0 or $vcpos != 0 or $vrpos != 0) {
            &layoutmatrixwidget($upperleftcol, $upperleftrow);
            &drawfocusmatrixwidget();
        }
    } until ($breakout);
    &destroymatrixwidget();

    return "save" if ($breakout == 1);
    return "quit" if ($breakout == 2);
    return "exit" if ($breakout == 3);
    return;
}

1;
