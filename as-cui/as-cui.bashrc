##
##  AS -- Accounting System
##  Copyright (c) 2002 Cable & Wireless Deutschland <http://www.cw.com/de/>
##  Copyright (c) 2002 Ralf S. Engelschall <rse@engelschall.com>
##
##  This file is part of AS, an accounting system which can be
##  found at http://as.is.eu.cw.com/
##
##  This program is free software; you can redistribute it and/or
##  modify it under the terms of the GNU General Public License
##  as published by the Free Software Foundation; either version
##  2.0 of the License, or (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
##  USA, or contact The OSSP Project <ossp@ossp.org>.
##
##  as-cui.bashrc: AS command line convenience wrapper for GNU bash 
##

as_complete () {
    #   determine current context
    local arg_pos="$COMP_CWORD"
    local arg_cur="${COMP_WORDS[COMP_CWORD]}"
    local arg_prev="${COMP_WORDS[COMP_CWORD-1]}"

    #   initialize reply
    COMPREPLY=()

    if [ $arg_pos -eq 2 ]; then
        #   complete account name
        COMPREPLY=($(as-cui --complete=account "$arg_cur"))
    fi
}

complete -F as_complete as-cui

alias as="as-cui"

